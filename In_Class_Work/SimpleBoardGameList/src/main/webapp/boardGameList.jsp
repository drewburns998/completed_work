<%-- 
    Document   : boardGameList
    Created on : Mar 25, 2016, 1:44:47 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List of Awesome and yet not PC Board Games</title>
    </head>
    <body>

        <h1>Board Game List</h1>

        <c:forEach items="${myExcellentLibrary}" var="boardGame">

            <c:out value="${boardGame.name}" default="Unknown" />
            (<c:out value="${boardGame.publisher}" default="Unknown" />)
            <br />
            Min Players: <c:out value="${boardGame.minPlayers}" default="?" />,
            Max Players: <c:out value="${boardGame.maxPlayers}" default="?" />
            <br />
            <c:out value="${boardGame.avgDuration}" default="??" /> hours
            <br />

            <hr />
        </c:forEach>


    </body>
</html>
