/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.basecalc.dao;

import com.swcguild.basecalc.dto.NumberReformed;
import java.util.Stack;

/**
 *
 * @author apprentice
 */
public interface BaseCalcDAO {

    Stack<Integer> convertFromBaseTen(NumberReformed num);

    Stack<Integer> convertToBaseTen(int num);
    
}
