/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.basecalc.ops;

import com.swcguild.basecalc.dao.BaseCalcDAO;
import com.swcguild.basecalc.dto.NumberReformed;
import com.swcguild.basecalc.ui.ConsoleIO;
import java.util.Stack;

/**
 *
 * @author apprentice
 */
public class BaseCalcControllerImpl implements BaseCalcController {

    ConsoleIO console;// = new ConsoleIOImpl();
    BaseCalcDAO dao;// = new BaseCalcDAOImpl();


    @Override
    public void run() {
        int startingNumber, newBase, startingBase;
        
        console.print("Welcome to the Binary Conversion Tool.\n"
                + "You can convert from Base 10 to any other Base Value and Reverse!\n");

        switch(menuSelect()){
            case 1:
                console.print("Conversion From Base 10");
                startingNumber = console.readInt("Number to be converted: ");
                newBase = console.readInt("Enter the new base of the number");
                fromBaseTen(new NumberReformed(startingNumber, 10, newBase));
                break;
            case 2:
                console.print("Conversion to Base 10");
                startingNumber = console.readInt("Number to be converted: ");
                startingBase = console.readInt("Enter the Current Base:");
                toBaseTen(new NumberReformed(startingNumber, startingBase, 10));
                break;
            
        }
        
    }

    private int menuSelect() {
        console.print("************************");
        int userChoice = console.readInt("Enter 1 to convert a number from base 10 "
                + "to another base.\n"
                + "Enter 2 to convert from any base back to base 10.",1,2);
        console.print("************************");
        return userChoice;        
    }

    private void fromBaseTen(NumberReformed num) {
        Stack<Integer> results = dao.convertFromBaseTen(num);

        while (results.size() > 0) {
            console.printOneLine(results.pop().toString());
        }
    }

    private void toBaseTen(NumberReformed num) {
        int sumToBaseTen = 0;
        int index = 0;
        Stack<Integer> results = dao.convertToBaseTen(num.getStartingNumber());
        while (results.size() > 0) {
            sumToBaseTen = (int) (sumToBaseTen + results.pop() * Math.pow(num.getStartingBase(), index));
            index++;
        }

        console.print("Converting from Base " + num.getStartingBase() + " to Base 10");
        console.print("Result: " + sumToBaseTen);
    }
    
    public ConsoleIO getConsole() {
        return console;
    }

    public void setConsole(ConsoleIO console) {
        this.console = console;
    }

    public BaseCalcDAO getDao() {
        return dao;
    }

    public void setDao(BaseCalcDAO dao) {
        this.dao = dao;
    }
}
