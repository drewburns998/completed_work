/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.basecalc.dto;

/**
 *
 * @author apprentice
 */
public class NumberReformed {
    private final int startingNumber;
    private final int startingBase;
    private final int newBase;

    public NumberReformed(int startingNumber, int startingBase, int newBase) {
        this.startingNumber = startingNumber;
        this.startingBase = startingBase;
        this.newBase = newBase;
    }

    public int getStartingNumber() {
        return startingNumber;
    }

    public int getStartingBase() {
        return startingBase;
    }

    public int getNewBase() {
        return newBase;
    }
    
    
    
    
    
}
