/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.basecalc.dao;

import com.swcguild.basecalc.dto.NumberReformed;
import java.util.Stack;

/**
 *
 * @author apprentice
 */
public class BaseCalcDAOImpl implements BaseCalcDAO {

    Stack<Integer> intStack = new Stack<>();

    @Override
    public Stack<Integer> convertFromBaseTen(NumberReformed num) {
        int tempNum = num.getStartingNumber();
        while (tempNum > 0) {
            intStack.add(tempNum % num.getNewBase());
            tempNum = tempNum / num.getNewBase();
        }
        return intStack;
    }

    @Override
    public Stack<Integer> convertToBaseTen(int num) {
        do {
            intStack.add(num % 10);
            num = num / 10;
        } while (num > 0);

        return intStack;
    }

}
