/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package methods;

/**
 *
 * @author apprentice
 */
public class Methods {

    public static int addInts(int numOne, int numTwo) {
        int numSum = numOne + numTwo;
        return numSum;
    }

    public static String animalNames(int numEntered) {
        String animalName = "";
        //llama 3, squirrel 5, bonobo 1, 
        switch (numEntered) {
            case 1:
                animalName = "bonobo";
                break;
            case 3:
                animalName = "Llama";
                break;
            case 5:
                animalName = "squirrel";
                break;            
        }
        return animalName;
    }

    public static String greeting(String name) {
        if (!name.isEmpty()) {
            return "Hello " + name;
        } else{
            return "Who are you .. doo doo.. doo doo";
        }
    }
    
    public boolean and (boolean a, boolean b){
        return a && b;
    }
    
    public boolean and(boolean a, boolean b, boolean c){
        return and(a, b) && c;
    }

}
