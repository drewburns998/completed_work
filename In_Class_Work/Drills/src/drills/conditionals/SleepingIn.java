/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class SleepingIn {

    public boolean CanSleepIn(boolean isWeekday, boolean isVacation) {
        if (isWeekday && !isVacation) {
            return false;
        } else if (!isWeekday) {
            return true;
        }
        return true;
    }    
}
