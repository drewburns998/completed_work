/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class SoAlone {

    public boolean SoAlone(int a, int b) {
        boolean teenA = 13 <= a && a <= 19;
        boolean teenB = 13 <= b && b <= 19;

        return teenA != teenB;
    }
}
