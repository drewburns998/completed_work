/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class Multiple3Or5 {

    public boolean Multiple3Or5(int number) {
        return number % 3 == 0 || number % 5 == 0;
    }
}
