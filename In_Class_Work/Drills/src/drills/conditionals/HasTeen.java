/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class HasTeen {

    public boolean HasTeen(int a, int b, int c) {
        return 13 <= a && a <= 19 || 13 <= b && b <= 19
                || 13 <= c && c <= 19;
    }
}
