/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class StartOz {

    public String StartOz(String str) {
        String output = "";

        if (str.length() >= 1 && str.charAt(0) == 'o') {
            output += str.charAt(0);
        }
        if (str.length() >= 2 && str.charAt(1) == 'z') {
            output += str.charAt(1);
        }
        return output;
    }
}
