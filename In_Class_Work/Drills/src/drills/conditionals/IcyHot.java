/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class IcyHot {

    public boolean IcyHot(int temp1, int temp2) {
        return (temp1 * temp2 < 0) && Math.abs(temp1 - temp2) >= 102;
    }
}
