/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class NotString {

    public String NotString(String s) {
        if (s.length() > 2) {
            String checkNot = s.substring(0, 3);
            if (checkNot.equals("not")) {
                return s;
            } else {
                return "not " + s;
            }
        } else {
            return "not " + s;
        }

    }
}
