/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class FrontBack {

    public static void main(String[] args) {
        FrontBack test = new FrontBack();

        String test2 = test.FrontBack("a");

        System.out.println(test2);
    }

    public String FrontBack(String str) {
        if (str.length() > 1) {
            String beginning = str.substring(0, 1);
            String end = str.substring(str.length() - 1);
            String middle = str.substring(1, str.length() - 1);
            return end + middle + beginning;
        } else {
            return str;
        }
    }

}
