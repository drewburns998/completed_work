/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class Closer {

    public int Closer(int a, int b) {
        int aDist = Math.abs(a - 10);
        int bDist = Math.abs(b - 10);

        if (aDist == bDist) {
            return 0;
        }
        return aDist < bDist ? a : b;
    }
}
