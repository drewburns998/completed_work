/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 *
 * @author apprentice
 */
public class Diff21 {

    public int Diff21(int n) {
        int diff = Math.abs(n - 21);

        if (n <= 21) {
            return diff;
        } else {
            return diff * 2;
        }
    }
}
