/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

/**
 * BackAround("cat") -> "tcatt" BackAround("Hello") -> "oHelloo" BackAround("a")
 * -> "aaa"
 *
 * @author apprentice
 */
public class BackAround {

    public String BackAround(String str) {
        String endChar = str.substring(str.length() - 1);

        return endChar + str + endChar;
    }
}
