/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import java.util.Arrays;

/**
 *
 * @author apprentice
 */
public class Make2 {
    
    public int[] make2(int[] a, int[] b) {
        int[] newArray = new int[a.length + b.length];
        System.arraycopy(a, 0, newArray, 0, a.length);
        System.arraycopy(b, 0, newArray, a.length, b.length);
        
        int[] finalArray = Arrays.copyOfRange(newArray, 0, 2);
        return finalArray;
    }

}
