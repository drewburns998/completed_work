/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

/**
 *
 * @author apprentice
 */
public class HigherWins {

    public int[] HigherWins(int[] numbers) {
        int[] newArray = new int[numbers.length];
        int highNum = 0;

        if (numbers[0] > numbers[numbers.length - 1]) {
            highNum = numbers[0];
        } else {
            highNum = numbers[numbers.length - 1];
        }
        for (int i = 0; i < numbers.length; i++) {
            newArray[i] = highNum;
        }
        return newArray;
    }
}
