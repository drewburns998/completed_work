/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

/**
 *
 * @author apprentice
 */
public class Reverse {

    public int[] Reverse(int[] numbers) {
        int[] backward = new int[numbers.length];

        for (int i = 0; i < numbers.length; i++) {
            backward[numbers.length - i - 1] = numbers[i];
        }

        return backward;
    }
}
