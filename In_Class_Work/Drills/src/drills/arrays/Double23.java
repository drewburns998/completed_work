/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

/**
 *
 * @author apprentice
 */
public class Double23 {

    public boolean Double23(int[] numbers) {
        int twoCount = 0;
        int threeCount = 0;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] == 2) {
                twoCount++;
            } else if (numbers[i] == 3) {
                threeCount++;
            }
        }
        return twoCount > 1 || threeCount > 1;
    }
}
