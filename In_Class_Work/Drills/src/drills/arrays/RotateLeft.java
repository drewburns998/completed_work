/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

/**
 *
 * @author apprentice
 */
public class RotateLeft {
    public static void main(String[] args) {
        RotateLeft test = new RotateLeft();
        int[] test2 = {1,2,3};
        int[] test4 = {2,3,1};
        int[] test3 = test.RotateLeft(test2);
        
        for (int i = 0; i < test3.length; i++) {
            System.out.println(test3[i]);
        }
        
    }
    public int[] RotateLeft(int[] numbers) {
        int temp = numbers[0];
        
        for (int i = 0; i < (numbers.length - 1); i++) {
            numbers[i] = numbers[i+1];
        }
        numbers[numbers.length-1] = temp;
        
        return numbers;
    }
}
