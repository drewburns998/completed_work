/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class MiddleTwo {
    //MiddleTwo("string") -> "ri"
    //MiddleTwo("code") -> "od"
    //MiddleTwo("Practice") -> "ct"

    public String MiddleTwo(String str) {
        int mid = str.length()/2;
        
        return str.substring(mid-1, mid+1);
    }
}
