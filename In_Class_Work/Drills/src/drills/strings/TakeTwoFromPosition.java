/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class TakeTwoFromPosition {
    
    public String TakeTwoFromPosition(String str, int n) {
        if (n + 2 > str.length() || n < 0) {
            return str.substring(0, 2);
        }
        return str.substring(n, n + 2);
    }
}
