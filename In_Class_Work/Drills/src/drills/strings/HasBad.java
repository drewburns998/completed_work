/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class HasBad {

    public boolean HasBad(String str) {
        if (str.length() == 3) {
            return str.substring(0, 3).equals("bad");
        }
        if (str.length() >= 4) {
            return str.substring(0, 3).equals("bad") || str.substring(1, 4).equals("bad");
        }
        return false;
    }
}
