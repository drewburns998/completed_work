/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class MinCat {

    public String MinCat(String a, String b) {
        int min = Math.min(a.length(), b.length());
        return a.substring(a.length() - min) + b.substring(b.length() - min);
    }
}
