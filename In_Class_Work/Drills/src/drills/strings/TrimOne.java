/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class TrimOne {
    //TrimOne("Hello") -> "ell"
    //TrimOne("java") -> "av"
    //TrimOne("coding") -> "odin"

    public String TrimOne(String str) {
        return str.substring(1,str.length()-1);
    }
}
