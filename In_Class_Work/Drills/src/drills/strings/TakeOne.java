/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class TakeOne {
    //TakeOne("Hello", true) -> "H"
    //TakeOne("Hello", false) -> "o"
    //TakeOne("oh", true) -> "o"

    public String TakeOne(String str, boolean fromFront) {
        if(fromFront){
            return str.substring(0,1);
        } else{
            return str.substring(str.length()-1);
        }
    }
}
