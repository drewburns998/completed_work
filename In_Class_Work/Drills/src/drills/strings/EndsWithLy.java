/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class EndsWithLy {

    public boolean EndsWithLy(String str) {
        if (str.length() > 1) {
            return str.substring(str.length() - 2, str.length()).equals("ly");
        } else {
            return false;
        }

    }
}
