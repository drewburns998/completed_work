/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class MultipleEndings {
    //MultipleEndings("Hello") -> "lololo"
    //MultipleEndings("ab") -> "ababab"
    //MultipleEndings("Hi") -> "HiHiHi"

    public String MultipleEndings(String str) {
        String newString = str.substring(str.length() - 2);
        return newString + newString + newString;
    }
}
