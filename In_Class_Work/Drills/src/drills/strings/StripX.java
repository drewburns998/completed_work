/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

/**
 *
 * @author apprentice
 */
public class StripX {

    public String StripX(String str) {
        int start = 0;
        int end = str.length();

        if (str.length() > 0 && str.charAt(0) == 'x') {
            start = 1;
        }
        if (str.length() > 1 && str.charAt(str.length() - 1) == 'x') {
            end--;
        }
        return str.substring(start, end);
    }
}
