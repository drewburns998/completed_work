/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

/**
 *
 * @author apprentice
 */
public class ArrayFront9 {

    public boolean ArrayFront9(int[] numbers) {
        boolean has9 = false;
        if (numbers.length < 4) {
            for (int i = 0; i < numbers.length; i++) {
                if (numbers[i] == 9) {
                    has9 = true;
                }
            }
        } else {
            for (int i = 0; i < 4; i++) {
                if (numbers[i] == 9) {
                    has9 = true;
                }
            }
        }
        return has9;
    }
}
