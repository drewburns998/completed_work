/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

/**
 *
 * @author apprentice
 */
public class SubStringMatch {

    public int SubStringMatch(String a, String b) {
        int counter = 0;

        for (int i = 0; i < a.length() - 1; i++) {
            String sub = a.substring(i, i + 2);
            if (b.contains(sub)) {
                counter++;
            }
        }
        return counter;
    }
}
