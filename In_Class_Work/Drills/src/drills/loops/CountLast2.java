/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

/**
 *
 * @author apprentice
 */
public class CountLast2 {
    
    public static void main(String[] args) {
        CountLast2 testObj = new CountLast2();
        System.out.println(testObj.CountLast2("xaxxaxaxx"));
    }
    public int CountLast2(String str) {
        String lastTwo = str.substring(str.length()-2, str.length());
        int count = 0;
        for (int i = 0; i < str.length()-2; i++) {
            if(str.substring(i,i+2).equals(lastTwo)){
                count++;
            }
        }
        return count;
    }
}
