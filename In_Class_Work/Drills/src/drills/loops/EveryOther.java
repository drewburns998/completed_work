/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

/**
 *
 * @author apprentice
 */
public class EveryOther {

    public String EveryOther(String str) {
        String newString = "";
        for (int i = 0; i < str.length(); i++) {
            if ((i + 2) % 2 == 0) {
                newString = newString + str.substring(i, i + 1);
            }
        }
        return newString;
    }
}
