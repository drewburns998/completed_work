/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

/**
 *
 * @author apprentice
 */
public class NoTriples {

    public boolean NoTriples(int[] numbers) {
        for (int i = 0; i < numbers.length - 2; i++) {
            if (numbers[i + 1] == numbers[i] && numbers[i + 2] == numbers[i]) {
                return false;
            }
        }
        return true;
    }
}
