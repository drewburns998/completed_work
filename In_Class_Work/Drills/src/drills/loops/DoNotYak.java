/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

/**
 *
 * @author apprentice
 */
public class DoNotYak {
    
    public String DoNotYak(String str) {
        char[] result = new char[str.length()];
        int count = 0;

        for (int i = 0; i < str.length();) {
            if (i < str.length() - 2 && str.charAt(i) == 'y'
                    && str.charAt(i + 2) == 'k') {
                i += 3;
            } else {
                result[count] = str.charAt(i);
                count++;
                i++;
            }
        }

        return new String(result, 0, count);
    }
}
