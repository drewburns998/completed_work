/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

/**
 *
 * @author apprentice
 */
public class StringX {

    public String StringX(String str) {
        String start = str.substring(0, 1);
        String end = str.substring(str.length() - 1, str.length());
        String result = str.replaceAll("x", "");

        if (start.equals("x")) {
            result = start + result;
        }
        if (end.equals("x")) {
            result = result + end;
        }
        return result;
    }
}
