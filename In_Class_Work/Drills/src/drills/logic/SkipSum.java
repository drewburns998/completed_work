/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class SkipSum {
    
    public int SkipSum(int a, int b) {
        int sum = a + b;
        
        if (sum < 10 || sum > 19) {
            return sum;
        } else {
            return 20;
        }
    }
}
