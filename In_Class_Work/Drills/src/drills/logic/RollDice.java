/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class RollDice {

    public int RollDice(int die1, int die2, boolean noDoubles) {
        int sum = die1 + die2;

        if (noDoubles && die1 == die2) {
            if (sum == 12) {
                sum = 7;
            } else {
                sum = sum + 1;
            }
        }
        return sum;
    }
}
