/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class LastDigit {
    
    public boolean LastDigit(int a, int b, int c) {
        int aDigit = a % 10;
        int bDigit = b % 10;
        int cDigit = c % 10;

        if (aDigit == bDigit || aDigit == cDigit) {
            return true;
        } else {
            return bDigit == cDigit;
        }
    }
}
