/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class PlayOutside {
    
    public boolean PlayOutside(int temp, boolean isSummer) {
        if(isSummer){
            return temp >= 60 && temp <= 100;
        } else{
            return temp >= 60 && temp <= 90;
        }
    }
}
