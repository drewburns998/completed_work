/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class InRange {
    
    public boolean InRange(int n, boolean outsideMode) {
        if(!outsideMode){
            return n >=1 && n <=10;
        }else{
            return n <=1 || n >=10;
        }        
    }
}
