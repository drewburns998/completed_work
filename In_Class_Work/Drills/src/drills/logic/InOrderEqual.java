/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class InOrderEqual {
    
    public boolean InOrderEqual(int a, int b, int c, boolean equalOk) {

        if (!equalOk) {
            return c > b && b > a;
        } else {
            return c >= b && b >=a;
        }
    }
}
