/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class Mod35 {
    //Mod35(3) → true
    //Mod35(10) → true
    //Mod35(15) → false

    public boolean Mod35(int n) {
        return (n % 3 == 0 || n % 5 == 0) && (n % 15 != 0);
    }
}
