/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class AnswerCell {
    
    public boolean AnswerCell(boolean isMorning, boolean isMom, boolean isAsleep) {
        if (isAsleep) {
            return false;
        } else return !(isMorning && !isMom);
    }
}
