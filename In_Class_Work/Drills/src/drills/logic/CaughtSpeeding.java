/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class CaughtSpeeding {    

    //0=no ticket, 1=small ticket, 2=big ticket
    public int CaughtSpeeding(int speed, boolean isBirthday) {
        if (isBirthday) {
            speed = speed - 5;
        }
        if (speed <= 60) {
            return 0;
        } else if (speed >= 61 && speed <= 80) {
            return 1;
        } else {
            return 2;
        }
    }
}
