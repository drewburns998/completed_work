/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

/**
 *
 * @author apprentice
 */
public class AlarmClock {
    //AlarmClock(1, false) → "7:00"
    //AlarmClock(5, false) → "7:00"
    //AlarmClock(0, false) → "10:00"

    //0=Sun, 1=Mon, 2=Tue, ...6=Sat
    public String AlarmClock(int day, boolean vacation) {
        if (vacation) {
            if (day < 6 && day > 0) {
                return "10:00";
            } else {
                return "off";
            }
        } else if (day < 6 && day > 0) {
            return "7:00";
        } else {
            return "10:00";
        }

    }

}
