/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class StringSplosionTest {
    StringSplosion testObj = new StringSplosion();
    public StringSplosionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //StringSplosion("Code") -> "CCoCodCode"
    //StringSplosion("abc") -> "aababc"
    //StringSplosion("ab") -> "aab"
    @Test
    public void tryFourLetterWord(){
        Assert.assertTrue(testObj.StringSplosion("Code").equals("CCoCodCode"));
    }
    @Test
    public void tryThreeLetterWord(){
        Assert.assertTrue(testObj.StringSplosion("abc").equals("aababc"));
    }
    @Test
    public void tryTwoLetterWord(){
        Assert.assertTrue(testObj.StringSplosion("ab").equals("aab"));
    }
}
