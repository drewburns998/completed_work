/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class Pattern51Test {
    Pattern51 testObj = new Pattern51();
    public Pattern51Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //Pattern51({1, 2, 7, 1}) -> true
    //Pattern51({1, 2, 8, 1}) -> false
    //Pattern51({2, 7, 1}) -> true
    @Test
    public void hasPatternEnd(){
        int[] input = {1,2,7,1};
        Assert.assertTrue(testObj.Pattern51(input));
    }
    
    @Test
    public void hasNoPattern(){
        int[] input = {1,2,8,1};
        Assert.assertFalse(testObj.Pattern51(input));
    }
    @Test
    public void hasPattern(){
        int[] input = {2,7,1};
        Assert.assertTrue(testObj.Pattern51(input));
    }
}
