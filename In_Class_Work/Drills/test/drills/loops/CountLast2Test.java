/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class CountLast2Test {
    CountLast2 testObj = new CountLast2();
    public CountLast2Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //CountLast2("hixxhi") -> 1
    //CountLast2("xaxxaxaxx") -> 1
    //CountLast2("axxxaaxx") -> 2
    @Test
    public void has2BeginAndEnd(){
        Assert.assertTrue(testObj.CountLast2("hixxhi") == 1);
    }
    
    @Test
    public void has2BeginAndEndAgain(){
        Assert.assertTrue(testObj.CountLast2("xaxxaxaxx") == 1);
    }
    
    @Test
    public void has3BeginAndEndWithConsecutives(){
        Assert.assertTrue(testObj.CountLast2("axxxaaxx") == 2);
    }
}
