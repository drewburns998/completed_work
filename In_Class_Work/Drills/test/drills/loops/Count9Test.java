/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Count9Test {
    Count9 testObj = new Count9();
    public Count9Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //Count9({1, 2, 9}) -> 1
    //Count9({1, 9, 9}) -> 2
    //Count9({1, 9, 9, 3, 9}) -> 3
    @Test
    public void hasOne9(){
        int[] testArray = {1,2,9};
        Assert.assertTrue(testObj.Count9(testArray)==1);
    }
    @Test
    public void hasTwo9(){
        int[] testArray = {1,9,9};
        Assert.assertTrue(testObj.Count9(testArray)==2);
    }
    @Test
    public void hasThreeNonConsec9(){
        int[] testArray = {1,9,9,3,9};
        Assert.assertTrue(testObj.Count9(testArray)==3);
    }
}
