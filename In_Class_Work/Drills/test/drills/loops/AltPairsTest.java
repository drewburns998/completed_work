/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class AltPairsTest {
    AltPairs testObj = new AltPairs();
    public AltPairsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //AltPairs("kitten") -> "kien"
    //AltPairs("Chocolate") -> "Chole"
    //AltPairs("CodingHorror") -> "Congrr"
    @Test
    public void mediumSizeWord(){
        Assert.assertTrue(testObj.AltPairs("kitten").equals("kien"));
    }
    @Test
    public void LongWord(){
        Assert.assertTrue(testObj.AltPairs("Chocolate").equals("Chole"));
    }
    @Test
    public void LongerWord(){
        Assert.assertTrue(testObj.AltPairs("CodingHorror").equals("Congrr"));
    }
}
