/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FrontTimesTest {

    FrontTimes testObj = new FrontTimes();
    
    public FrontTimesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //FrontTimes("Chocolate", 2) -> "ChoCho"
    //FrontTimes("Chocolate", 3) -> "ChoChoCho"
    //FrontTimes("Abc", 3) -> "AbcAbcAbc"
    
    @Test
    public void longWordTwice(){
        Assert.assertTrue(testObj.FrontTimes("Chocolate", 2).equals("ChoCho"));
    }
    
    @Test
    public void longWordThreeTimes(){
        Assert.assertTrue(testObj.FrontTimes("Chocolate", 3).equals("ChoChoCho"));
    }
    @Test
    public void shortWordThreeTimes(){
        Assert.assertTrue(testObj.FrontTimes("Abc", 3).equals("AbcAbcAbc"));
    }
}
