/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class StringTimesTest {
    StringTimes testObj = new StringTimes();
    public StringTimesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //StringTimes("Hi", 2) -> "HiHi"
    //StringTimes("Hi", 3) -> "HiHiHi"
    //StringTimes("Hi", 1) -> "Hi"
    @Test
    public void test2Times(){
        Assert.assertTrue(testObj.StringTimes("Hi", 2).equals("HiHi"));
    }
    @Test
    public void test3Times(){
        Assert.assertTrue(testObj.StringTimes("Hi", 3).equals("HiHiHi"));
    }
    @Test
    public void test1Time(){
        Assert.assertTrue(testObj.StringTimes("Hi", 1).equals("Hi"));
    }
    
}
