/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class EveryOtherTest {
    EveryOther testObj = new EveryOther();
    public EveryOtherTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //EveryOther("Hello") -> "Hlo"
    //EveryOther("Hi") -> "H"
    //EveryOther("Heeololeo") -> "Hello"
    @Test
    public void tryMedSizeWord(){
        Assert.assertTrue(testObj.EveryOther("Hello").equals("Hlo"));
    }
    
    @Test
    public void trySmallSizeWord(){
        Assert.assertTrue(testObj.EveryOther("Hi").equals("H"));
    }
    @Test
    public void tryLargeSizeWord(){
        Assert.assertTrue(testObj.EveryOther("Heeololeo").equals("Hello"));
    }
}
