/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Array123Test {
    Array123 testObj = new Array123();
    public Array123Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //Array123({1, 1, 2, 3, 1}) -> true
    //Array123({1, 1, 2, 4, 1}) -> false
    //Array123({1, 1, 2, 1, 2, 3}) -> true
    @Test
    public void valueInMiddle(){
        int[] input = {1,1,2,3,1};
        Assert.assertTrue(testObj.Array123(input));
    }
    @Test
    public void valueNotInArray(){
        int[] input = {1,1,2,4,1};
        Assert.assertFalse(testObj.Array123(input));
    }
    @Test
    public void valueAtEnd(){
        int[] input = {1,1,2,1,2,3};
        Assert.assertTrue(testObj.Array123(input));
    }
}
