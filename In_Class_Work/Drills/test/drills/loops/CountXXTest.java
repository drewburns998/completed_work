/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CountXXTest {
    
    CountXX testObj = new CountXX();
    
    public CountXXTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //CountXX("abcxx") -> 1
    //CountXX("xxx") -> 2
    //CountXX("xxxx") -> 3
    @Test
    public void oneSetAtEnd(){
        Assert.assertTrue(testObj.CountXX("abcxx") == 1);
    }
    
    @Test
    public void threeXValues(){
        Assert.assertTrue(testObj.CountXX("xxx") == 2);
    }
    @Test
    public void fourXValues(){
        Assert.assertTrue(testObj.CountXX("xxxx") == 3);
    }
}
