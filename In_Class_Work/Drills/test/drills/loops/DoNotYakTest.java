/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class DoNotYakTest {
    DoNotYak testObj = new DoNotYak();
    public DoNotYakTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //DoNotYak("yakpak") -> "pak"
    //DoNotYak("pakyak") -> "pak"
    //DoNotYak("yak123ya") -> "123ya"
    @Test
    public void startWithYak(){
        Assert.assertTrue(testObj.DoNotYak("yakpak").equals("pak"));
    }
    @Test
    public void endWithYak(){
        Assert.assertTrue(testObj.DoNotYak("pakyak").equals("pak"));
    }
    @Test
    public void startWithYakEndWithYA(){
        Assert.assertTrue(testObj.DoNotYak("yak123ya").equals("123ya"));
    }
    
}
