/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class DoubleXTest {
    DoubleX testObj = new DoubleX();
    public DoubleXTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //DoubleX("axxbb") -> true
    //DoubleX("axaxxax") -> false
    //DoubleX("xxxxx") -> true
    @Test
    public void hasOneDoubleX(){
        Assert.assertTrue(testObj.DoubleX("axxbb"));
    }
    
    @Test
    public void hasSingleXThenOneDoubleX(){
        Assert.assertFalse(testObj.DoubleX("axaxxax"));
    }
    @Test
    public void hasFiveConsecX(){
        Assert.assertTrue(testObj.DoubleX("xxxxx"));
    }
    @Test
    public void hasNoX(){
        Assert.assertFalse(testObj.DoubleX("abcdef"));
    }
}
