/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class NoTriplesTest {
    NoTriples testObj = new NoTriples();
    public NoTriplesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //NoTriples({1, 1, 2, 2, 1}) -> true
    //NoTriples({1, 1, 2, 2, 2, 1}) -> false
    //NoTriples({1, 1, 1, 2, 2, 2, 1}) -> false
    @Test
    public void tripleNotConsec(){
        int[] input = {1, 1, 2, 2, 1};
        Assert.assertTrue(testObj.NoTriples(input));
    }
    @Test
    public void oneConsecTriple(){
        int[] input = {1, 1, 2, 2, 2, 1};
        Assert.assertFalse(testObj.NoTriples(input));
    }
    @Test
    public void twoConsecTriples(){
        int[] input = {1, 1, 1, 2, 2, 2, 1};
        Assert.assertFalse(testObj.NoTriples(input));
    }
}
