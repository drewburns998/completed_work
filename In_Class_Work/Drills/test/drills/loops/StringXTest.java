/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class StringXTest {
    StringX testObj = new StringX();
    public StringXTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //StringX("xxHxix") -> "xHix"
    //StringX("abxxxcd") -> "abcd"
    //StringX("xabxxxcdx") -> "xabcdx"
    @Test
    public void t1(){
        Assert.assertTrue(testObj.StringX("xxHxix").equals("xHix"));
    }
    @Test
    public void t2(){
        Assert.assertTrue(testObj.StringX("abxxxcd").equals("abcd"));
    }
    @Test
    public void t3(){
        Assert.assertTrue(testObj.StringX("xabxxxcdx").equals("xabcdx"));
    }
}
