/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class SubStringMatchTest {

    SubStringMatch testObj = new SubStringMatch();

    public SubStringMatchTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //SubStringMatch("xxcaazz", "xxbaaz") -> 3
    //SubStringMatch("abc", "abc") -> 2
    //SubStringMatch("abc", "axc") -> 0
    @Test
    public void tryLongSimilar() {
        Assert.assertTrue(testObj.SubStringMatch("xxcaazz", "xxbaaz") == 3);
    }

    @Test
    public void tryShortSame() {
        Assert.assertTrue(testObj.SubStringMatch("abc", "abc") == 2);
    }

    @Test
    public void tryShortDifferent() {
        Assert.assertTrue(testObj.SubStringMatch("abc", "axc") == 0);
    }

}
