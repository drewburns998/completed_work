/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class Array667Test {

    Array667 testObj = new Array667();

    public Array667Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    //Array667({6, 6, 2}) -> 1
    //Array667({6, 6, 2, 6}) -> 1
    //Array667({6, 7, 2, 6}) -> 1

    @Test
    public void test1() {
        int[] input = {6, 6, 2};
        Assert.assertTrue(testObj.Array667(input) == 1);
    }

    @Test
    public void test2() {
        int[] input = {6, 6, 2, 6};
        Assert.assertTrue(testObj.Array667(input) == 1);
    }

    @Test
    public void test3() {
        int[] input = {6, 7, 2, 6};
        Assert.assertTrue(testObj.Array667(input) == 1);
    }
}
