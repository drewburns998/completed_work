/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CloserTest {
    Closer testObj = new Closer();
    public CloserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //Closer(8, 13) -> 8
    //Closer(13, 8) -> 8
    //Closer(13, 7) -> 0
    @Test
    public void firstCloser(){
        Assert.assertTrue(testObj.Closer(8, 13) == 8);
    }
    
    @Test
    public void secondCloser(){
        Assert.assertTrue(testObj.Closer(13, 8) == 8);
    }
    
    @Test
    public void neitherCloser(){
        Assert.assertTrue(testObj.Closer(13, 7) == 0);
    }
}
