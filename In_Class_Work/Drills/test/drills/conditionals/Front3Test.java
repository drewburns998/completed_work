/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Front3Test {
    Front3 testObj = new Front3();
    
    public Front3Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //Front3("Microsoft") -> "MicMicMic"
    //Front3("Chocolate") -> "ChoChoCho"
    //Front3("at") -> "atatat"
    @Test
    public void tryLongWord(){
        Assert.assertTrue(testObj.Front3("Microsoft").equals("MicMicMic"));
    }
    @Test
    public void tryLongWord2(){
        Assert.assertTrue(testObj.Front3("Chocolate").equals("ChoChoCho"));
    }
    @Test
    public void tryShortWord(){
        Assert.assertTrue(testObj.Front3("at").equals("atatat"));
    }
}
