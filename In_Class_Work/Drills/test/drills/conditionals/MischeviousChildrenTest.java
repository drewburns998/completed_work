/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class MischeviousChildrenTest {
    MischeviousChildren testObj = new MischeviousChildren();
    
    public MischeviousChildrenTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void bothSmiling(){        
        Assert.assertTrue(testObj.AreWeInTrouble(true, true));
    }
    @Test
    public void neitherSmiling(){        
        Assert.assertTrue(testObj.AreWeInTrouble(false, false));
    }
    @Test
    public void oneSmiling(){        
        Assert.assertFalse(testObj.AreWeInTrouble(true, false));
    }
}
