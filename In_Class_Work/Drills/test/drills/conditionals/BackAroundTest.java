/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class BackAroundTest {
    
    BackAround testObj = new BackAround();
    public BackAroundTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void shortWord(){
        Assert.assertTrue(testObj.BackAround("cat").equals("tcatt"));
    }
    @Test
    public void middleWord(){
        Assert.assertTrue(testObj.BackAround("Hello").equals("oHelloo"));
    }
    @Test
    public void singleLetter(){
        Assert.assertTrue(testObj.BackAround("a").equals("aaa"));
    }
}
