/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class ParrotTroubleTest {
    
    ParrotTrouble testObj = new ParrotTrouble();
    
    public ParrotTroubleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void talkingBefore7(){
        Assert.assertTrue(testObj.ParrotTrouble(true, 6));
    }
    @Test
    public void talkingAt7(){
        Assert.assertFalse(testObj.ParrotTrouble(true, 7));
    }
    @Test
    public void NotTalkingBefore7(){
        Assert.assertFalse(testObj.ParrotTrouble(false, 6));
    }
}
