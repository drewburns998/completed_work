/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class EveryNthTest {

    EveryNth testObj = new EveryNth();

    public EveryNthTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //EveryNth("Miracle", 2) -> "Mrce"
    //EveryNth("abcdefg", 2) -> "aceg"
    //EveryNth("abcdefg", 3) -> "adg"
    @Test
    public void every2() {
        Assert.assertTrue(testObj.EveryNth("Miracle", 2).equals("Mrce"));
    }

    @Test
    public void every2RandomLetters() {
        Assert.assertTrue(testObj.EveryNth("abcdefg", 2).equals("aceg"));
    }

    @Test
    public void every3RandomLetters() {
        Assert.assertTrue(testObj.EveryNth("abcdefg", 3).equals("adg"));
    }
}
