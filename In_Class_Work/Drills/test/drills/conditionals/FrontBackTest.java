/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FrontBackTest {
    FrontBack testObj = new FrontBack();
    public FrontBackTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //FrontBack("code") -> "eodc"
    //FrontBack("a") -> "a"
    //FrontBack("ab") -> "ba"
    
    @Test
    public void fourLetterWord(){
        
      Assert.assertTrue(testObj.FrontBack("code").equals("eodc"));
      
    }
    
    @Test
    public void oneLetterWord(){
        
      Assert.assertTrue(testObj.FrontBack("a").equals("a"));
      
    }
    
    @Test
    public void twoLetterWord(){
        
      Assert.assertTrue(testObj.FrontBack("ab").equals("ba"));
      
    }
}
