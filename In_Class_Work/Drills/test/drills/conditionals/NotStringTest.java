/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class NotStringTest {
    
    NotString testObj = new NotString();
    
    public NotStringTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //NotString("candy") -> "not candy"
    //NotString("x") -> "not x"
    //NotString("not bad") -> "not bad"
    @Test
    public void noNotInWord(){        
        Assert.assertTrue(testObj.NotString("candy").equals("not candy"));
    }
    
    @Test
    public void noNotWithLetter(){
        Assert.assertTrue(testObj.NotString("x").equals("not x"));
    }
    @Test
    public void notInStart(){
        Assert.assertTrue(testObj.NotString("not bad").equals("not bad"));
    }
}
