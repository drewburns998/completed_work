/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class SoAloneTest {
    SoAlone testObj = new SoAlone();
    public SoAloneTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //SoAlone(13, 99) -> true
    //SoAlone(21, 19) -> true
    //SoAlone(13, 13) -> false
    @Test
    public void firstTeen(){
        Assert.assertTrue(testObj.SoAlone(13, 99));
    }
    
    @Test
    public void secondTeen(){
        Assert.assertTrue(testObj.SoAlone(21, 19));
    }
    
    @Test
    public void bothTeen(){
        Assert.assertFalse(testObj.SoAlone(13, 13));
    }
}
