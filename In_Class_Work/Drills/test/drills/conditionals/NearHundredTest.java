/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class NearHundredTest {
    NearHundred testObj = new NearHundred();
    public NearHundredTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void nearAndAbove100(){
        Assert.assertTrue(testObj.NearHundred(103));
    }
    
    @Test
    public void nearAndBelowAtThreshold(){
        Assert.assertTrue(testObj.NearHundred(90));
    }
    
    @Test
    public void notNearAndBelow100(){
        Assert.assertFalse(testObj.NearHundred(89));
    }
    @Test
    public void nearAndAbove200(){
        Assert.assertTrue(testObj.NearHundred(203));
    }
    @Test
    public void notNearAndBelow200(){
        Assert.assertFalse(testObj.NearHundred(189));
    }
}
