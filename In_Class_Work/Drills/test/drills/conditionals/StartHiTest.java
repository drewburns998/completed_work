/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class StartHiTest {
    StartHi testObj = new StartHi();
    public StartHiTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //StartHi("hi there") -> true
    //StartHi("hi") -> true
    //StartHi("high up") -> false
    @Test
    public void startsWithHi(){
        Assert.assertTrue(testObj.StartHi("hi there"));
    }
    @Test
    public void onlyWordHi(){
        Assert.assertTrue(testObj.StartHi("hi"));
    }
    @Test
    public void startsWithHigh(){
        Assert.assertTrue(testObj.StartHi("high up"));
    }
    
}
