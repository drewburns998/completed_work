/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class EndUpTest {
    EndUp testObj = new EndUp();
    public EndUpTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //EndUp("Hello") -> "HeLLO"
    //EndUp("hi there") -> "hi thERE"
    //EndUp("hi") -> "HI"
    @Test
    public void allLowerCaseWord(){
        Assert.assertTrue(testObj.EndUp("Hello").equals("HeLLO"));
    }
    
    @Test
    public void allLowerCaseTwoWords(){
        Assert.assertTrue(testObj.EndUp("hi there").equals("hi thERE"));
    }
    
    @Test
    public void oneWordTwoLowerCaseLetters(){
        Assert.assertTrue(testObj.EndUp("hi").equals("HI"));
    }
}
