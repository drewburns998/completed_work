/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class PosNegTest {
    PosNeg testObj = new PosNeg();
    public PosNegTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //PosNeg(1, -1, false) -> true
    //PosNeg(-1, 1, false) -> true
    //PosNeg(-4, -5, true) -> true
    @Test
    public void firstNegWithFalseFlag(){
        Assert.assertTrue(testObj.PosNeg(1, -1, false));
    }
    @Test
    public void secondNegWithFalseFlag(){
        Assert.assertTrue(testObj.PosNeg(-1, 1, false));
    }
    @Test
    public void bothNegWithNegFlagTrue(){
        Assert.assertTrue(testObj.PosNeg(-4, -5, true));
    }
    
    @Test
    public void oneNegWithNegFlagTrue(){
        Assert.assertFalse(testObj.PosNeg(4, -5, true));
    }
}
