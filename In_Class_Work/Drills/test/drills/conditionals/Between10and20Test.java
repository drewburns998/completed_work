/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Between10and20Test {
    Between10and20 testObj = new Between10and20();
    public Between10and20Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //Between10and20(12, 99) -> true
    //Between10and20(21, 12) -> true
    //Between10and20(8, 99) -> false
    @Test
    public void firstInsideRange(){
        Assert.assertTrue(testObj.Between10and20(12, 99));
    }
    @Test
    public void secondInsideRange(){
        Assert.assertTrue(testObj.Between10and20(21,12));
    }
    @Test
    public void neitherInsideRange(){
        Assert.assertFalse(testObj.Between10and20(8, 99));
    }
}
