/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class SleepingInTest {
    SleepingIn testObj = new SleepingIn();
    
    public SleepingInTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //sleepIn(false, false) -> true
    //sleepIn(true, false) -> false
    //sleepIn(false, true) -> true
    @Test
    public void weekendNotVacation(){        
        Assert.assertTrue(testObj.CanSleepIn(false, false));
    }
    @Test
    public void weekdayNotVacation(){        
        Assert.assertFalse(testObj.CanSleepIn(true, false));
    }
    @Test
    public void weekendAndVacation(){        
        Assert.assertTrue(testObj.CanSleepIn(false, true));
    }
}
