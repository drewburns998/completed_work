/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class StartOzTest {
    StartOz testObj = new StartOz();
    public StartOzTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //StartOz("ozymandias") -> "oz"
    //StartOz("bzoo") -> "z"
    //StartOz("oxx") -> "o"
    @Test
    public void startWithOz(){
        Assert.assertTrue(testObj.StartOz("ozymandias").equals("oz"));
    }
    
    @Test
    public void startWithBz(){
        Assert.assertTrue(testObj.StartOz("bzoo").equals("z"));
    }
    
    @Test
    public void startWithO(){
        Assert.assertTrue(testObj.StartOz("oxx").equals("o"));
    }
}
