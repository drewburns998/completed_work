/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class IxStartTest {
    IxStart testObj = new IxStart();
    public IxStartTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //IxStart("mix snacks") -> true
    //IxStart("pix snacks") -> true
    //IxStart("piz snacks") -> false
    @Test
    public void tryMix(){
        Assert.assertTrue(testObj.IxStart("mix snacks"));
    }
    
    @Test
    public void tryPix(){
        Assert.assertTrue(testObj.IxStart("pix snacks"));
    }
    
    @Test
    public void tryPiz(){
        Assert.assertFalse(testObj.IxStart("piz snacks"));
    }
}
