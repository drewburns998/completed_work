/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MaxTest {

    Max testObj = new Max();

    public MaxTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //Max(1, 2, 3) -> 3
    //Max(1, 3, 2) -> 3
    //Max(3, 2, 1) -> 3
    @Test
    public void maxAtEnd() {
        Assert.assertTrue(testObj.Max(1, 2, 3) == 3);
    }

    @Test
    public void maxAtMid() {
        Assert.assertTrue(testObj.Max(1, 3, 2) == 3);
    }

    @Test
    public void maxAtStart() {
        Assert.assertTrue(testObj.Max(3, 2, 1) == 3);
    }

}
