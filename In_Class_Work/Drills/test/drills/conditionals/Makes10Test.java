/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Makes10Test {

    Makes10 testObj = new Makes10();

    public Makes10Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void oneValueIs10() {
        Assert.assertTrue(testObj.Makes10(9, 10));
    }

    @Test
    public void neitherValueIs10() {
        Assert.assertFalse(testObj.Makes10(9, 9));
    }

    @Test
    public void sumIs10() {
        Assert.assertTrue(testObj.Makes10(1, 9));
    }
}
