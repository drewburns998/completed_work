/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class Diff21Test {
    Diff21 testObj = new Diff21();
    
    public Diff21Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testOver21(){        
        Assert.assertTrue(testObj.Diff21(23)==4);
    }
    @Test
    public void testUnder21(){        
        Assert.assertTrue(testObj.Diff21(10)==11);
    }
    @Test
    public void test21(){        
        Assert.assertTrue(testObj.Diff21(21)==0);
    }
}
