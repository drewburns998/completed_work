/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class GotETest {

    GotE testObj = new GotE();

    public GotETest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //GotE("Hello") -> true
    //GotE("Heelle") -> true
    //GotE("Heelele") -> false
    @Test
    public void oneE() {
        Assert.assertTrue(testObj.GotE("Hello"));
    }

    @Test
    public void threeE() {
        Assert.assertTrue(testObj.GotE("Heelle"));
    }

    @Test
    public void fourE() {
        Assert.assertFalse(testObj.GotE("Heelele"));
    }
}
