/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class SumDoubleTest {
    SumDouble testObj = new SumDouble();
    
    public SumDoubleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //SumDouble(1, 2) -> 3
    //SumDouble(3, 2) -> 5
    //SumDouble(2, 2) -> 8
    @Test
    public void sumDifferentSmallLarge(){        
        Assert.assertTrue(testObj.SumDouble(1, 2) == 3);
    }
    @Test
    public void sumDifferentLargeSmall(){        
        Assert.assertTrue(testObj.SumDouble(3, 2) == 5);
    }
    @Test
    public void sumSame(){        
        Assert.assertTrue(testObj.SumDouble(2, 2) == 8);
    }
}
