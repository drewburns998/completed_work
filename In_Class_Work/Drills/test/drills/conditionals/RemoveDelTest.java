/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class RemoveDelTest {

    RemoveDel testObj = new RemoveDel();

    public RemoveDelTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //RemoveDel("adelbc") -> "abc"
    //RemoveDel("adelHello") -> "aHello"
    //RemoveDel("adedbc") -> "adedbc"
    @Test
    public void delAtIndex1() {
        Assert.assertTrue(testObj.RemoveDel("adelbc").equals("abc"));
    }

    @Test
    public void delAtIndex1InLongerWords() {
        Assert.assertTrue(testObj.RemoveDel("adelHello").equals("aHello"));
    }
    
    @Test
    public void noDelInString(){
        Assert.assertTrue(testObj.RemoveDel("adedbc").equals("adedbc"));
    }

}
