/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Multiple3Or5Test {
    Multiple3Or5 testObj = new Multiple3Or5();
    public Multiple3Or5Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    //Multiple3or5(3) -> true
    //Multiple3or5(10) -> true
    //Multiple3or5(8) -> false
    @Test
    public void multOf3(){
        Assert.assertTrue(testObj.Multiple3Or5(3));
    }
    @Test
    public void multOf5(){
        Assert.assertTrue(testObj.Multiple3Or5(10));
    }
    @Test
    public void multOfNeither(){
        Assert.assertFalse(testObj.Multiple3Or5(8));
    }

}
