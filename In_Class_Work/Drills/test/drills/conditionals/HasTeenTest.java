/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class HasTeenTest {

    HasTeen testObj = new HasTeen();

    public HasTeenTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //HasTeen(13, 20, 10) -> true
    //HasTeen(20, 19, 10) -> true
    //HasTeen(20, 10, 12) -> false
    @Test
    public void firstTwoInRange() {
        Assert.assertTrue(testObj.HasTeen(13, 20, 10));
    }

    @Test
    public void firstInRange() {
        Assert.assertTrue(testObj.HasTeen(20, 19, 10));
    }
    @Test
    public void noneInRange(){
        Assert.assertFalse(testObj.HasTeen(20, 10, 12));
    }

}
