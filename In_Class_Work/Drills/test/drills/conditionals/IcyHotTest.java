/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class IcyHotTest {

    IcyHot testObj = new IcyHot();

    public IcyHotTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //IcyHot(120, -1) -> true
    //IcyHot(-1, 120) -> true
    //IcyHot(2, 120) -> false
    @Test
    public void bothOutsideRange() {
        Assert.assertTrue(testObj.IcyHot(120, -1));
    }

    @Test
    public void bothOutsideRangeReversed() {
        Assert.assertTrue(testObj.IcyHot(-1, 120));
    }

    @Test
    public void oneOutsideRange() {
        Assert.assertFalse(testObj.IcyHot(2, 120));
    }
}
