/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.conditionals;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class MissingCharTest {
    
    MissingChar testObj = new MissingChar();
    
    public MissingCharTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    //MissingChar("kitten", 1) -> "ktten"
    //MissingChar("kitten", 0) -> "itten"
    //MissingChar("kitten", 4) -> "kittn"
    @Test
    public void testIndex1(){
      
        Assert.assertTrue(testObj.MissingChar("kitten", 1).equals("ktten"));
    }
    @Test
    public void testIndex0(){
      
        Assert.assertTrue(testObj.MissingChar("kitten", 0).equals("itten"));
    }
    @Test
    public void testIndex4(){
      
        Assert.assertTrue(testObj.MissingChar("kitten", 4).equals("kittn"));
    }
}
