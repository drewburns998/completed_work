/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Double23Test {

    Double23 testObj = new Double23();

    public Double23Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //Double23({2, 2, 3}) -> true
    //Double23({3, 4, 5, 3}) -> true
    //Double23({2, 3, 2, 2}) -> false
    @Test
    public void containsTwoTwos() {
        int[] input = {2, 2, 4};
        Assert.assertTrue(testObj.Double23(input));
    }

    @Test
    public void containsTwoThrees() {
        int[] input = {3, 4, 5, 3};
        Assert.assertTrue(testObj.Double23(input));
    }

    @Test
    public void containsThreeTwos() {
        int[] input = {2, 3, 2, 2};
        Assert.assertTrue(testObj.Double23(input));
    }

}
