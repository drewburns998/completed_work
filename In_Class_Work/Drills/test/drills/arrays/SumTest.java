/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SumTest {

    Sum testObj = new Sum();

    public SumTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //Sum({1, 2, 3}) -> 6
    //Sum({5, 11, 2}) -> 18
    //Sum({7, 0, 0}) -> 7
    @Test
    public void testThreeSmallIncreasing() {
        int[] testArray = {1, 2, 3};

        Assert.assertTrue(testObj.Sum(testArray) == 6);
    }
    
    @Test
    public void testThreeNotIncreasing() {
        int[] testArray = {5, 11, 2};

        Assert.assertTrue(testObj.Sum(testArray) == 18);
    }
    
    @Test
    public void testThreeWithZeros() {
        int[] testArray = {7, 0, 0};

        Assert.assertTrue(testObj.Sum(testArray) == 7);
    }
}
