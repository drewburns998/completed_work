/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class GetMiddleTest {

    GetMiddle testObj = new GetMiddle();

    public GetMiddleTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //GetMiddle({1, 2, 3}, {4, 5, 6}) -> {2, 5}
    //GetMiddle({7, 7, 7}, {3, 8, 0}) -> {7, 8}
    //GetMiddle({5, 2, 9}, {1, 4, 5}) -> {2, 4}
    @Test
    public void tryThreeAllIncreasing() {
        int[] input1 = {1, 2, 3};
        int[] input2 = {4, 5, 6};
        int[] answer = {2, 5};

        Assert.assertTrue(Arrays.equals(testObj.GetMiddle(input1, input2), answer));
    }

    @Test
    public void tryThreeRepeatValues() {
        int[] input1 = {7, 7, 7};
        int[] input2 = {3, 8, 0};
        int[] answer = {7, 8};

        Assert.assertTrue(Arrays.equals(testObj.GetMiddle(input1, input2), answer));
    }

    @Test
    public void tryThreeNoPattern() {
        int[] input1 = {5, 2, 9};
        int[] input2 = {1, 4, 5};
        int[] answer = {2, 4};

        Assert.assertTrue(Arrays.equals(testObj.GetMiddle(input1, input2), answer));
    }
}
