/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class HasEvenTest {
    HasEven testObj = new HasEven();
    public HasEvenTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //HasEven({2, 5}) -> true
    //HasEven({4, 3}) -> true
    //HasEven({7, 5}) -> false
    
    @Test
    public void firstIsEven(){
        int[] input = {2,5};
        Assert.assertTrue(testObj.HasEven(input));
    }
    @Test
    public void firstEvenAgain(){
        int[] input = {4,3};
        Assert.assertTrue(testObj.HasEven(input));
    }
    @Test
    public void neitherIsEven(){
        int[] input = {7,5};
        Assert.assertFalse(testObj.HasEven(input));
    }
}
