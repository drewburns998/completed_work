/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class KeepLastTest {

    KeepLast testObj = new KeepLast();

    public KeepLastTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //KeepLast({4, 5, 6}) -> {0, 0, 0, 0, 0, 6}
    //KeepLast({1, 2}) -> {0, 0, 0, 2}
    //KeepLast({3}) -> {0, 3}
    @Test
    public void tryThreeNums() {
        int[] input = {4, 5, 6};
        int[] answer = {0, 0, 0, 0, 0, 6};
        Assert.assertTrue(Arrays.equals(testObj.KeepLast(input), answer));
    }

    @Test
    public void tryTwoNums() {
        int[] input = {1, 2};
        int[] answer = {0, 0, 0, 2};
        Assert.assertTrue(Arrays.equals(testObj.KeepLast(input), answer));
    }

    @Test
    public void tryOneNum() {
        int[] input = {3};
        int[] answer = {0, 3};
        Assert.assertTrue(Arrays.equals(testObj.KeepLast(input), answer));
    }

}
