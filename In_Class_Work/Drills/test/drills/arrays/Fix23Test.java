/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import java.util.Arrays;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Fix23Test {

    Fix23 testObj = new Fix23();

    public Fix23Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //Fix23({1, 2, 3}) ->{1, 2, 0}
    //Fix23({2, 3, 5}) -> {2, 0, 5}
    //Fix23({1, 2, 1}) -> {1, 2, 1}
    @Test
    public void has23AtEnd() {
        int[] input = {1, 2, 3};
        int[] answer = {1, 2, 0};

        Assert.assertTrue(Arrays.equals(testObj.Fix23(input), answer));
    }

    @Test
    public void has23AtStart() {
        int[] input = {2, 3, 5};
        int[] answer = {2, 0, 5};

        Assert.assertTrue(Arrays.equals(testObj.Fix23(input), answer));
    }

    @Test
    public void hasNo23() {
        int[] input = {1, 2, 1};
        int[] answer = {1, 2, 1};
        Assert.assertTrue(Arrays.equals(testObj.Fix23(input), answer));
    }

}
