/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CommonEndTest {

    CommonEnd testObj = new CommonEnd();

    public CommonEndTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
//CommonEnd({1, 2, 3}, {7, 3}) -> true
//CommonEnd({1, 2, 3}, {7, 3, 2}) -> false
//CommonEnd({1, 2, 3}, {1, 3}) -> true

    @Test
    public void testEndEqualWithDiffSize() {
        int[] test1 = {1, 2, 3};
        int[] test2 = {7, 3};

        Assert.assertTrue(testObj.commonEnd(test1, test2));
    }

    @Test
    public void testEndUnequalWithSameSize() {
        int[] test1 = {1, 2, 3};
        int[] test2 = {7, 3, 2};

        Assert.assertFalse(testObj.commonEnd(test1, test2));
    }

    @Test
    public void testBothEndEqualWithDiffSize() {
        int[] test1 = {1, 2, 3};
        int[] test2 = {1, 3};

        Assert.assertTrue(testObj.commonEnd(test1, test2));
    }
}
