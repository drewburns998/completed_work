/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class HigherWinsTest {

    HigherWins testObj = new HigherWins();

    public HigherWinsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //HigherWins({1, 2, 3}) -> {3, 3, 3}
    //HigherWins({11, 5, 9}) -> {11, 11, 11}
    //HigherWins({2, 11, 3}) -> {3, 3, 3}
    @Test
    public void highAtEnd() {
        int[] input = {1, 2, 3};
        int[] output = {3, 3, 3};
        Assert.assertTrue(Arrays.equals(testObj.HigherWins(input), output));
    }

    @Test
    public void highAtStart() {
        int[] input = {11, 5, 9};
        int[] output = {11, 11, 11};
        Assert.assertTrue(Arrays.equals(testObj.HigherWins(input), output));
    }

    @Test
    public void highAtMid() {
        int[] input = {2, 11, 3};
        int[] output = {3, 3, 3};
        Assert.assertTrue(Arrays.equals(testObj.HigherWins(input), output));
    }
}
