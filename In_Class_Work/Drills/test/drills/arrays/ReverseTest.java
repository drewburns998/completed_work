/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertArrayEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class ReverseTest {
    Reverse testObj = new Reverse();
    
    public ReverseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testThreeSmallNumbers(){
        int[] testArray = {1,2,3};
        int[] answerArray = {3,2,1};
        
        assertArrayEquals(answerArray, testObj.Reverse(testArray));
    }
    @Test
    public void testOneNumber(){
        int[] testArray = {1};
        int[] answerArray = {1};
        
        assertArrayEquals(answerArray, testObj.Reverse(testArray));
    }
}
