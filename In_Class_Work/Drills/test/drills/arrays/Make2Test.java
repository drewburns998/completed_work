/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Make2Test {

    Make2 testObj = new Make2();

    public Make2Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //Make2({4, 5}, {1, 2, 3}) -> {4, 5}
    //Make2({4}, {1, 2, 3}) -> {4, 1}
    //Make2({}, {1, 2}) -> {1, 2}
    @Test
    public void firstHasAllValuesNeeded() {
        int[] inputA = {4, 5};
        int[] inputB = {1, 2, 3};
        int[] answer = {4, 5};
        Assert.assertTrue(Arrays.equals(testObj.make2(inputA, inputB), answer));
    }

    @Test
    public void firstHasOneValueNeeded() {
        int[] inputA = {4};
        int[] inputB = {1, 2, 3};
        int[] answer = {4, 1};
        Assert.assertTrue(Arrays.equals(testObj.make2(inputA, inputB), answer));
    }

    @Test
    public void firstIsEmpty() {
        int[] inputA = {};
        int[] inputB = {1, 2};
        int[] answer = {1, 2};
        Assert.assertTrue(Arrays.equals(testObj.make2(inputA, inputB), answer));
    }
}
