/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class RotateLeftTest {

    RotateLeft testObj = new RotateLeft();

    public RotateLeftTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //RotateLeft({1, 2, 3}) -> {2, 3, 1}
    //RotateLeft({5, 11, 9}) -> {11, 9, 5}
    //RotateLeft({7, 0, 0}) -> {0, 0, 7}
    @Test
    public void testThreeSmallConsecutive() {
        int[] testArray = {1, 2, 3};
        int[] answerArray = {2, 3, 1};
        int[] outputArray = testObj.RotateLeft(testArray);

        assertArrayEquals(answerArray, outputArray);
    }

    @Test
    public void testThreeNonConsecutive() {
        int[] testArray = {5, 11, 9};
        int[] answerArray = {11, 9, 5};
        int[] outputArray = testObj.RotateLeft(testArray);

        assertArrayEquals(answerArray, outputArray);
    }
    @Test
    public void testThreeTwoZeros() {
        int[] testArray = {7, 0, 0};
        int[] answerArray = {0, 0, 7};
        int[] outputArray = testObj.RotateLeft(testArray);

        assertArrayEquals(answerArray, outputArray);
    }
}
