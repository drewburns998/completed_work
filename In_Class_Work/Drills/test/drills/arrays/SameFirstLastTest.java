/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class SameFirstLastTest {
    
    SameFirstLast testObj = new SameFirstLast();
    
    public SameFirstLastTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testWithThreeItemsInArrayAndUnequalEndpoints() {
        int[] testArray = {1, 2, 3};        
        Assert.assertFalse(testObj.SameFirstLast(testArray));
    }
    @Test
    public void testWithFourItemsInArrayAndEqualEndpoints() {
        int[] testArray = {1, 2, 3, 1};        
        Assert.assertTrue(testObj.SameFirstLast(testArray));
    }
    
    @Test
    public void testWithThreeItemsInArrayAndEqualEndpoints() {
        int[] testArray = {1, 2, 1};        
        Assert.assertTrue(testObj.SameFirstLast(testArray));
    }
}
