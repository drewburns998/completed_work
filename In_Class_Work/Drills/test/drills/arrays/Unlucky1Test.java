/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Unlucky1Test {

    Unlucky1 testObj = new Unlucky1();

    public Unlucky1Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //Unlucky1({1, 3, 4, 5}) -> true
    //Unlucky1({2, 1, 3, 4, 5}) -> true
    //Unlucky1({1, 1, 1}) -> false
    @Test
    public void unluckyInPos0() {
        int[] input = {1, 3, 4, 5};
        Assert.assertTrue(testObj.Unlucky1(input));
    }

    @Test
    public void unluckyInPos1() {
        int[] input = {2, 1, 3, 4, 5};
        Assert.assertTrue(testObj.Unlucky1(input));
    }

    @Test
    public void noUnluckyInArray() {
        int[] input = {1, 1, 1};
        Assert.assertFalse(testObj.Unlucky1(input));
    }
}
