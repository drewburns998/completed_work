/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class AreInOrderTest {
    AreInOrder testObj = new AreInOrder();
    public AreInOrderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //AreInOrder(1, 2, 4, false) → true
    //AreInOrder(1, 2, 1, false) → false
    //AreInOrder(1, 1, 2, true) → true
    @Test
    public void normalInOrder(){
        Assert.assertTrue(testObj.AreInOrder(1, 2, 4, false));
    }
    @Test
    public void notInNormalInOrder(){
        Assert.assertFalse(testObj.AreInOrder(1, 2, 1, false));
    }
    @Test
    public void bEqualAbutBOkIsTrue(){
        Assert.assertTrue(testObj.AreInOrder(1, 1, 2, true));
    }
}
