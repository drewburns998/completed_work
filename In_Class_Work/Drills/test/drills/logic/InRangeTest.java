/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class InRangeTest {
    
    InRange testObj = new InRange();
    
    public InRangeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //InRange(5, false) → true
    //InRange(11, false) → false
    //InRange(11, true) → true
    @Test
    public void inRangeNotOutMode(){
        Assert.assertTrue(testObj.InRange(5, false));
    }
    @Test
    public void outRangeNotOutMode(){
        Assert.assertFalse(testObj.InRange(11, false));
    }
    @Test
    public void outRangeOutMode(){
        Assert.assertTrue(testObj.InRange(11, true));
    }
}
