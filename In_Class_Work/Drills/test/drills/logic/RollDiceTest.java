/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class RollDiceTest {
    RollDice testObj = new RollDice();
    public RollDiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //RollDice(2, 3, true) → 5
    //RollDice(3, 3, true) → 7
    //RollDice(3, 3, false) → 6
    
    @Test
    public void diffRollsNoDouble(){
        Assert.assertTrue(testObj.RollDice(2, 3, true) == 5);
    }
    
    @Test
    public void sameRollsNoDouble(){
        Assert.assertTrue(testObj.RollDice(3, 3, true) == 7);
    }
    @Test
    public void sameRollsDoubleAllowed(){
        Assert.assertTrue(testObj.RollDice(3, 3, false) == 6);
    }
}
