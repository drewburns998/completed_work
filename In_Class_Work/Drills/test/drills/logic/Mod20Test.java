/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Mod20Test {
    Mod20 testObj = new Mod20();
    public Mod20Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //Mod20(20) → false
    //Mod20(21) → true
    //Mod20(22) → true
    
    @Test
    public void is20(){
        Assert.assertFalse(testObj.Mod20(20));
    }
    @Test
    public void is21(){
        Assert.assertTrue(testObj.Mod20(21));
    }
    @Test
    public void is22(){
        Assert.assertTrue(testObj.Mod20(22));
    }
    
}
