/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class InOrderEqualTest {

    InOrderEqual testObj = new InOrderEqual();

    public InOrderEqualTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //InOrderEqual(2, 5, 11, false) → true
    //InOrderEqual(5, 7, 6, false) → false
    //InOrderEqual(5, 5, 7, true) → true
    @Test
    public void strictInOrder() {
        Assert.assertTrue(testObj.InOrderEqual(2, 5, 11, false));
    }

    @Test
    public void notInOrder() {
        Assert.assertFalse(testObj.InOrderEqual(5, 7, 6, false));
    }
    @Test
    public void inOrderNotStrictly(){
        Assert.assertTrue(testObj.InOrderEqual(5, 5, 7, true));
    }
}
