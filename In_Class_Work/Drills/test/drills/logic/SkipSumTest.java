/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SkipSumTest {
    
    SkipSum testObj = new SkipSum();
    public SkipSumTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //SkipSum(3, 4) → 7
    //SkipSum(9, 4) → 20
    //SkipSum(10, 11) → 21
    @Test
    public void sumBelow10(){
        Assert.assertTrue(testObj.SkipSum(3,4) == 7);
    }
    
    @Test
    public void sumBetween10And19(){
        Assert.assertTrue(testObj.SkipSum(9,4) == 20);
    }
    
    @Test
    public void sumAbove20(){
        Assert.assertTrue(testObj.SkipSum(10,11) == 21);
    }
}
