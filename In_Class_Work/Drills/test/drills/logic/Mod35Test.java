/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Mod35Test {
    Mod35 testObj = new Mod35();
    public Mod35Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //Mod35(3) → true
    //Mod35(10) → true
    //Mod35(15) → false
    @Test
    public void divBy3(){
        Assert.assertTrue(testObj.Mod35(3));
    }
    @Test
    public void divBy5(){
        Assert.assertTrue(testObj.Mod35(10));
    }
    @Test
    public void divBy15(){
        Assert.assertFalse(testObj.Mod35(15));
    }
    
}
