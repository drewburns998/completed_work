/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class SpecialElevenTest {
    SpecialEleven testObj = new SpecialEleven();
    public SpecialElevenTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //SpecialEleven(22) → true
    //SpecialEleven(23) → true
    //SpecialEleven(24) → false
    
    @Test
    public void multOf11(){
        Assert.assertTrue(testObj.SpecialEleven(22));
    }
    
    @Test
    public void oneMoreThanMultOf11(){
        Assert.assertTrue(testObj.SpecialEleven(23));
    }
    
    @Test
    public void notSpecialmultOf11(){
        Assert.assertFalse(testObj.SpecialEleven(24));
    }
}
