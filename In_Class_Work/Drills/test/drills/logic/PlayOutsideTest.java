/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class PlayOutsideTest {
    
    PlayOutside testObj = new PlayOutside();
    public PlayOutsideTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //PlayOutside(70, false) → true
    //PlayOutside(95, false) → false
    //PlayOutside(95, true) → true
    @Test
    public void tempInRangeNotSummer(){
        assertTrue(testObj.PlayOutside(70, false));
    }
    public void tempAboveRangeNotSummer(){
        assertFalse(testObj.PlayOutside(95, false));
    }
    public void tempUpperRangeInSummer(){
        assertTrue(testObj.PlayOutside(95, true));
    }
}
