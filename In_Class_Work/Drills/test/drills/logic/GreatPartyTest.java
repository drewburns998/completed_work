/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class GreatPartyTest {
    
    GreatParty testObj = new GreatParty();
    public GreatPartyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //GreatParty(30, false) → false
    //GreatParty(50, false) → true
    //GreatParty(70, true) → true
    
    @Test
    public void lowCigarsNotWeekend(){
        Assert.assertFalse(testObj.GreatParty(30, false));
    }
    
    @Test
    public void midCigarsNotWeekend(){
        Assert.assertTrue(testObj.GreatParty(50, false));
    }
    
    @Test
    public void highCigarsIsWeekend(){
        Assert.assertTrue(testObj.GreatParty(70, true));
    }
}
