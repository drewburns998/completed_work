/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class AnswerCellTest {
    AnswerCell testObj = new AnswerCell();
    public AnswerCellTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //AnswerCell(false, false, false) → true
    //AnswerCell(false, false, true) → false
    //AnswerCell(true, false, false) → false
    @Test
    public void notMornMomOrSleep(){
        Assert.assertTrue(testObj.AnswerCell(false, false, false));        
    }
    @Test
    public void sleepingNotMornOrMom(){
        Assert.assertFalse(testObj.AnswerCell(false, false, true));        
    }
    @Test
    public void mornNotMomNotSleep(){
        Assert.assertFalse(testObj.AnswerCell(true, false, false));        
    }
}
