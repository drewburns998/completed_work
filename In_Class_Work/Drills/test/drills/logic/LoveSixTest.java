/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class LoveSixTest {

    LoveSix testObj = new LoveSix();

    public LoveSixTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //LoveSix(6, 4) → true
    //LoveSix(4, 5) → false
    //LoveSix(1, 5) → true
    @Test
    public void firstIs6() {
        Assert.assertTrue(testObj.LoveSix(6, 4));
    }

    @Test
    public void neitherIs6NorSum() {
        Assert.assertFalse(testObj.LoveSix(4, 5));
    }

    @Test
    public void sumIs6NeitherIs6() {
        Assert.assertTrue(testObj.LoveSix(1, 5));
    }

}
