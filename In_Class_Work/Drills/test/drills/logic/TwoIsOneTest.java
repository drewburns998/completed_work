/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class TwoIsOneTest {
    TwoIsOne testObj = new TwoIsOne();
    public TwoIsOneTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //TwoIsOne(1, 2, 3) → true
    //TwoIsOne(3, 1, 2) → true
    //TwoIsOne(3, 2, 2) → false
    @Test
    public void first2EqualThird(){
        Assert.assertTrue(testObj.TwoIsOne(1, 2, 3));
    }
    @Test
    public void secondAnd3Equal1(){
        Assert.assertTrue(testObj.TwoIsOne(3, 1, 2));
    }
    @Test
    public void noSumOfThreePossible(){
        Assert.assertFalse(testObj.TwoIsOne(3, 2, 2));
    }
    
}
