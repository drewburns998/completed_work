/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class LastDigitTest {
    LastDigit testObj = new LastDigit();
    public LastDigitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //LastDigit(23, 19, 13) → true
    //LastDigit(23, 19, 12) → false
    //LastDigit(23, 19, 3) → true
    @Test
    public void firstLastHaveSame(){
        Assert.assertTrue(testObj.LastDigit(23, 19, 13));        
    }
    @Test
    public void noneHaveSameOnesDigit(){
        Assert.assertFalse(testObj.LastDigit(23, 19, 12));        
    }
    @Test
    public void firstLastHaveSameLastIsSingleDigit(){
        Assert.assertTrue(testObj.LastDigit(23, 19, 3));        
    }
}
