/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CaughtSpeedingTest {
    
    CaughtSpeeding testObj = new CaughtSpeeding();
    public CaughtSpeedingTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //CaughtSpeeding(60, false) → 0
    //CaughtSpeeding(65, false) → 1
    //CaughtSpeeding(65, true) → 0
    @Test
    public void notSpeedingNotBday(){
        Assert.assertTrue(testObj.CaughtSpeeding(60, false) == 0);
    }
    @Test
    public void speedingNotBday(){
        Assert.assertTrue(testObj.CaughtSpeeding(65, false) == 1);
    }
    @Test
    public void speedingOnBday(){
        Assert.assertTrue(testObj.CaughtSpeeding(65, true) == 0);
    }
    
}
