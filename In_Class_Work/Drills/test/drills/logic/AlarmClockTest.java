/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class AlarmClockTest {
    AlarmClock testObj = new AlarmClock();
    public AlarmClockTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //AlarmClock(1, false) → "7:00"
    //AlarmClock(5, false) → "7:00"
    //AlarmClock(0, false) → "10:00"
    @Test
    public void tuesNotVaca(){
        Assert.assertTrue(testObj.AlarmClock(1, false).equals("7:00"));
    }
    @Test
    public void friNotVaca(){
        Assert.assertTrue(testObj.AlarmClock(5, false).equals("7:00"));
    }
    @Test
    public void monNotVaca(){
        Assert.assertTrue(testObj.AlarmClock(0, false).equals("10:00"));
    }
}
