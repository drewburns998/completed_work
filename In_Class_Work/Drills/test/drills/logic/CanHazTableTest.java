/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.logic;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CanHazTableTest {
    
    CanHazTable testObj = new CanHazTable();
    public CanHazTableTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //CanHazTable(5, 10) → 2
    //CanHazTable(5, 2) → 0
    //CanHazTable(5, 5) → 1
    @Test
    public void oneStylishOtherMaybe(){
        Assert.assertTrue(testObj.CanHazTable(5, 10) == 2);
    }
    @Test
    public void oneMaybeOneNotStylish(){
        Assert.assertTrue(testObj.CanHazTable(5, 2) == 0);
    }
    @Test
    public void bothMaybeStylish(){
        Assert.assertTrue(testObj.CanHazTable(5, 5) == 1);
    }
}
