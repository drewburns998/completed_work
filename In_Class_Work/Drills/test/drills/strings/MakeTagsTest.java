/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class MakeTagsTest {
    MakeTags testObj = new MakeTags();
    public MakeTagsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //MakeTags("i", "Yay") -> "<i>Yay</i>"
    //MakeTags("i", "Hello") -> "<i>Hello</i>"
    //MakeTags("cite", "Yay") -> "<cite>Yay</cite>"
    @Test
    public void test1(){
        Assert.assertTrue(testObj.MakeTags("i", "Yay").equals("<i>Yay</i>"));
    }
    @Test
    public void test2(){
        Assert.assertTrue(testObj.MakeTags("i", "Hello").equals("<i>Hello</i>"));
    }
    @Test
    public void test3(){
        Assert.assertTrue(testObj.MakeTags("cite", "Yay").equals("<cite>Yay</cite>"));
    }
}
