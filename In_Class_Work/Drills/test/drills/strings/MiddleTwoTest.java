/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MiddleTwoTest {
    MiddleTwo testObj = new MiddleTwo();
    public MiddleTwoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //MiddleTwo("string") -> "ri"
    //MiddleTwo("code") -> "od"
    //MiddleTwo("Practice") -> "ct"
    @Test
    public void lowCaseMedWord(){
        Assert.assertTrue(testObj.MiddleTwo("string").equals("ri"));
    }
    
    @Test
    public void lowCaseSmallWord(){
        Assert.assertTrue(testObj.MiddleTwo("code").equals("od"));
    }
    
    @Test
    public void upCaseLongWord(){
        Assert.assertTrue(testObj.MiddleTwo("Practice").equals("ct"));
    }
}
