/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FrontAgainTest {
    FrontAgain testObj = new FrontAgain();
    public FrontAgainTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //FrontAgain("edited") -> true
    //FrontAgain("edit") -> false
    //FrontAgain("ed") -> true
    @Test
    public void frontMatchesBack(){
        Assert.assertTrue(testObj.FrontAgain("edited"));
    }
    @Test
    public void frontDoesntMatchBack(){
        Assert.assertFalse(testObj.FrontAgain("edit"));
    }
    @Test
    public void lengthTwo(){
        Assert.assertTrue(testObj.FrontAgain("ed"));
    }
}
