/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FrontAndBackTest {
    FrontAndBack testObj = new FrontAndBack();
    public FrontAndBackTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //FrontAndBack("Hello", 2) -> "Helo"
    //FrontAndBack("Chocolate", 3) -> "Choate"
    //FrontAndBack("Chocolate", 1) -> "Ce"
    @Test
    public void test2N(){
        Assert.assertTrue(testObj.FrontAndBack("Hello", 2).equals("Helo"));
    }
    @Test
    public void test3N(){
        Assert.assertTrue(testObj.FrontAndBack("Chocolate", 3).equals("Choate"));
    }
    @Test
    public void test1N(){
        Assert.assertTrue(testObj.FrontAndBack("Chocolate", 1).equals("Ce"));
    }
    
}
