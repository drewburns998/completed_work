/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class TweakFrontTest {
    TweakFront testObj = new TweakFront();
    public TweakFrontTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //TweakFront("Hello") -> "llo"
    //TweakFront("away") -> "aay"
    //TweakFront("abed") -> "abed"
    @Test
    public void normalWord(){
        Assert.assertTrue(testObj.TweakFront("Hello").equals("llo"));
    }
    @Test
    public void startsWithA(){
        Assert.assertTrue(testObj.TweakFront("away").equals("aay"));
    }
    @Test
    public void startsWithAB(){
        Assert.assertTrue(testObj.TweakFront("abed").equals("abed"));
    }
}
