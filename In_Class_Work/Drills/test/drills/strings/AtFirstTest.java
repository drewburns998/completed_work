/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class AtFirstTest {
    AtFirst testObj = new AtFirst();
    public AtFirstTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //AtFirst("hello") -> "he"
    //AtFirst("hi") -> "hi"
    //AtFirst("h") -> "h@"
    
    @Test
    public void greaterThanTwoChars(){
        Assert.assertTrue(testObj.AtFirst("hello").equals("he"));
    }
    @Test
    public void exactlyTwoChars(){
        Assert.assertTrue(testObj.AtFirst("hi").equals("hi"));
    }
    @Test
    public void lessThanTwoChars(){
        Assert.assertTrue(testObj.AtFirst("h").equals("h@"));
    }
}
