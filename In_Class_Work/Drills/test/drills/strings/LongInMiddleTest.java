/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class LongInMiddleTest {
    LongInMiddle testObj = new LongInMiddle();
    public LongInMiddleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //LongInMiddle("Hello", "hi") -> "hiHellohi"
    //LongInMiddle("hi", "Hello") -> "hiHellohi"
    //LongInMiddle("aaa", "b") -> "baaab"
    
    @Test
    public void tryLongThenShort(){
        Assert.assertTrue(testObj.LongInMiddle("Hello","hi").equals("hiHellohi"));
    }
    
    @Test
    public void tryShortThenLong(){
        Assert.assertTrue(testObj.LongInMiddle("hi","Hello").equals("hiHellohi"));
    }
    
    @Test
    public void tryShortThenShorter(){
        Assert.assertTrue(testObj.LongInMiddle("aaa","b").equals("baaab"));
    }
}
