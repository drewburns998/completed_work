/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class TakeOneTest {
    TakeOne testObj = new TakeOne();
    public TakeOneTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //TakeOne("Hello", true) -> "H"
    //TakeOne("Hello", false) -> "o"
    //TakeOne("oh", true) -> "o"
    
    @Test
    public void tryFrontLetterReturn(){
        Assert.assertTrue(testObj.TakeOne("Hello", true).equals("H"));
    }
    
    @Test
    public void tryBackLetterReturn(){
        Assert.assertTrue(testObj.TakeOne("Hello", false).equals("o"));
    }
    
    @Test
    public void trySmallWordFrontReturn(){
        Assert.assertTrue(testObj.TakeOne("oh", true).equals("o"));
    }
}
