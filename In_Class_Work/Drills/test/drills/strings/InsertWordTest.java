/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class InsertWordTest {
    InsertWord testObj = new InsertWord();
    public InsertWordTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //InsertWord("<<>>", "Yay") -> "<<Yay>>"
    //InsertWord("<<>>", "WooHoo") -> "<<WooHoo>>"
    //InsertWord("[[]]", "word") -> "[[word]]"
    @Test
    public void testArrowsShort(){
        Assert.assertTrue(testObj.InsertWord("<<>>", "Yay").equals("<<Yay>>"));
    }
    @Test
    public void testArrowsMed(){
        Assert.assertTrue(testObj.InsertWord("<<>>", "WooHoo").equals("<<WooHoo>>"));
    }
    @Test
    public void testBracketsShort(){
        Assert.assertTrue(testObj.InsertWord("[[]]", "word").equals("[[word]]"));
    }
}
