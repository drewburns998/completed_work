/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class TakeTwoFromPositionTest {

    TakeTwoFromPosition testObj = new TakeTwoFromPosition();

    public TakeTwoFromPositionTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //TakeTwoFromPosition("java", 0) -> "ja"
    //TakeTwoFromPosition("java", 2) -> "va"
    //TakeTwoFromPosition("java", 3) -> "ja"
    @Test
    public void checkFromStartingPos() {
        Assert.assertTrue(testObj.TakeTwoFromPosition("java", 0).equals("ja"));
    }

    @Test
    public void checkFromFinalTwoPos() {
        Assert.assertTrue(testObj.TakeTwoFromPosition("java", 2).equals("va"));
    }

    @Test
    public void checkOverlappingEnd() {
        Assert.assertTrue(testObj.TakeTwoFromPosition("java", 3).equals("ja"));
    }
}
