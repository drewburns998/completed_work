/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SayHiTest {
    SayHi testObj = new SayHi();
    public SayHiTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //SayHi("Bob") -> "Hello Bob!"
    //SayHi("Alice") -> "Hello Alice!"
    //SayHi("X") -> "Hello X!"
    @Test
    public void smallName(){
        Assert.assertTrue(testObj.SayHi("Bob").equals("Hello Bob"));
    }
    @Test
    public void medName(){
        Assert.assertTrue(testObj.SayHi("Alice").equals("Hello Alice"));
    }
    @Test
    public void shortName(){
        Assert.assertTrue(testObj.SayHi("X").equals("Hello X"));
    }
    
    
}
