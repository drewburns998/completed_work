/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class HadBadTest {
    HasBad testObj = new HasBad();
    public HadBadTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //HasBad("badxx") -> true
    //HasBad("xbadxx") -> true
    //HasBad("xxbadxx") -> false
    @Test
    public void badStartAt0(){
        Assert.assertTrue(testObj.HasBad("badxx"));
    }
    @Test
    public void badStartAt1(){
        Assert.assertTrue(testObj.HasBad("xbadxx"));
    }
    @Test
    public void badStartAt2(){
        Assert.assertFalse(testObj.HasBad("xxbadxx"));
    }
}
