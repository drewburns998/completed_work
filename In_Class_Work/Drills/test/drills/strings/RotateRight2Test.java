/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class RotateRight2Test {

    RotateRight2 testObj = new RotateRight2();

    public RotateRight2Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //RotateRight2("Hello") -> "loHel"
    //RotateRight2("java") -> "vaja"
    //RotateRight2("Hi") -> "Hi"
    
    @Test
    public void tryUpperCaseMedWord(){
        Assert.assertTrue(testObj.RotateRight2("Hello").equals("loHel"));
    }
    
    @Test
    public void tryLowCaseMedWord(){
        Assert.assertTrue(testObj.RotateRight2("java").equals("vaja"));
    }
    
    @Test
    public void tryUpperSmallMedWord(){
        Assert.assertTrue(testObj.RotateRight2("Hi").equals("Hi"));
    }
}
