/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ConCatTest {
    ConCat testObj = new ConCat();
    public ConCatTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //ConCat("abc", "cat") -> "abcat"
    //ConCat("dog", "cat") -> "dogcat"
    //ConCat("abc", "") -> "abc"
    @Test
    public void endWord1EqualsStartWord2(){
        Assert.assertTrue(testObj.ConCat("abc", "cat").equals("abcat"));
    }
    @Test
    public void noCommonLetterJoin(){
        Assert.assertTrue(testObj.ConCat("dog", "cat").equals("dogcat"));
    }
    @Test
    public void stringPlusBlank(){
        Assert.assertTrue(testObj.ConCat("abc", "").equals("abc"));
    }
    
    
}
