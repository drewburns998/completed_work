/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class TrimOneTest {
    TrimOne testObj = new TrimOne();
    public TrimOneTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //TrimOne("Hello") -> "ell"
    //TrimOne("java") -> "av"
    //TrimOne("coding") -> "odin"
    @Test
    public void testMedWord(){
        Assert.assertTrue(testObj.TrimOne("Hello").equals("ell"));
    }
    
    @Test
    public void testSmallWordLowerCase(){
        Assert.assertTrue(testObj.TrimOne("java").equals("av"));
    }
    @Test
    public void testMedWordLowerCase(){
        Assert.assertTrue(testObj.TrimOne("coding").equals("odin"));
    }
}
