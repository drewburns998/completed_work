/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class AbbaTest {

    Abba testObj = new Abba();

    public AbbaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    //Abba("Hi", "Bye") -> "HiByeByeHi"
    //Abba("Yo", "Alice") -> "YoAliceAliceYo"
    //Abba("What", "Up") -> "WhatUpUpWhat"
    @Test
    public void test1() {
        Assert.assertTrue(testObj.Abba("Hi", "Bye").equals("HiByeByeHi"));
    }

    @Test
    public void test2() {
        Assert.assertTrue(testObj.Abba("Yo", "Alice").equals("YoAliceAliceYo"));
    }

    @Test
    public void test3() {
        Assert.assertTrue(testObj.Abba("What", "Up").equals("WhatUpUpWhat"));
    }
}
