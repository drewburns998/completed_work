/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class LastCharsTest {
    LastChars testObj = new LastChars();
    public LastCharsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //LastChars("last", "chars") -> "ls"
    //LastChars("yo", "mama") -> "ya"
    //LastChars("hi", "") -> "h@"
    @Test
    public void twoLongerWords(){
      Assert.assertTrue(testObj.LastChars("last", "chars").equals("ls"));
    }
    @Test
    public void oneShortOneLongWord(){
      Assert.assertTrue(testObj.LastChars("yo", "mama").equals("ya"));
    }
    @Test
    public void oneShortOneBlankWord(){
      Assert.assertTrue(testObj.LastChars("hi", "").equals("h@"));
    }
}
