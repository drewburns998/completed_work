/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MinCatTest {
    MinCat testObj = new MinCat();
    public MinCatTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //MinCat("Hello", "Hi") -> "loHi"
    //MinCat("Hello", "java") -> "ellojava"
    //MinCat("java", "Hello") -> "javaello"
    @Test
    public void firstLongSecondShorter(){
        Assert.assertTrue(testObj.MinCat("Hello","Hi").equals("loHi"));
    }
    @Test
    public void firstLongSecondShort(){
        Assert.assertTrue(testObj.MinCat("Hello","java").equals("ellojava"));
    }
    @Test
    public void firstShortSecondLonger(){
        Assert.assertTrue(testObj.MinCat("java","Hello").equals("javaello"));
    }
}
