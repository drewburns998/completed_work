/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class StripXTest {
    StripX testObj = new StripX();
    public StripXTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //StripX("xHix") -> "Hi"
    //StripX("xHi") -> "Hi"
    //StripX("Hxix") -> "Hxi"
    @Test
    public void xAtStartAndEnd(){
        Assert.assertTrue(testObj.StripX("xHix").equals("Hi"));
    }
    @Test
    public void xAtStart(){
        Assert.assertTrue(testObj.StripX("xHi").equals("Hi"));
    }
    @Test
    public void xAtEnd(){
        Assert.assertTrue(testObj.StripX("Hxix").equals("Hxi"));
    }
    
}
