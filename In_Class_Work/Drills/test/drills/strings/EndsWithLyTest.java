/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class EndsWithLyTest {
    EndsWithLy testObj = new EndsWithLy();
    public EndsWithLyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //EndsWithLy("oddly") -> true
    //EndsWithLy("y") -> false
    //EndsWithLy("oddy") -> false
    
    @Test
    public void hasLyAtEnd(){
        Assert.assertTrue(testObj.EndsWithLy("oddly"));
    }
    
    @Test
    public void noLyAtEnd(){
        Assert.assertFalse(testObj.EndsWithLy("y"));
    }
    
    @Test
    public void hasYAtEnd(){
        Assert.assertFalse(testObj.EndsWithLy("oddy"));
    }
}
