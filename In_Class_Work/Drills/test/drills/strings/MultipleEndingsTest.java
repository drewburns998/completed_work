/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MultipleEndingsTest {
    MultipleEndings testObj = new MultipleEndings();
    public MultipleEndingsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //MultipleEndings("Hello") -> "lololo"
    //MultipleEndings("ab") -> "ababab"
    //MultipleEndings("Hi") -> "HiHiHi"
    
    @Test
    public void medSizeWord(){
        Assert.assertTrue(testObj.MultipleEndings("Hello").equals("lololo"));
    }
    @Test
    public void smallLowerCaseWord(){
        Assert.assertTrue(testObj.MultipleEndings("ab").equals("ababab"));
    }
    @Test
    public void smallWordWithCapitalLetter(){
        Assert.assertTrue(testObj.MultipleEndings("Hi").equals("HiHiHi"));
    }
}
