/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class RotateLeft2Test {
    RotateLeft2 testObj = new RotateLeft2();
    public RotateLeft2Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //Rotateleft2("Hello") -> "lloHe"
    //Rotateleft2("java") -> "vaja"
    //Rotateleft2("Hi") -> "Hi"
    @Test
    public void tryCapitalLetter(){
        Assert.assertTrue(testObj.Rotateleft2("Hello").equals("lloHe"));
    }
    @Test
    public void trySmallNoCaps(){
        Assert.assertTrue(testObj.Rotateleft2("java").equals("vaja"));
    }
    @Test
    public void trySmallWithCapital(){
        Assert.assertTrue(testObj.Rotateleft2("Hi").equals("Hi"));
    }
    
    
}
