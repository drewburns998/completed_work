/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class FirstHalfTest {
    FirstHalf testObj = new FirstHalf();
    public FirstHalfTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //FirstHalf("WooHoo") -> "Woo"
    //FirstHalf("HelloThere") -> "Hello"
    //FirstHalf("abcdef") -> "abc"
    @Test
    public void testCapitalMedWord(){
        Assert.assertTrue(testObj.FirstHalf("WooHoo").equals("Woo"));
    }
    
    @Test
    public void testCapitalTwoWord(){
        Assert.assertTrue(testObj.FirstHalf("HelloThere").equals("Hello"));
    }
    
    @Test
    public void testStringOfLetters(){
        Assert.assertTrue(testObj.FirstHalf("abcdef").equals("abc"));
    }
}
