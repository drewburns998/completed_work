/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.interestcalcweb;

import java.text.DecimalFormat;

/**
 *
 * @author apprentice
 */
public class InterestPeriod {
    private DecimalFormat df = new DecimalFormat("#.00");
    private Double currentBalance, accruedInterest, yearlyInterest;
    private int year;
    public InterestPeriod(Double currentBalance, Double accruedInterest, Double yearlyInterest, int year) {
        this.currentBalance = Double.valueOf(df.format(currentBalance));
        this.accruedInterest = Double.valueOf(df.format(accruedInterest));
        this.yearlyInterest = Double.valueOf(df.format(yearlyInterest));
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    
    public Double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public Double getAccruedInterest() {
        return accruedInterest;
    }

    public void setAccruedInterest(Double accruedInterest) {
        this.accruedInterest = accruedInterest;
    }

    public Double getYearlyInterest() {
        return yearlyInterest;
    }

    public void setYearlyInterest(Double yearlyInterest) {
        this.yearlyInterest = yearlyInterest;
    }
    
    
    
}
