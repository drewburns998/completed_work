/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.interestcalcweb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "InterestCalcServlet", urlPatterns = {"/InterestCalcServlet"})
public class InterestCalcServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String dollarAmountString = request.getParameter("dollarAmount");
        String interestRateString = request.getParameter("interestRate");
        String yearsMaturityString = request.getParameter("yearsMaturity");
        String paymentPeriodsString = request.getParameter("paymentPeriods");
        Double currentBalance;
        ArrayList<InterestPeriod> yearTotals = new ArrayList<>();

        if (dollarAmountString == null || interestRateString == null || yearsMaturityString == null || paymentPeriodsString == null) {
            request.setAttribute("badInput", true);
        } else {
            try {
                double dollarAmount = Double.parseDouble(dollarAmountString);
                double interestRate = Double.parseDouble(interestRateString);
                double yearsMaturity = Double.parseDouble(yearsMaturityString);
                double paymentPeriods = Double.parseDouble(paymentPeriodsString);

                if (dollarAmount < 0 || interestRate < 0 || yearsMaturity <= 0 || paymentPeriods < 0) {
                    request.setAttribute("badInput", true);
                } else {

                    currentBalance = dollarAmount;
                    double yearlyInterest = 0;
                    double accruedInterest = 0;
                    for (int i = 1; i <= yearsMaturity; i++) {
                        for (int j = 1; j <= paymentPeriods; j++) {
                            accruedInterest = ((interestRate / paymentPeriods) / 100) * currentBalance;
                            currentBalance = currentBalance + accruedInterest;
                            yearlyInterest = yearlyInterest + accruedInterest;
                        }
                        yearTotals.add(new InterestPeriod(currentBalance, accruedInterest, yearlyInterest, i));
                    }
                    request.setAttribute("moneyList", yearTotals);
                }

            } catch (NumberFormatException ex) {
                request.setAttribute("badInput", true);
            }
        }
        request.getRequestDispatcher("result.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
