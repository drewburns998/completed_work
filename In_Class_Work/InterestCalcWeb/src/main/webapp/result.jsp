<%-- 
    Document   : result
    Created on : Mar 28, 2016, 7:55:33 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <style type="text/css">
            
            #tableid{
                width: auto;
            }

        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Interest Rate Results</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" />
    </head>
    <body>
        
        <h1>Here are the results:</h1>
        <c:if test="${badInput==true}">
            Sorry, you entered some bad data!
            <a href="index.jsp">Return to Flooring Form.</a>
        </c:if>
        <table id="tableid" class="table table-bordered">
            <c:forEach items="${moneyList}" var="yearlyStatement">

                <tr>
                    <td>Year:&nbsp;<c:out value="${yearlyStatement.year}" default="n/a" /></td>
                    <td>Balance:&nbsp;$<c:out value="${yearlyStatement.currentBalance}" default="n/a" /></td>
                    <td>Accrued Interest (YTD):&nbsp;$<c:out value="${yearlyStatement.accruedInterest}" default="n/a" /></td>
                    <td>Total Accrued Interest:&nbsp;$<c:out value="${yearlyStatement.yearlyInterest}" default="n/a"/></td>
                </tr>

            </c:forEach>
        </table>
        <br/>
    </body>
</html>
