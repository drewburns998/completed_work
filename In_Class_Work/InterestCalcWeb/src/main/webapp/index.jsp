<%-- 
    Document   : index
    Created on : Mar 28, 2016, 7:55:22 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <title>Simple Interest Rate Calculator</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" />
</head>
<body>
    <div>
        <h1 align="left">Simple Interest Rate Calculator</h1>
    </div>
    <h3>Quarterly Compounding</h3>
    <form role="form" method="POST" action="InterestCalcServlet">
        <div class="col-xs-12 col-md-2">
            <div class="form-group">                                
                <input type="number" placeholder="$ Amount Invested" min=0 class="form-control" name="dollarAmount">
                <input type="text" placeholder="Interest Rate (% APR)" min=0 class="form-control" name="interestRate">
                <input type="number" placeholder="Years to Invest" min=0 class="form-control" name="yearsMaturity">
                <input type="number" placeholder="Payments Per Year" min=0 class="form-control" name="paymentPeriods">
            </div>        
            <button type="submit" class="btn btn-default">Calculate</button>
        </div>
    </form>


</body>
</html>