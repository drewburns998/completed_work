/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rockpaperscissors;

import Game.Game;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class RPSApp implements Game {

    @Override
    public void run() {
        int myChoice = 0;
        Random randomChoice = new Random();
        int compChoice;
        int numRounds = 0;
        int userWins = 0;
        int compWins = 0;
        int tieGames = 0;
        int keepPlaying = 1; //1 keep playing, any other number quits

        // while loop keeps track if the player wishes to play more iterations
        while (keepPlaying == 1) {
            //reset wins to 0 so that playing repeatedly does not add wins
            //ties and losses across different rounds
            userWins = 0;
            compWins = 0;
            tieGames = 0;

            Scanner sc = new Scanner(System.in);
            System.out.print("How many rounds will you play? ");
            if (sc.hasNextInt()) {
                numRounds = sc.nextInt();
                if (numRounds < 1 || numRounds > 10) {
                    System.out.println("you picked a number outside of 1 - 10");
                    return;
                }
            } else {
                System.out.println("Wrong input, between 1 and 10 rounds inclusive");
                return;
            }

            while (numRounds > 0) {
                compChoice = randomChoice.nextInt(3) + 1;
                System.out.print("Please enter 1:rock, 2:paper, 3:scissors ");

                if (sc.hasNextInt()) {
                    myChoice = sc.nextInt();
                } else {
                    System.out.println("Wrong input, play again using 1 2 or 3");
                    return;
                }

                switch (myChoice) {
                    case 1:
                        switch (compChoice) {
                            case 1:
                                tieGames = isTie(tieGames);
                                break;
                            case 2:
                                compWins = isLoss(compWins);
                                break;
                            case 3:
                                userWins = isWin(userWins);
                                break;
                        }
                        numRounds = numRounds - 1;
                        break;
                    case 2:
                        switch (compChoice) {
                            case 1:
                                userWins = isWin(userWins);
                                break;
                            case 2:
                                tieGames = isTie(tieGames);
                                break;
                            case 3:
                                compWins = isLoss(compWins);
                                break;
                        }
                        numRounds = numRounds - 1;
                        break;
                    case 3:
                        switch (compChoice) {
                            case 1:
                                compWins = isLoss(compWins);
                                break;
                            case 2:
                                userWins = isWin(userWins);
                                break;
                            case 3:
                                tieGames = isTie(tieGames);
                                break;
                        }
                        numRounds = numRounds - 1;
                        break;
                }
            }
            matchSummary(userWins, compWins, tieGames);
            whoWon(userWins, compWins);

            System.out.print("Enter 1 to play again or 0 to quit: ");
            keepPlaying = sc.nextInt();

        }
        System.out.println("Thanks for playing!");
    }

    public static void matchSummary(int wins, int losses, int ties) {
        System.out.println("You won " + wins + " game(s) these rounds");
        System.out.println("Computer won " + losses + " game(s) these rounds");
        System.out.println("total ties: " + ties + " game(s) these rounds");
    }

    public static void whoWon(int wins, int losses) {
        if (wins > losses) {
            System.out.println("You beat the computer!");
        } else if (wins < losses) {
            System.out.println("The computer beat you!");
        } else {
            System.out.println("You tied overall with the computer");
        }
    }

    public static int isTie(int ties) {
        System.out.println("You Tie!");
        return ties + 1;
    }

    public static int isWin(int wins) {
        System.out.println("You Win!");
        return wins + 1;
    }

    public static int isLoss(int loss) {
        System.out.println("You Lose!");
        return loss + 1;

    }

    @Override
    public String getGameName() {
        return "Rock Paper Scissors";
    }

}
