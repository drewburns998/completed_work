/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luckysevens;

import Game.Game;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class LuckySevensApp implements Game {

    @Override
    public void run() {
        Scanner sc = new Scanner(System.in);
        int initialMoney = 0;

        System.out.println("Please input the starting bet in full dollars: ");
        
        //check if input is valid int
        if (sc.hasNextInt()) {
            System.out.println("Success!!");
            initialMoney = sc.nextInt();
        } else {
            System.out.println("Wrong input, please enter a positive whole dollar amount "
                    + "the next time you play!");
            return;
        }
        
        if(initialMoney < 1){
            System.out.println("You need money to play! Bet with at least 1 dollar!");
            return;
        }
        int currentMoney = initialMoney;
        int diceSum = 0;
        int totalRollCounter = 0;
        int maxRound = 0;
        int maxMoney = initialMoney;

        //keep playing while you have money
        while (currentMoney > 0) {
            Random random1 = new Random();
            int dice1 = random1.nextInt(5) + 1;
            Random random2 = new Random();
            int dice2 = random2.nextInt(5) + 1;
            totalRollCounter = totalRollCounter + 1;
            diceSum = dice1 + dice2;

            if (diceSum == 7) {
                currentMoney = currentMoney + 4;
            } else {
                currentMoney = currentMoney - 1;
            }
            if (currentMoney > maxMoney) {
                maxMoney = currentMoney;
                maxRound = totalRollCounter;
            }
        }
        System.out.println("The game has ended");
        System.out.println("The max money is " + maxMoney);
        System.out.println("The max money round is " + maxRound);
        System.out.println("The total rounds played was " + totalRollCounter);        
        
    }

    @Override
    public String getGameName() {
        return "Lucky Sevens";
    }
    
}
