/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gamebot;

import Game.Game;
import blackjack.BlackJackApp;
import gamebot.ui.ConsoleIO;
import java.util.ArrayList;
import luckysevens.LuckySevensApp;
import rockpaperscissors.RPSApp;

/**
 *
 * @author apprentice
 */
public class GameBot {

    ConsoleIO io = new ConsoleIO();
    ArrayList<Game> gameList = new ArrayList<>();
    
    public void run(){
        addGame();
        boolean keepRunning = true;
        
        while(keepRunning){
            io.print("Selection:");
            int choice = 1; //default choice
            
            for(Game g : gameList){
                io.print(choice + " : " + g.getGameName());
                choice++;
            }
            
            int userChoice = io.readInt("Enter the number of the game to play"
                    + ".  Or, press " + (gameList.size()+1) 
                    + " to exit.", 1, gameList.size()+1);
            
            if (userChoice == (gameList.size() + 1)){
                keepRunning = false;
            } else{
                io.print(gameList.get(userChoice - 1).getGameName() + " has started!");
                gameList.get(userChoice - 1).run();
                
                String contPlay = io.readString("\n"+gameList.get(userChoice - 1).getGameName() + " has ended.  "
                        + "Would you like to play another game? (yes or no)");
                
                if(contPlay.equalsIgnoreCase("no")){
                    keepRunning = false;
                }
            }
        }
    }
    
    public void addGame() {
        Game bj = new BlackJackApp();
        gameList.add(bj);
        
        Game rps = new RPSApp();
        gameList.add(rps);

        Game luck7 = new LuckySevensApp();
        gameList.add(luck7);
        
    }
    
}
