/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.contactlistmvc.controller;

import com.swcguild.contactlistmvc.dao.ContactListDao;
import com.swcguild.contactlistmvc.model.Contact;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@Controller
public class HomeController {
    
    private ContactListDao dao;
    
    @Inject
    public HomeController(ContactListDao dao){
        this.dao = dao;
    }
    
    
    @RequestMapping(value={"/","/home"}, method=RequestMethod.GET)
    public String displayHomePage(){
        return "home";
    }
    
    @RequestMapping(value={"/contact/{contactId}"}, method=RequestMethod.GET)
    @ResponseBody
    public Contact getTheContact(@PathVariable("contactId")int id){
        return dao.getContactById(id);        
    }
    
    @RequestMapping(value={"/contact"}, method=RequestMethod.POST)
    @ResponseBody @ResponseStatus(HttpStatus.CREATED)    
    public Contact createTheContact(@Valid @RequestBody Contact newContact){
        dao.addContact(newContact);        
        return newContact;
    }
    
    @RequestMapping(value={"/contact/{contactId}"}, method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTheContact(@PathVariable("contactId")int contactId){
        dao.removeContact(contactId);
    }
    
    @RequestMapping(value="/contact/{cId}", method=RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateTheContact(@PathVariable("cId")int id, @Valid @RequestBody Contact edittedContact){        
        edittedContact.setContactId(id);
        dao.updateContact(edittedContact);
    }
    
    @RequestMapping(value="/contacts", method=RequestMethod.GET)
    @ResponseBody
    public List<Contact> getAllTheContact(){
        return dao.getAllContacts();
    }
}
