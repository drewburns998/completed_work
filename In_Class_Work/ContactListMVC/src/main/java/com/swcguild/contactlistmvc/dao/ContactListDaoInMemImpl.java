/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.contactlistmvc.dao;

import com.swcguild.contactlistmvc.model.Contact;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class ContactListDaoInMemImpl implements ContactListDao {

    private Map<Integer, Contact> contactMap = new HashMap<>();
    private static int contactIdCounter = 0;
    
    public ContactListDaoInMemImpl(){
        Contact contact = new Contact();
        contact.setFirstName("Winston");
        contact.setLastName("Churchill");
        contact.setCompany("Match.com");
        contact.setPhone("222-2222");
        contact.setEmail("winnie@notpooh.com");
        this.addContact(contact);
        
        contact = new Contact();
        contact.setFirstName("Bart");
        contact.setLastName("Simpson");
        contact.setCompany("Student");
        contact.setPhone("5558888");
        contact.setEmail("homer@notpooh.com");
        this.addContact(contact);
        
        contact = new Contact();
        contact.setFirstName("Turd");
        contact.setLastName("Furgeson");
        contact.setCompany("Plain Old JavaScript Objects");
        contact.setPhone("555-5555");
        contact.setEmail("javaturdlet@gmail.com");
        this.addContact(contact);
        
        contact = new Contact();
        contact.setFirstName("Sarah");
        contact.setLastName("Dukie");
        contact.setCompany("The Software Craftsmanship Guild");
        contact.setPhone("684-0000");
        contact.setEmail("sfunnygirl@gmail.com");
        this.addContact(contact);
        
        contact = new Contact();
        contact.setFirstName("Keyboard");
        contact.setLastName("Warrior");
        contact.setCompany("Can we pass");
        contact.setPhone("684-0000");
        contact.setEmail("LEETH4xOR@urmom.com");
        this.addContact(contact);
        
        
        
    }

    @Override
    public Contact addContact(Contact contact) {
        contact.setContactId(contactIdCounter);
        contactIdCounter++;
        contactMap.put(contact.getContactId(), contact);
        return contact;
    }

    @Override
    public void removeContact(int contactId) {
        contactMap.remove(contactId);
    }

    @Override
    public void updateContact(Contact contact) {
        contactMap.put(contact.getContactId(), contact);
    }

    @Override
    public List<Contact> getAllContacts() {
        Collection<Contact> c = contactMap.values();
        return new ArrayList(c);
    }

    @Override
    public Contact getContactById(int contactId) {
        return contactMap.get(contactId);
    }

    @Override
    public List<Contact> searchContacts(Map<SearchTerm, String> criteria) {
        //get all criteria
        String firstNameCriteria = criteria.get(SearchTerm.FIRST_NAME);
        String lastNameCriteria = criteria.get(SearchTerm.LAST_NAME);
        String companyCriteria = criteria.get(SearchTerm.COMPANY);
        String phoneCriteria = criteria.get(SearchTerm.PHONE);
        String emailCriteria = criteria.get(SearchTerm.EMAIL);

        // Declare the predicates
        Predicate<Contact> firstNamePred;
        Predicate<Contact> lastNamePred;
        Predicate<Contact> companyPred;
        Predicate<Contact> phonePred;
        Predicate<Contact> emailPred;

        // Declare & initialize an "all pass" predicate filter
        Predicate<Contact> allPass = (contact) -> {return true;};
        
        firstNamePred = (firstNameCriteria == null || firstNameCriteria.isEmpty()) ?
                allPass : (contact) -> contact.getFirstName().toLowerCase().contains(firstNameCriteria.toLowerCase());
        lastNamePred = (lastNameCriteria == null || lastNameCriteria.isEmpty()) ?
                allPass : (contact) -> contact.getLastName().toLowerCase().contains(lastNameCriteria.toLowerCase());
        companyPred = (companyCriteria == null || companyCriteria.isEmpty()) ?
                allPass : (contact) -> contact.getCompany().toLowerCase().contains(companyCriteria.toLowerCase());
        phonePred = (phoneCriteria == null || phoneCriteria.isEmpty()) ?
                allPass : (contact) -> contact.getPhone().toLowerCase().contains(phoneCriteria.toLowerCase());
        emailPred = (emailCriteria == null || emailCriteria.isEmpty()) ?
                allPass : (contact) -> contact.getEmail().toLowerCase().contains(emailCriteria.toLowerCase());
        
        
        return contactMap.values().stream()
                .filter(firstNamePred)
                .filter(lastNamePred)
                .filter(companyPred)
                .filter(phonePred)
                .filter(emailPred)
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> searchContacts(Predicate<Contact> filter) {
        return contactMap.values().stream()
                .filter(filter)
                .collect(Collectors.toList());
    }

}
