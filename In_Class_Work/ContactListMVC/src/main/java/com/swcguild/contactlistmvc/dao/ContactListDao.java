/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.contactlistmvc.dao;

import com.swcguild.contactlistmvc.model.Contact;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

/**
 *
 * @author apprentice
 */
public interface ContactListDao {

    // add the contact to the data store
    public Contact addContact(Contact contact);
    // remove the contact from the data store
    public void removeContact(int contactId);
    // update the given contact in the data store
    public void updateContact(Contact contact);
    // list all contacts from the data store
    public List<Contact> getAllContacts();
    // retrieve contact with the given id from the data store
    public Contact getContactById(int contactId);
    // search for Contacts by the given search criteria values
    public List<Contact> searchContacts(Map<SearchTerm, String> criteria);
    // search all Contacts by the given predicate filter
    public List<Contact> searchContacts(Predicate<Contact> filter);
}
