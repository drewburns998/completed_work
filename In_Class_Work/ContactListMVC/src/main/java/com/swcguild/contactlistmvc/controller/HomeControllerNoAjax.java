/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.contactlistmvc.controller;

import com.swcguild.contactlistmvc.dao.ContactListDao;
import com.swcguild.contactlistmvc.dao.SearchTerm;
import com.swcguild.contactlistmvc.model.Contact;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class HomeControllerNoAjax {

    // reference to the dao    
    private ContactListDao dao;

    @Inject
    public HomeControllerNoAjax(ContactListDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/displayContactListNoAjax", method = RequestMethod.GET)
    public String displayContactList(Model model) {

        List<Contact> allContacts = dao.getAllContacts();
        model.addAttribute("contactList", allContacts);

        return "noAjax/noAjaxDisplayContactList";
    }

    @RequestMapping(value = "/displaySearchFormNoAjax", method = RequestMethod.GET)
    public String displaySearchFormWithoutAjax(Model model) {
        return "noAjax/searchFormWithoutAjax";
    }

    @RequestMapping(value = "/searchWithoutAjax", method = RequestMethod.POST)
    public String searchFormWithoutAjax(Model model, HttpServletRequest req) {
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String company = req.getParameter("company");
        String email = req.getParameter("email");
        String phone = req.getParameter("phone");

        Map<SearchTerm, String> criteriaForSearch = new HashMap<>();

        criteriaForSearch.put(SearchTerm.FIRST_NAME, firstName);
        criteriaForSearch.put(SearchTerm.LAST_NAME, lastName);
        criteriaForSearch.put(SearchTerm.COMPANY, company);
        criteriaForSearch.put(SearchTerm.EMAIL, email);
        criteriaForSearch.put(SearchTerm.PHONE, phone);

        List<Contact> foundContacts = dao.searchContacts(criteriaForSearch);
        model.addAttribute("contactList", foundContacts);
        model.addAttribute("fromSearch", true);
        model.addAttribute("listSize", foundContacts.size());

        return "noAjax/noAjaxDisplayContactList";
    }

    @RequestMapping(value = "/newContactFormNoAjax", method = RequestMethod.GET)
    public String displayNewContactFormNoAjax() {
        return "noAjax/newContactFormWithoutAjax";
    }

    @RequestMapping(value = "/addContactNoAjax", method = RequestMethod.POST)
    public String addNewContact(HttpServletRequest req) {
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String company = req.getParameter("company");
        String email = req.getParameter("email");
        String phone = req.getParameter("phone");

        Contact newContact = new Contact();
        newContact.setFirstName(firstName);
        newContact.setLastName(lastName);
        newContact.setCompany(company);
        newContact.setPhone(phone);
        newContact.setEmail(email);

        return "redirect:displayContactListNoAjax";
    }

    @RequestMapping(value = "/deleteContactNoAjax", method = RequestMethod.GET)
    public String deleteContact(HttpServletRequest req) {
        String contactId = req.getParameter("cId");

        int sanitizedId = this.sanitizeContactId(contactId);
        if (sanitizedId >= 0) {
            dao.removeContact(sanitizedId);
        }
        return "redirect:displayContactListNoAjax";
    }

    @RequestMapping(value = "/editContactNoAjax", method = RequestMethod.GET)
    public String displayEditFormNoAjax(HttpServletRequest req, Model model) {

        int sanitizedId = this.sanitizeContactId(req.getParameter("cId"));

        if (sanitizedId >= 0) {
            Contact contact = dao.getContactById(sanitizedId);
            model.addAttribute("contactToEdit", contact);
        }

        return "noAjax/editContactFormWithoutAjax";
    }

    @RequestMapping(value = "/editContactNoAjax", method = RequestMethod.POST)
    public String editModelContactNoAjax(@ModelAttribute("contactToEdit") Contact contact) {
        dao.updateContact(contact);
        return "redirect:displayContactListNoAjax";

    }

    @RequestMapping(value = "/editContactNoAjaxWithValidation", method = RequestMethod.POST)
    public String editModelContactNoAjaxValidation(@Valid @ModelAttribute("contactToEdit") Contact contact,
            BindingResult result) {
        
        if(result.hasErrors()){
            return "noAjax/editContactFormWithoutAjax";
        }
        
        dao.updateContact(contact);
        return "redirect:displayContactListNoAjax";

    }

    private int sanitizeContactId(String contactIdParam) {
        int contactIdInt;
        try {
            contactIdInt = Integer.parseInt(contactIdParam);
        } catch (NumberFormatException ex) {
            contactIdInt = -1;
        }
        return contactIdInt;
    }

}
