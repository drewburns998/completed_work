<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Company Contacts</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Company Contacts</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/newContactFormNoAjax">Add Contact (no Ajax)</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/displayContactListNoAjax">Contact List (no Ajax)</a></li>
                    
                </ul>    
            </div>
            <c:if test="${fromSearch}">
                Here are the results of your search... <br/>
                <c:if test="${listSize < 1}">
                    No results found.
                </c:if>
            </c:if>

            <c:forEach var="contact" items="${contactList}">

                <s:url value="deleteContactNoAjax" var="deleteContact_url">
                    <s:param name="cId" value="${contact.contactId}" />
                </s:url>
                <s:url value="editContactNoAjax" var="editContact_url">
                    <s:param name="cId" value="${contact.contactId}" />
                </s:url>

                Name:
                <c:if test="${contact.lastName == 'Furgeson'}">
                    Mr.
                </c:if>
                ${contact.firstName} ${contact.lastName} | 
                <a href="${deleteContact_url}">Delete</a> | 
                <a href="${editContact_url}">Edit</a> <br/>
                Company: ${contact.company} <br/>
                Phone: ${contact.phone} <br/>
                Email: ${contact.email} <br/>
                <hr/>

            </c:forEach>


        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

