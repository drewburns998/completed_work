<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Company Contacts</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Company Contacts</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/">Home</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/newContactFormNoAjax">Add Contact (no Ajax)</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/displayContactListNoAjax">Contact List (no Ajax)</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/stats">Stats</a></li>
                </ul>    
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h2>Search Results</h2>
                    <%@include file="contactSummaryTableFragment.jsp" %>
                </div>
                <div class="col-md-offset-1 col-md-5">
                    <h2>Search Contact</h2>
                    <form class="form-vertical" role="form">
                        <div class="form-group">
                            <label for=search-first-name" class="col-md-4 control-label">First Name:</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="search-first-name" placeholder="First Name" /></div>
                        </div>
                        <div class="form-group">
                            <label for=search-last-name" class="col-md-4 control-label">Last Name:</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="search-last-name" placeholder="Last Name" /></div>
                        </div>
                        <div class="form-group">
                            <label for=search-company" class="col-md-4 control-label">Company:</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="search-company" placeholder="Company" /></div>
                        </div>
                        <div class="form-group">
                            <label for=search-phone" class="col-md-4 control-label">Phone:</label>
                            <div class="col-md-8"><input type="tel" class="form-control" id="search-phone" placeholder="Phone" /></div>
                        </div>
                        <div class="form-group">
                            <label for=search-email" class="col-md-4 control-label">Email::</label>
                            <div class="col-md-8"><input type="email" class="form-control" id="search-email" placeholder="Email" /></div>
                        </div>
                        <br/>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <button id="search-button"type="submit" class="btn btn-default btn-primary">Search Contact</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <%@include file="detailsAndEditModalFragment.jsp"%>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <%@include file="scriptsFragment.jsp"%>

    </body>
</html>

