/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    // put other code to call when the document has finished loading
    loadContacts();

    $('#add-button').click(function (event) {

        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'contact',
            data: JSON.stringify({
                contactId: $('#edit-contactId').val(),
                firstName: $('#add-first-name').val(),
                lastName: $('#add-last-name').val(),
                company: $('#add-company').val(),
                phone: $('#add-phone').val(),
                email: $('#add-email').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            $('#add-first-name').val('');
            $('#add-last-name').val('');
            $('#add-company').val('');
            $('#add-phone').val('');
            $('#add-email').val('');

            loadContacts();
        });

    });

    $('#edit-button').click(function (event) {

        event.preventDefault();

        $.ajax({
            type: 'PUT',
            url: 'contact/' + $('#edit-contactId').val(),
            data: JSON.stringify({
                contactId: $('#edit-contactId').val(),
                firstName: $('#edit-first-name').val(),
                lastName: $('#edit-last-name').val(),
                company: $('#edit-company').val(),
                phone: $('#edit-phone').val(),
                email: $('#edit-email').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            loadContacts();
        });

    });

    $('#search-button').click(function(event) {

        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'search/contacts',
            data: JSON.stringify({
                firstNameSearch: $('#search-first-name').val(),
                lastNameSearch: $('#search-last-name').val(),
                companySearch: $('#search-company').val(),
                phoneSearch: $('#search-phone').val(),
                emailSearch: $('#search-email').val(),
                extraLlama: "llama"
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            fillContactTable(data, status);
        });
    });

});

function loadContacts() {

    var cTableBody = $('#contentRows');
    cTableBody.empty();

    $.ajax({
        url: "contacts",
        type: "GET",
        headers: {
            'Accept': 'application/json'
        }
    }).success(function(data, status) {
        fillContactTable(data, status);
    });
}

function fillContactTable(contactList, status) {

    var cTableBody = $('#contentRows');
    cTableBody.empty();
    $.each(contactList, function (index, contact) {
        cTableBody.append($('<tr>')
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-contact-id': contact.contactId,
                                    'data-toggle': 'modal',
                                    'data-target': '#detailsModal'
                                })
                                .text(contact.firstName + ' ' + contact.lastName)
                                )
                        )
                .append($('<td>').text(contact.company))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-contact-id': contact.contactId,
                                    'data-toggle': 'modal',
                                    'data-target': '#editModal'
                                })
                                .text('Edit')
                                )
                        )
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'onClick': 'deleteContact(' + contact.contactId + ')'
                                })
                                .text('Delete')
                                )
                        )
                );
    });

}

function deleteContact(id) {

    var answer = confirm("Do you REALLY want to delete this contact?");

    if (answer === true) {
        $.ajax({
            url: 'contact/' + id,
            type: 'DELETE'

        }).success(function () {
            loadContacts();
        });
    }
}

// This code is going to run in response to the show.bs.modal event
$('#detailsModal').on('show.bs.modal', function (event) {

    var element = $(event.relatedTarget);
    var contactId = element.data('contact-id');

    var modal = $(this);

    $.ajax({
        type: 'GET',
        url: 'contact/' + contactId,
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (contact) {
        modal.find('#contact-id').text(contact.contactId);
        modal.find('#contact-firstName').text(contact.firstName);
        modal.find('#contact-lastName').text(contact.lastName);
        modal.find('#contact-company').text(contact.company);
        modal.find('#contact-phone').text(contact.phone);
        modal.find('#contact-email').text(contact.email);
    });

});

// This code is going to run in response to the show.bs.modal event
$('#editModal').on('show.bs.modal', function (event) {

    var element = $(event.relatedTarget);
    var contactId = element.data('contact-id');

    var modal = $(this);

    $.ajax({
        type: 'GET',
        url: 'contact/' + contactId,
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (contact) {

        modal.find('#contact-id').text(contact.contactId);
        modal.find('#edit-first-name').val(contact.firstName);
        modal.find('#edit-last-name').val(contact.lastName);
        modal.find('#edit-company').val(contact.company);
        modal.find('#edit-phone').val(contact.phone);
        modal.find('#edit-email').val(contact.email);
        modal.find('#edit-contactId').val(contact.contactId);

    });

});

// Test Data (basically contacts as JSON)
var testContactData = [
    {
        contactId: 1,
        firstName: "Quinoa",
        lastName: "Vegan",
        company: "Whole Foods",
        phone: "555-FRUIT",
        email: "ilovegrainprotein@quinoaisawesome.com"
    },
    {
        contactId: 2,
        firstName: "Andrew",
        lastName: "Burns",
        company: "Bad Company",
        phone: "555-7897",
        email: "cooldude63@awesome.com"
    },
    {
        contactId: 3,
        firstName: "Winona",
        lastName: "Wixson",
        company: "Forever Alone",
        phone: "555-FRUIT",
        email: "ilovesilence@loudnoises.com"
    },
    {
        contactId: 4,
        firstName: "Rene",
        lastName: "Gomez",
        company: "Bacon Shop",
        phone: "555-POPO",
        email: "ilovejustice@quinoaisawesome.com"
    },
    {
        contactId: 5,
        firstName: "Solomon",
        lastName: "Kim",
        company: "WWE",
        phone: "555-OIL",
        email: "iloveoil@lathermeup.com"
    },
];


  