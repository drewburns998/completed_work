/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.time.LocalDate;

/**
 *
 * @author apprentice
 */
public enum SearchTerm {
    
    TITLE, MPAA_RATING, DIRECTOR, STUDIO, USER_RATING, RELEASE_DATE   
    
}
