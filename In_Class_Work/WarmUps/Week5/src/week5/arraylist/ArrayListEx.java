/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5.arraylist;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author apprentice
 */
public class ArrayListEx {
    public static void main(String[] args) {
        
        ArrayList<String> nflTeams = new ArrayList<>();
        //Vikings, Packers, Lions, Bears, Browns, Bengals, Steelers, Ravens
        nflTeams.add("Vikings");
        nflTeams.add("Packers");
        nflTeams.add("Lions");
        nflTeams.add("Bears");
        nflTeams.add("Browns");
        nflTeams.add("Bengals");
        nflTeams.add("Steelers");
        nflTeams.add("Ravens");

        System.out.println("Enhanced For Loop:");
        for(String t : nflTeams){
            System.out.println(t);
        }
        
        Iterator itr = nflTeams.iterator();
        
        System.out.println("\nUsing Iterator:");
        while(itr.hasNext()){
            String element = (String) itr.next();
            System.out.println(element);
        }
        
        System.out.println("\nRegular For Loop:");
        for(int i=0; i<nflTeams.size(); i++){
            System.out.println(nflTeams.get(i));
        }
        
        
    }
}
