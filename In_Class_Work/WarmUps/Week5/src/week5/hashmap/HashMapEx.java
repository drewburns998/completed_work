/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5.hashmap;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public class HashMapEx {
    public static void main(String[] args) {
        HashMap<String, String> nflCities = new HashMap<>();
        //[Cleveland, Browns], [Pittsburgh, Steelers], 
        //[Cincinnati, Bengals], [Minneapolis, Vikings], [Kansas City, Chiefs]
        
        nflCities.put("Cleveland", "Browns");
        nflCities.put("Pittsburgh", "Steelers");
        nflCities.put("Cincinnati", "Bengals");
        nflCities.put("Minneapolis", "Vikings");
        nflCities.put("Kansas City", "Chiefs");
        
        System.out.println("Print keys and values with enhanced for loops:");
        for(String key : nflCities.keySet()){
            System.out.println(key);
        }
        
        for(String value : nflCities.values()){
            System.out.println(value);
        }
        
        
        Iterator<Map.Entry<String, String>> iterator = nflCities.entrySet().iterator();
        System.out.println("\nPrint keys and values with iterator and entry set");
        while(iterator.hasNext()){
            Map.Entry<String, String> entry = iterator.next();
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
        
        
        
    }
}
