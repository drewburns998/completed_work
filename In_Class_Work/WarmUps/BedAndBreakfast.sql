DROP DATABASE IF EXISTS BedAndBreakfast;

create database BedAndBreakfast;

use BedAndBreakfast;

CREATE TABLE Customer
(custID int NOT NULL,
custName varchar(50) NOT NULL,
Phone varchar(13) NOT NULL,
Email varchar(50),
License varchar(12),
CreditCard int(4) NOT NULL,
PRIMARY KEY(custID));

CREATE TABLE Room
(roomID int NOT NULL,
roomType varchar(20) NOT NULL,
BasePrice DECIMAL(10,2),
NumOfBeds int(2),
Smoking TINYINT(1),
PRIMARY KEY(roomID));

CREATE TABLE Reservation
(resID int NOT NULL,
StartDate DATE,
EndDate DATE,
NumOfNights int(3),
custID int NOT NULL,
PRIMARY KEY(resID),
FOREIGN KEY(custID) references Customer(custID));

CREATE TABLE Room_Reservations
(BookedDate DATE,
DayType varchar(10),
roomID int NOT NULL,
resID int NOT NULL,
FOREIGN KEY(roomID) references Room(roomID),
FOREIGN KEY (resID) references Reservation(resID));


CREATE TABLE Amenities
(Amen_Type varchar(30),
Price DECIMAL(10,2),
PRIMARY KEY(Amen_Type));

CREATE TABLE Day_Type
(Day_Type varchar(30),
Adjustment varchar(10));

CREATE TABLE Purchase
(date DATETIME,
quantity int(5),
resID int NOT NULL,
Amen_Type varchar(30),
FOREIGN KEY(resID) references Reservation(resID),
FOREIGN KEY(Amen_Type) references Amenities(Amen_Type));

USE BedAndBreakfast;

INSERT INTO Customer (custID, custName, Phone, Email, License, CreditCard)  VALUES('001', 'Ana Trujillo', '585-111-2222', 'atrujillo@gmail.com', '123ab', '1234');
INSERT INTO Room (roomID, roomType, BasePrice, NumOfBeds, Smoking) VALUES('001', 'suite', '150', '2', '0');
INSERT INTO Reservation (resID, StartDate, EndDate, NumOfNights, custID) VALUES('001', '2016-05-04', '2016-05-08', '4', '001');
INSERT INTO Room_Reservations(BookedDate, DayType, roomID, resID) VALUES('2016-05-04', 'WD', '001', '001');
INSERT INTO Amenities(Amen_Type, Price) VALUES ('RS', '30');
INSERT INTO Day_Type(Day_Type, Adjustment) VALUES('WD', '10');
INSERT INTO Purchase(date, quantity, resID, Amen_Type) VALUES ('2016-05-04 12:12:12','1', '001', 'RS');

INSERT INTO Customer (custID, custName, Phone, Email, License, CreditCard)  VALUES('002', 'John Smith', '500-112-2442', 'john@gmail.com', '123ab', '1234');
INSERT INTO Room (roomID, roomType, BasePrice, NumOfBeds, Smoking) VALUES('002', 'honeymooonsuite', '150', '2', '0');
INSERT INTO Reservation (resID, StartDate, EndDate, NumOfNights, custID) VALUES('002', '2016-05-04', '2016-05-08', '4', '001');
INSERT INTO Room_Reservations(BookedDate, DayType, roomID, resID) VALUES('2016-05-04', 'WD', '002', '002');
INSERT INTO Amenities(Amen_Type, Price) VALUES ('DRINKS', '30');
INSERT INTO Day_Type(Day_Type, Adjustment) VALUES('WE', '10');
INSERT INTO Purchase(date, quantity, resID, Amen_Type) VALUES ('2016-05-04 12:12:12','1', '002', 'DRINKS');


SELECT * FROM Purchase