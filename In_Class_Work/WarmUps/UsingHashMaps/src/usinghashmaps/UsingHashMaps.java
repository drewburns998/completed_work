/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usinghashmaps;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class UsingHashMaps {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        HashMap<String, Integer> teamScores = new HashMap<>();
        Integer totalScores = 0;
        teamScores.put("Smith", 23);
        teamScores.put("Jones", 12);
        teamScores.put("Jordan", 45);
        teamScores.put("Pippen", 32);
        teamScores.put("Kerr", 15);

        Set keys = teamScores.keySet();
        Set<Entry<String, Integer>> allScores = teamScores.entrySet();

        for (Entry<String, Integer> ent : allScores) {
            totalScores = totalScores + ent.getValue();
            System.out.println(ent.getKey() + " : " + ent.getValue());
        }
        
        System.out.println("Team Average: " + totalScores/keys.size());

    }

}
