/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package countingdice;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author apprentice
 */
public class CountingDice {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Random r = new Random();
        int sum = 0;

        int[] resultList = new int[13]; //13 possible combinations, though 0 and 1 will not occur
        
        //roll 2 six sided dice
        for (int i = 0; i < 50; i++) {
            sum = (r.nextInt(6) + 1) + (r.nextInt(6) + 1);
            resultList[sum] = resultList[sum] + 1;
        }

        System.out.println("Counting Dice Histogram");
        System.out.println("\t By: Winona Wixson and Andrew Burns");
        System.out.println("\nSum of Dice");
        for (int i = 2; i < resultList.length; i++) {
            System.out.print("\t  " + i + ": ");
            for (int j = 0; j < resultList[i]; j++) {
                System.out.print("+");
            }
            System.out.println("");
        }
        System.out.println("\n\t Frequency: \'+\' = one occurrence");

    }

}
