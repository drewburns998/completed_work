/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.restfulwarmup;

import java.util.Map;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */

@Controller
@RequestMapping({"/"})
public class RestfulWarmupController {
    Random r = new Random();
    
    @RequestMapping(value="/rollDice", method=RequestMethod.POST)
    public String rollDice() {
        //fill in
        return "index";
    }
    
    
    @RequestMapping(value="/randomNum", method=RequestMethod.GET)
    @ResponseBody
    public Integer randomNum(HttpServletRequest request, Model model) {
        
        return r.nextInt(25);
        
    }
    
    
    @RequestMapping(value="/randomCompliment", method=RequestMethod.POST)
    public String randomCompliment() {
        //fill in
        return "index";
    }
    
    
    @RequestMapping(value="/randomAnimal", method=RequestMethod.GET)
    @ResponseBody
    public String randomAnimal() {
        String[] animals = {"Dog", "Cat", "Elephant", "Wolf"};        
        return animals[r.nextInt(animals.length)];
    }
    
}
