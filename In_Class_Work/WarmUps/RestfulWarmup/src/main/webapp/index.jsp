<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Restful Warmup</h1>
            <hr/>
            <br>
            <div class="row">
                <div id="displayDice" class="col-md-6">
                    <h3>Random Dice Roll</h3>
                    <form class="form-vertical" role="form" method="POST">
                        <div class="form-group">
                            <label for="randomDice" class="control-label">Number of Sides</label><br/>
                            <div><input type="number" min="0" class="form-control" id="randomDice" name="itemNumber" placeholder="Number of Sides" /></div>
                        </div>


                        <br>
                        <div class="form-group">
                            <div class="col-md-4">
                                <button id="dice-button" type="submit" class="btn btn-default btn-primary">Roll</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div id="displayNum" class="col-md-6">
                    <h3>Random Number</h3>
                    <div class="col-md-4">
                        <button id="random-num-button" type="submit" class="btn btn-default btn-primary">Generate</button>
                    </div>
                </div>
            </div>
            <br><br><br>

            <div class="row">
                <div id="displayCompliment" class="col-md-6">
                    <h3>Random Compliment</h3>
                    <form class="form-vertical" role="form" method="POST">
                        <div class="form-group">
                            <label for="userName" class="control-label">Enter Name:</label><br/>
                            <div><input type="text" min="0" class="form-control" id="userName" name="userName" placeholder="Name:" /></div>
                        </div>


                        <br>
                        <div class="form-group">
                            <div class="col-md-4">
                                <button id="compliment-button" type="submit" class="btn btn-default btn-primary">Generate</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div id="displayAnimal" class="col-md-6">
                    <h3>Random Animal</h3>
                    <div class="col-md-4">
                        <button id="random-animal-button" type="submit" class="btn btn-default btn-primary">Generate</button>
                    </div>
                </div>
            </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/restfulwarmup.js"></script>

    </body>
</html>

