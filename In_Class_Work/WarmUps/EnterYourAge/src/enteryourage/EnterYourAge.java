/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enteryourage;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class EnterYourAge {

    public static void main(String[] args) {
        int userAge;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter your age:");
        userAge = sc.nextInt();

        checkAge(userAge);
    }

    public static void checkAge(int age) {
        if (age <= 0) {
            System.out.println("You picked a weird age!");
        } else if (age <= 18) {
            System.out.println("You must be in school");
        } else if (age < 65) {
            System.out.println("Time to go to work!");
        } else {
            System.out.println("Enjoy retirement!");
        }
    }
}
