<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            
            <div class="col-md-6">Dice Roll</div>
            <div class="col-md-6">Random Number</div>
            <div class="col-md-6">
                <h2 class="col-md-12" id=compliment"></h2>
                <input type="text" id="compliment-name"/>
                <button id="compliment-btn" class="btn btn-primary" onclick="complimentMe()">Compliment Me</button>
            </div>
            <div class="col-md-6">
                <h1 id="animal">####</h1>
                <button id="animal-btn" class="btn btn-success">Click Here for a Random Animal</button>
            </div>
            
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/restpractice.js"></script>

    </body>
</html>

