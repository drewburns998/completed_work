/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $('#animal-btn').click(function (event) {

        $.ajax({
            url: 'animal',
            type: 'GET',
            headers: {
                'Accept': 'text/plain'
            }

        }).success(function (animal){
            $('#animal').text(animal);
        });


    });

});

function complimentMe(){
    $.ajax({
        url:'compliment',
        type: 'POST',
        data: $('#compliment-name').val(),
        headers: {
            'Accept': 'text/plain',
            'Content-Type': 'text/plain'            
        },
        dataType : 'text/plain'
        
    }).success(function(incomingCompliment){
        $('#compliment').text(incomingCompliment);
        
    });
    
}
