/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.randomrestpractice;

import java.util.Random;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
public class RestPracticeController {

    Random r = new Random();

    @RequestMapping(value = "/randomNum", method = RequestMethod.GET)
    @ResponseBody
    public int getRandomNumber() {
        return r.nextInt(100);
    }

    @RequestMapping(value = "/dieRoll/d{sideNum}", method = RequestMethod.GET)
    @ResponseBody
    public int getRandomNumber(@PathVariable("sideNum") int numSides) {
        return r.nextInt(numSides) + 1;
    }

    @RequestMapping(value = "/animal", method = RequestMethod.GET)
    @ResponseBody
    public String getRandomAnimal() {
        String[] animals = {"Dog", "Cat", "Elephant", "Wolf"};
        return animals[r.nextInt(animals.length)];
    }

    @RequestMapping(value = "/compliment", method = RequestMethod.POST)
    @ResponseBody
    public String getRandomCompliment(@RequestBody String name) {
        String compliment = "";
        String[] compliments = {"%s is Dumb", "%s is Lazy", "%s is Lethargic", "%s is an Idiot", "%s has Bad Breath", "%s is Sloth"};

        compliment = String.format(compliments[r.nextInt(compliments.length)], name);
        return compliment;
    }

}
