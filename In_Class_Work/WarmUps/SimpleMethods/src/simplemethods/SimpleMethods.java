/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simplemethods;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class SimpleMethods {

    /**
     * @param args the command line arguments
     */
    
    public static String excite(String A) {
        A = A + "!!!";
        return A;
    }
    
    public static boolean not(boolean B) {        
        return !B;
    }
    
    public static String iLoveLouisville(String A) {
        return "I Love Louisville";
    }
    
    public static void main(String[] args) {
        String userInput = "";
        boolean defaultState = true;
        
        Scanner sc = new Scanner(System.in);
        
        System.out.println("enter a string: ");
        
        userInput = sc.nextLine();
        userInput = excite(userInput);
        
        System.out.println(userInput);
        
        System.out.println("Default:" + defaultState);
        defaultState = not(defaultState);
        
        System.out.println("New State: " + defaultState);
        System.out.println(iLoveLouisville(""));
        
    }    
    
}
