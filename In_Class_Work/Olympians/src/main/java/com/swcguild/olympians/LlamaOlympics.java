/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.olympians;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class LlamaOlympics {
    public static void main(String[] args) {
        
        ApplicationContext factory = new ClassPathXmlApplicationContext("applicationContext.xml");        
        Olympian nlg = factory.getBean("norwegianLlamaGymnast", Olympian.class);
        System.out.println(nlg.competeInEvent());
        
        Olympian plj = factory.getBean("peruvianLlamaJouster", Olympian.class);
        System.out.println(plj.competeInEvent());
        
        
//        Olympian nls = factory.getBean("norwegianLlamaGymnast", Olympian.class);
//        Olympian nla = factory.getBean("norwegianLlamaGymnast", Olympian.class);
//        Olympian nld = factory.getBean("norwegianLlamaGymnast", Olympian.class);
        
        
    }
    
}
