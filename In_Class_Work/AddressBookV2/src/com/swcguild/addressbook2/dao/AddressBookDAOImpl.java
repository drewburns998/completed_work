/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.addressbook2.dao;

import com.swcguild.addressbook2.dto.Address;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class AddressBookDAOImpl implements AddressBookDAO {

    public static final String ADDRESS_BOOK_FILE = "AddressBook.txt";
    private static final String DELIMITER = "::";

    ArrayList<Address> addressBook = new ArrayList<>();

    @Override
    public ArrayList<Address> getAddresses() {
        ArrayList<Address> fullList
                = addressBook
                .stream()
                .collect(Collectors.toCollection(ArrayList::new));

        return fullList;
    }

    @Override
    public List<Address> getByLastName(String lastName) {
        List<Address> filteredList
                = addressBook
                .stream()
                .filter(s -> s.getLastName().equalsIgnoreCase(lastName))
                .collect(Collectors.toList());
        return filteredList;
    }

    @Override
    public List<Address> getByState(String state) {
        List<Address> filteredList
                = addressBook
                .stream()
                .filter(s -> s.getState().equalsIgnoreCase(state))                
                .collect(Collectors.toList());
        Collections.sort(filteredList);
        return filteredList;        
    }

    @Override
    public List<Address> getByZip(String zip) {
        List<Address> filteredList
                = addressBook
                .stream()
                .filter(s -> s.getZip().equalsIgnoreCase(zip))
                .collect(Collectors.toList());
        return filteredList;
    }

    @Override
    public List<Address> getByCity(String city) {
        List<Address> filteredList
                = addressBook
                .stream()
                .filter(s -> s.getCity().equalsIgnoreCase(city))
                .collect(Collectors.toList());
        return filteredList;
    }

    @Override
    public void loadAddressBook() throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader(ADDRESS_BOOK_FILE)));
        while (sc.hasNextLine()) {

            String recordLine = sc.nextLine();
            String[] recordProperties = recordLine.split(DELIMITER);

            if (recordProperties.length != 6) {
                continue;
            }

            String address = recordProperties[0];
            String city = recordProperties[1];
            String state = recordProperties[2];
            String zip = recordProperties[3];
            String firstName = recordProperties[4];
            String lastName = recordProperties[5];

            Address addressRecord = new Address(address, city, state, zip, firstName, lastName);
            addressBook.add(addressRecord);

        }
    }

    @Override
    public void writeAddressBook() throws IOException {
        PrintWriter writer = new PrintWriter(new FileWriter(ADDRESS_BOOK_FILE));
        ArrayList<Address> allAddresses = getAddresses();

        allAddresses.stream().forEach((address) -> {
            writer.println(address.getStreetAddress() + DELIMITER
                    + address.getCity() + DELIMITER
                    + address.getState() + DELIMITER
                    + address.getZip() + DELIMITER
                    + address.getFirstName() + DELIMITER
                    + address.getLastName());
        });

        writer.flush();
        writer.close();
    }

    @Override
    public Address removeAddress(Address address) {
        addressBook.remove(address);
        return address;
    }

    @Override
    public void addAddress(Address address) {
        addressBook.add(address);
    }

}
