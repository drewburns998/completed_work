/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.addressbook2.dao;

import com.swcguild.addressbook2.dto.Address;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface AddressBookDAO {

    public ArrayList<Address> getAddresses();

    public List<Address> getByLastName(String lastName);

    public List<Address> getByState(String state);

    public List<Address> getByZip(String zip);

    public List<Address> getByCity(String city);

    public void loadAddressBook() throws FileNotFoundException;

    public void writeAddressBook() throws IOException;

    public Address removeAddress(Address address);

    public void addAddress(Address address);

}
