/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.addressbook2.ops;

import com.swcguild.addressbook2.dao.AddressBookDAO;
import com.swcguild.addressbook2.dao.AddressBookDAOImpl;
import com.swcguild.addressbook2.dto.Address;
import com.swcguild.addressbook2.ui.ConsoleIO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class AddressBookController {

    private ConsoleIO console = new ConsoleIO();
    private AddressBookDAO daoLayer = new AddressBookDAOImpl();

    public void run() throws IOException {

        boolean keepRunning = true;
        int menuSelect = 0;

        try {
            daoLayer.loadAddressBook();
        } catch (FileNotFoundException ex) {
            keepRunning = false;
            console.print("Address book file not found.");
        }

        while (keepRunning) {
            printMenu();
            menuSelect = console.readInt("Select an option...");
            switch (menuSelect) {

                case 1: //add
                    addAddress();
                    break;

                case 2: //remove
                    removeAddress();
                    break;

                case 3: //list all
                    listAllAddresses();
                    break;

                case 4: 
                    searchByLastName();
                    break;

                case 5: 
                    searchByState();
                    break;

                case 6: 
                    searchByCity();
                    break;
                case 7:
                    searchByZip();
                    break;
                case 8: //exit
                    daoLayer.writeAddressBook();
                    keepRunning = false;
                    break;
            }
        }

    }

    public void printMenu() {
        console.print("Main Menu");
        console.print("1. Add Address");
        console.print("2. Remove Address");        
        console.print("3. List All");
        console.print("4. Search by Last Name");        
        console.print("5. Search by State.");
        console.print("6. Search by City.");
        console.print("7. Search by Zip Code.");
        console.print("8. Exit");

    }

    private void addAddress() {
        String lastName = console.readString("Enter the last name.");
        String firstName = console.readString("Enter the first name.");
        String address = console.readString("Enter the street address.");
        String city = console.readString("Enter the city.");
        String state = console.readString("Enter the state.");
        String zip = console.readString("Enter the zip code.");
        Address add = new Address(address, city, state, zip, firstName, lastName);
        //add address to HashMap
        daoLayer.addAddress(add);

    }

    private void removeAddress() {
        String firstName = console.readString("Please enter the first name of the entry to delete: ");
        String lastName = console.readString("Please enter the last name of the entry to delete: ");
        List<Address> searchedAddress = daoLayer.getByLastName(lastName);
        Address removedAddress = null;

        for (Address add : searchedAddress) {
            if (add.getFirstName().equalsIgnoreCase(firstName)) {
                removedAddress = daoLayer.removeAddress(add);
            }

        }
        if (removedAddress != null) {
            console.print(removedAddress.getFirstName() + " " + removedAddress.getLastName()
                    + " has been removed from the address book.");
            console.readString("Please press enter to continue...");
        }

    }

    private void listAllAddresses() {
        ArrayList<Address> addresses = daoLayer.getAddresses();
        addressSummary(addresses);
    }

    private void searchByLastName() {
        String lastName = console.readString("Please enter the last name of the person you would like to look up: ");
        List<Address> searchedAddress = daoLayer.getByLastName(lastName);
        addressSummary(searchedAddress);
    }

    private void searchByState() {
        String state = console.readString("Please enter the state to look up: ");
        List<Address> searchedAddress = daoLayer.getByState(state);
        addressSummary(searchedAddress);
    }
    
    private void searchByCity(){
        String city = console.readString("Please enter the city you would like to look up: ");
        List<Address> searchedAddress = daoLayer.getByCity(city);
        addressSummary(searchedAddress);
    }
    
    private void searchByZip(){
        String zip = console.readString("Please enter the zip code you would like to look up: ");
        List<Address> searchedAddress = daoLayer.getByZip(zip);
        addressSummary(searchedAddress);
    }
    
    private void addressSummary(List<Address> add){
        if(add.isEmpty()){
            console.print("No results found");
        }else{
            add.stream().map((a) -> {
                console.print(a.getFirstName() + " " + a.getLastName());
                return a;
            }).forEach((a) -> {
                console.print(a.getStreetAddress() + " " + a.getCity() + ", "
                        + a.getState() + " " + a.getZip() + "\n");
            });            
        }        
    }

}
