/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.applicationsuite.interestcalc.controller;

import com.swcguild.applicationsuite.interestcalc.model.InterestPeriod;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class InterestCalculatorController {

    @RequestMapping(value = "/calcInterest", method = RequestMethod.GET)
    public String showHome() {
        return "interestcalc";
    }

    @RequestMapping(value = "/calcInterest", method = RequestMethod.POST)
    public String calcAmount(HttpServletRequest request, Model model) {
        String dollarAmountString = request.getParameter("dollarAmount");
        String interestRateString = request.getParameter("interestRate");
        String yearsMaturityString = request.getParameter("yearsMaturity");
        String paymentPeriodsString = request.getParameter("paymentPeriods");
        Double currentBalance;
        ArrayList<InterestPeriod> yearTotals = new ArrayList<>();

        if (dollarAmountString == null || interestRateString == null || yearsMaturityString == null || paymentPeriodsString == null) {
            model.addAttribute("badInput", true);
        } else {
            try {
                double dollarAmount = Double.parseDouble(dollarAmountString);
                double interestRate = Double.parseDouble(interestRateString);
                double yearsMaturity = Double.parseDouble(yearsMaturityString);
                double paymentPeriods = Double.parseDouble(paymentPeriodsString);

                if (dollarAmount < 0 || interestRate < 0 || yearsMaturity <= 0 || paymentPeriods < 0) {
                    model.addAttribute("badInput", true);
                } else {

                    currentBalance = dollarAmount;
                    double yearlyInterest = 0;
                    double accruedInterest = 0;
                    for (int i = 1; i <= yearsMaturity; i++) {
                        for (int j = 1; j <= paymentPeriods; j++) {
                            accruedInterest = ((interestRate / paymentPeriods) / 100) * currentBalance;
                            currentBalance = currentBalance + accruedInterest;
                            yearlyInterest = yearlyInterest + accruedInterest;
                        }
                        yearTotals.add(new InterestPeriod(currentBalance, accruedInterest, yearlyInterest, i));
                    }
                    model.addAttribute("moneyList", yearTotals);
                }

            } catch (NumberFormatException ex) {
                model.addAttribute("badInput", true);
            }
        }
        return "interestcalc";
    }
}
