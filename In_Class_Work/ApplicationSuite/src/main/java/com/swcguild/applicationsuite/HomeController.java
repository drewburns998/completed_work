/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.applicationsuite;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class HomeController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String displayMainPage() {
        return "index";
    }

    @RequestMapping(value = "luckysevens", method = RequestMethod.GET)
    public String displayLuckySevensPage() {
        return "luckysevens";
    }

    @RequestMapping(value = "factorizer", method = RequestMethod.GET)
    public String displayFactorizerPage() {
        return "factorizer";
    }

    @RequestMapping(value = "flooringcalc", method = RequestMethod.GET)
    public String displayFlooringCalcPage() {
        return "flooringcalc";
    }

    @RequestMapping(value = "interestcalc", method = RequestMethod.GET)
    public String displayInterestCalcPage() {
        return "interestcalc";
    }
    
    @RequestMapping(value = "tipcalc", method = RequestMethod.GET)
    public String displayTipCalcPage() {
        return "tipcalc";
    }
    
    @RequestMapping(value = "unitconv", method = RequestMethod.GET)
    public String displayUnitConvPage() {
        return "unitconv";
    }

}
