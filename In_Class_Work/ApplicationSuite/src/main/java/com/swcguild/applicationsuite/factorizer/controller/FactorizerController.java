/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.applicationsuite.factorizer.controller;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class FactorizerController {


    @RequestMapping(value = "/factorize", method = RequestMethod.GET)
    public String showHome() {
        return "factorizer";
    }
    
    
    @RequestMapping(value = "/factorize", method = RequestMethod.POST)
    public String calcAmount(HttpServletRequest request, Model model) {

        String userNumString = request.getParameter("userNum");

        if (userNumString == null) {
            //request.setAttribute("badInput", true);
            model.addAttribute("badInput", true);
        } else {
            try {
                int userNum = Integer.parseInt(userNumString);
                int sumFactor = 0; // addition of factor sumation
                if (userNum <= 0) {
                    //request.setAttribute("badInput", true);
                    model.addAttribute("badInput", true);
                } else {

                    ArrayList<Integer> factors = new ArrayList<>();
                    for (int i = 1; i < userNum; i++) {
                        if (userNum % i == 0) {
                            sumFactor = sumFactor + i;
                            factors.add(i);
                        }
                    }
                    //request.setAttribute("factorList", factors);
                    model.addAttribute("factorList", factors);
                    if (sumFactor == 1) {
                        //request.setAttribute("primeNum", true);
                        model.addAttribute("primeNum", true);
                    }
                    if (sumFactor == userNum) {
                        //request.setAttribute("perfectNum", true);
                        model.addAttribute("perfectNum", true);
                    }

                }
            } catch (NumberFormatException ex) {
                //request.setAttribute("badInput", true);
                model.addAttribute("badInput", true);
            }

        }

        return "factorizer";
    }
}
