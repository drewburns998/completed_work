/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.applicationsuite.flooringcalc.controller;

import com.swcguild.applicationsuite.flooringcalc.model.FlooringCalculator;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class FlooringCalculatorController {
    
    @RequestMapping(value = "/calcFlooring", method = RequestMethod.GET)
    public String showHome() {
        return "flooringcalc";
    }

    @RequestMapping(value = "/calcFlooring", method = RequestMethod.POST)
    public String calcAmount(HttpServletRequest request, Model model) {
        
        String widthString = request.getParameter("width");
        String lengthString = request.getParameter("length");
        String costString = request.getParameter("costSqFt");
        
        if (widthString == null || lengthString == null || costString == null) {
            model.addAttribute("badInput", true);
        } else {
            try {
                double width = Double.parseDouble(widthString);
                double length = Double.parseDouble(lengthString);
                double costSqFt = Double.parseDouble(costString);
                
                if (width <= 0 || length <= 0 || costSqFt <= 0) {
                    model.addAttribute("badInput", true);
                } else {
                    FlooringCalculator calc = new FlooringCalculator(width, length, costSqFt);
                    model.addAttribute("calculations", calc);
                    model.addAttribute("width", width);
                    model.addAttribute("length", length);
                    model.addAttribute("costSqFt", costSqFt);
                    
                    model.addAttribute("laborTime", calc.getLaborTime());
                    model.addAttribute("laborCost", calc.getLaborCost());
                    model.addAttribute("flooringCost", calc.getFlooringCost());
                    model.addAttribute("flooringArea", calc.getAreaSqFt());
                    
                }
                
            } catch (NumberFormatException ex) {
                model.addAttribute("badInput", true);
            }
            
        }
        
        return "flooringcalc";
    }
}
