/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.applicationsuite.tipcalculator.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class TipCalculatorController {

    @RequestMapping(value = "/calcTip", method = RequestMethod.GET)
    public String showHome() {
        return "tipcalc";
    }

    @RequestMapping(value = "/calcTip", method = RequestMethod.POST)
    public String calcAmount(HttpServletRequest request, Model model) {
        String billAmountString = request.getParameter("billAmount");
        String tipPercentString = request.getParameter("tipPercent");

        if (billAmountString == null || tipPercentString == null) {
            model.addAttribute("badInput", true);
        } else {
            try {
                double billAmount = Double.parseDouble(billAmountString);
                double tipPercent = Double.parseDouble(tipPercentString);

                if (billAmount < 0 || tipPercent < 0) {
                    model.addAttribute("badInput", true);
                } else {
                    model.addAttribute("subTotal", billAmount);
                    model.addAttribute("tipPercent", tipPercent);
                    model.addAttribute("tipAmount", (billAmount * tipPercent / 100));
                    model.addAttribute("grandTotal", (billAmount + billAmount * tipPercent / 100));
                    
                }

            } catch (NumberFormatException ex) {
                model.addAttribute("badInput", true);
            }
        }

        return "tipcalc";
    }
}
