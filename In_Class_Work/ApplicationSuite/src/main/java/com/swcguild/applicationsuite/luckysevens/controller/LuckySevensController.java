/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.applicationsuite.luckysevens.controller;

import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class LuckySevensController {

    @RequestMapping(value = "/calcLucky", method = RequestMethod.GET)
    public String showHome() {
        return "luckysevens";
    }

    @RequestMapping(value = "/calcLucky", method = RequestMethod.POST)
    public String calcAmount(HttpServletRequest request, Model model) {
        String userBetString = request.getParameter("userBet");
        int maxRound = 0;
        double currentMoney;
        int totalRollCounter = 0;
        int diceSum;
        double maxMoney;
        Random random1 = new Random();

        if (userBetString == null) {
            model.addAttribute("badInput", true);
        } else {
            try {
                double userBet = Double.parseDouble(userBetString);

                if (userBet <= 0) {
                    model.addAttribute("badInput", true);
                } else {
                    currentMoney = userBet;
                    maxMoney = userBet;
                    while (currentMoney > 0) {
                        int dice1 = random1.nextInt(5) + 1;
                        int dice2 = random1.nextInt(5) + 1;
                        totalRollCounter++;
                        diceSum = dice1 + dice2;

                        if (diceSum == 7) {
                            currentMoney += 4;
                        } else {
                            currentMoney -= 1;
                        }

                        if (currentMoney > maxMoney) {
                            maxMoney = currentMoney;
                            maxRound = totalRollCounter;
                        }

                    }
                    request.setAttribute("maxRound", maxRound);
                    request.setAttribute("maxMoney", maxMoney);
                    request.setAttribute("totalRounds", totalRollCounter);
                    model.addAttribute("maxRound", maxRound);
                    model.addAttribute("maxMoney", maxMoney);
                    model.addAttribute("totalRounds", totalRollCounter);

                }

            } catch (NumberFormatException ex) {
                model.addAttribute("badInput", true);
            }
        }

        return "luckysevens";
    }

}
