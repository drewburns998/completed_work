/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.applicationsuite.unitconv.controller;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class UnitConvController {

    @RequestMapping(value = "/convertunits", method = RequestMethod.GET)
    public String showHome() {
        return "unitconv";
    }

    @RequestMapping(value = "/convertunits", method = RequestMethod.POST)
    public String calcAmount(HttpServletRequest request, Model model) {

        String baseCelsString = request.getParameter("celsInput");
        String baseFahrString = request.getParameter("fahrInput");
        String basePoundsString = request.getParameter("poundsInput");
        String baseKiloString = request.getParameter("kiloInput");
        double baseCels, baseFahr, basePounds, baseKilo;

        if (baseCelsString == null && baseFahrString == null && basePoundsString == null && baseKiloString == null) {
            model.addAttribute("badInput", true);
        } else {
            try {
                baseCels = Double.parseDouble(baseCelsString);
                
                model.addAttribute("conversionFrom", "Celsius");
                model.addAttribute("conversionTo", "Fahrenheit");
                model.addAttribute("conversionFromValue", baseCels);
                model.addAttribute("conversionToValue", baseCels * 1.8 + 32.0);
            } catch (NumberFormatException ex) {
                model.addAttribute("badInput", true);
            }
            
            try {
                baseFahr = Double.parseDouble(baseFahrString);
                
                model.addAttribute("conversionFrom", "Fahrenheit");
                model.addAttribute("conversionTo", "Celsius");
                model.addAttribute("conversionFromValue", baseFahr);
                model.addAttribute("conversionToValue", (baseFahr-32.0)*(5.0/9.0));
            } catch (NumberFormatException ex) {
                model.addAttribute("badInput", true);
            }
            
            try {
                basePounds = Double.parseDouble(basePoundsString);
                
                model.addAttribute("conversionFrom", "Pounds");
                model.addAttribute("conversionTo", "Kilograms");
                model.addAttribute("conversionFromValue", basePounds);
                model.addAttribute("conversionToValue", basePounds*0.453592);
            } catch (NumberFormatException ex) {
                model.addAttribute("badInput", true);
            }
            
            try {
                baseKilo = Double.parseDouble(baseKiloString);                
                model.addAttribute("conversionFrom", "kilograms");
                model.addAttribute("conversionTo", "pounds");
                model.addAttribute("conversionFromValue", baseKilo);
                model.addAttribute("conversionToValue", baseKilo*2.20462);
            } catch (NumberFormatException ex) {
                model.addAttribute("badInput", true);
            }
        }

        return "unitconv";
    }
}
