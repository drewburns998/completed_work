<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Home Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">


    </head>
    <body>
        <div class="container">
            <h1>Application Suite - Spring MVC Application</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/factorizer">Factorizer</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/flooringcalc">Flooring Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/interestcalc">Interest Calculator</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/luckysevens">Lucky Sevens</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/tipcalc">Tip Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/unitconv">Unit Converter</a></li>
                </ul>    
            </div>

            <div clas="row">

                <div class="col-md-6">
                    <h3>Enter Starting Bet</h3>
                    <form role="form" method="POST" action="calcLucky">
                        <input type="number" placeholder="$ Starting Bet" min=0 class="form-control" name="userBet">                                                
                        <button type="submit" class="btn btn-default">Calculate</button>
                    </form>
                </div>


                <div class="col-md-6">
                    <h3>Results</h3>
                    <table id="tableid" class="table table-bordered">
                        <tr><td>Max Money:</td><td>$<c:out value="${maxMoney}" /></td></tr>
                        <tr><td>Max Round:</td><td><c:out value="${maxRound}" /></td> </tr> 
                        <tr><td>Total Rounds:</td><td><c:out value="${totalRounds}"/></td> </tr>
                    </table>
                </div>
            </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

