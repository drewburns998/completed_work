<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Home Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">


    </head>
    <body>
        <div class="container">
            <h1>Application Suite - Spring MVC Application</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/factorizer">Factorizer</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/flooringcalc">Flooring Calculator</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/interestcalc">Interest Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/luckysevens">Lucky Sevens</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/tipcalc">Tip Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/unitconv">Unit Converter</a></li>
                </ul>    
            </div>

            <div clas="row">

                <div class="col-md-6">
                    <h3>Enter Number</h3>
                    <form role="form" method="POST" action="calcInterest">
                        <div class="form-group">                                
                            <input type="number" placeholder="$ Amount Invested" min=0 class="form-control" name="dollarAmount">
                            <input type="text" placeholder="Interest Rate (% APR)" min=0 class="form-control" name="interestRate">
                            <input type="number" placeholder="Years to Invest" min=0 class="form-control" name="yearsMaturity">
                            <input type="number" placeholder="Payments Per Year" min=0 class="form-control" name="paymentPeriods">
                        </div>        
                        <button type="submit" class="btn btn-default">Calculate</button>
                    </form>

                </div>


                <div class="col-md-6">
                    <h3>Results</h3>
                    <c:if test="${badInput==true}">
                        Sorry, you entered some bad data!
                        <a href="index.jsp">Return to Flooring Form.</a>
                    </c:if>
                    <table id="tableid" class="table table-bordered">
                        <c:forEach items="${moneyList}" var="yearlyStatement">

                            <tr>
                                <td>Year:&nbsp;<c:out value="${yearlyStatement.year}" default="n/a" /></td>
                                <td>Balance:&nbsp;$<c:out value="${yearlyStatement.currentBalance}" default="n/a" /></td>
                                <td>Accrued Interest (YTD):&nbsp;$<c:out value="${yearlyStatement.accruedInterest}" default="n/a" /></td>
                                <td>Total Accrued Interest:&nbsp;$<c:out value="${yearlyStatement.yearlyInterest}" default="n/a"/></td>
                            </tr>

                        </c:forEach>
                    </table>

                </div>
            </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

