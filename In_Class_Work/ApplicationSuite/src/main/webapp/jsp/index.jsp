<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Home Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

        <style>
            .square {
                background: #A7A8A8;
                width: 15vw;
                height: 15vw;
                text-align:center;
                vertical-align:central;
                border:10px;           
                padding-top: 45px;
            }
            .square h1 {
                color: #90C3D4;                
            }

            a {
                display: block;
            }

        </style>

    </head>
    <body>
        <div class="container">
            <h1>Application Suite - Spring MVC Application</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/factorizer">Factorizer</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/flooringcalc">Flooring Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/interestcalc">Interest Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/luckysevens">Lucky Sevens</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/tipcalc">Tip Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/unitconv">Unit Converter</a></li>
                </ul>    
            </div>


            <div class="row">
                <a href="${pageContext.request.contextPath}/factorizer">
                    <div class="col-md-offset-1 col-md-3 square">
                        <h3>Factorizer</h3>
                    </div>
                </a>

                <a href="${pageContext.request.contextPath}/flooringcalc">
                    <div class="col-md-offset-1 col-md-3 square">
                        <h3>Flooring Calculator</h3>
                    </div>
                </a>
                <a href="${pageContext.request.contextPath}/interestcalc">
                    <div class="col-md-offset-1 col-md-3 square">
                        <h3>Interest Calculator</h3>
                    </div>
                </a>
            </div>
            <br />
            <div class="row">
                <a href="${pageContext.request.contextPath}/luckysevens">
                    <div class="col-md-offset-1 col-md-3 square">
                        <h3>Lucky Sevens</h3>
                    </div>
                </a>
                <a href="${pageContext.request.contextPath}/tipcalc">
                    <div class="col-md-offset-1 col-md-3 square">
                        <h3>Tip Calculator</h3>
                    </div>
                </a>
                <a href="${pageContext.request.contextPath}/convertunits">
                    <div class="col-md-offset-1 col-md-3 square">
                        <h3>Unit Converter</h3>
                    </div>
                </a>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

