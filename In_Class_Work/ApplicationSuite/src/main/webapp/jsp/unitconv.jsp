<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Home Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
        <script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".radio-inline3").hide();
                $(".radio-inline2").hide();
                $(".celsInputDiv").hide();
                $(".fahrInputDiv").hide();
                $(".poundsInputDiv").hide();
                $(".kiloInputDiv").hide();
                $(".convButton").hide();
                $('input[type="radio"]').click(function () {
                    if ($(this).attr("value") === "temp") {
                        $(".radio-inline2").show();
                        $(".radio-inline3").hide();
                    }
                    if ($(this).attr("value") === "mass") {
                        $(".radio-inline3").show();
                        $(".radio-inline2").hide();
                    }
                    if ($(this).attr("value") === "celsius") {
                        $(".celsInputDiv").show();
                        $(".convButton").show();
                        $(".fahrInputDiv").hide();
                        $(".poundsInputDiv").hide();
                        $(".kiloInputDiv").hide();
                    }
                    if ($(this).attr("value") === "fahrenheit") {
                        $(".fahrInputDiv").show();
                        $(".convButton").show();
                        $(".celsInputDiv").hide();
                        $(".poundsInputDiv").hide();
                        $(".kiloInputDiv").hide();
                    }
                    if ($(this).attr("value") === "pounds") {
                        $(".poundsInputDiv").show();
                        $(".convButton").show();
                        $(".celsInputDiv").hide();
                        $(".fahrInputDiv").hide();
                        $(".kiloInputDiv").hide();
                    }
                    if ($(this).attr("value") === "kilograms") {
                        $(".kiloInputDiv").show();
                        $(".convButton").show();
                        $(".celsInputDiv").hide();
                        $(".fahrInputDiv").hide();
                        $(".poundsInputDiv").hide();
                    }

                });
            });
        </script>

    </head>
    <body>
        <div class="container">
            <h1>Application Suite - Spring MVC Application</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/factorizer">Factorizer</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/flooringcalc">Flooring Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/interestcalc">Interest Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/luckysevens">Lucky Sevens</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/tipcalc">Tip Calculator</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/unitconv">Unit Converter</a></li>
                </ul>    
            </div>

            <div class="row">

                <div class="col-md-6">
                    <h2>Start Your Conversion</h2>

                    <p>Choose the conversion type below</p>

                    <div>
                        <label class="radio-inline">
                            <input type="radio" name="optradio" value="temp">Temperature
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="optradio" value="mass">Mass
                        </label>
                    </div>
                    </br>
                    <div>
                        <div>
                            <label class="radio-inline2">
                                <input type="radio" name="optradio2" value = "celsius" id="temp">&nbsp;Celsius -> Fahrenheit                        
                            </label>
                        </div>
                        <div>
                            <label class="radio-inline2">
                                <input type="radio" name="optradio2" value = "fahrenheit" id="mass">&nbsp;Fahrenheit -> Celsius                        
                            </label>
                        </div>

                        <div>                        
                            <label class="radio-inline3">
                                <input type="radio" name="optradio3" value = "pounds" id="temp">&nbsp;Pounds (lbs) -> Kilograms (Kg)                        
                            </label>
                        </div>
                        <div>
                            <label class="radio-inline3">
                                <input type="radio" name="optradio3" value = "kilograms" id="mass">&nbsp;Kilograms (Kg) -> Pounds (lbs)                        
                            </label>
                        </div>
                    </div>
                    </hr>

                    <form action="convertunits" method="POST">
                        <div class="celsInputDiv">                
                            </br>Enter Celsius Value:
                            <input type="number" step="0.1" name="celsInput" id="baseCels">                
                        </div>
                        <div class="fahrInputDiv">                
                            </br>Enter Fahrenheit Value:
                            <input type="number" step="0.1" name="fahrInput" id="baseFahr">                
                        </div>
                        <div class="poundsInputDiv">                
                            </br>Enter Pounds Value:
                            <input type="number" step="0.01" min=0 name="poundsInput" id="basePounds">                
                        </div>
                        <div class="kiloInputDiv" >                
                            </br>Enter Kilogram Value:
                            <input type="number" step="0.01" min=0 name="kiloInput" id="baseKilo">                
                        </div>
                        <div class="convButton">
                            <button type="submit" id="conv-button" class=btn btb-default">Convert</button>
                        </div>
                    </form>
                </div>


                <div class="col-md-6">
                    <h3>Results</h3>
                    <table id="tableid" class="table table-bordered">
                        <tr><td>Conversion From </td><td><c:out value="${conversionFrom}" /></td></tr>
                        <tr><td>Conversion To </td><td><c:out value="${conversionTo}" /></td></tr>                        
                        <tr><td>Conversion From Value </td><td><c:out value="${conversionFromValue}"/></td></tr>
                        <tr><td>Conversion To Value </td><td><c:out value="${conversionToValue}"/></td></tr>
                    </table>
                </div>
            </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

