<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Home Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Application Suite - Spring MVC Application</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/factorizer">Factorizer</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/flooringcalc">Flooring Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/interestcalc">Interest Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/luckysevens">Lucky Sevens</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/tipcalc">Tip Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/unitconv">Unit Converter</a></li>
                </ul>    
            </div>
                
            <div clas="row">

                <div class="col-md-6">
                    <h3>Flooring Calculator</h3>
                    <form role="form" method="POST" action="calcFlooring">
                        Width (ft):<input type=number min=100 max=1999 name="width" class="form-control"><br/>
                        Length (ft):<input type=number min=100 max=1999 name="length" class="form-control"><br/>
                        Cost Per Square Ft:<input type=number min=0 max=199 name="costSqFt" class="form-control"><br/>
                        <button type="submit" value="Submit">Submit</button>
                    </form>

                </div>


                <div class="col-md-6">
                    <h3>Results</h3>
                    <br/>
                    <c:if test="${badInput==true}">
                        Please go back and provide actual correct input.<br />
                        <a href="index.jsp">Return to Flooring Form.</a>
                    </c:if>

                    <table id="tableid" class="table table-bordered">
                        <tr><td>Total Square Feet: <c:out value="${flooringArea}" default="n/a"/></tr></td>
                        <tr><td>Material Cost: $<c:out value="${flooringCost}" default="n/a"/></tr></td>
                        <tr><td>Labor Time: <c:out value="${laborTime}" default="n/a"/> </tr></td>
                        <tr><td>Labor Cost: $<c:out value="${laborCost}" default="n/a"/> </tr></td>
                        <tr><td><i>Labor Cost billed rounded to 15m increments</i></tr></td>
                        <tr><td><i>Labor Cost is <b>$86/hour</b></i></tr></td>

                    </table>





                </div>
            </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

