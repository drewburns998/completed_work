<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Home Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">


    </head>
    <body>
        <div class="container">
            <h1>Application Suite - Spring MVC Application</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/factorizer">Factorizer</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/flooringcalc">Flooring Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/interestcalc">Interest Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/luckysevens">Lucky Sevens</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/tipcalc">Tip Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/unitconv">Unit Converter</a></li>
                </ul>    
            </div>

            <div class="row">
                
                <div class="col-md-6">
                    <h3>Enter Number</h3>
                    <form role="form" method="POST" action="factorize">
                        <input type="number" placeholder="Enter Number" min=0 class="form-control" name="userNum">
                        <button type="submit" class="btn btn-default">Calculate</button>
                    </form>
                </div>

                
                <div class="col-md-6">
                    <h3>Results</h3>
                    <c:if test="${badInput==true}">
                        Sorry, you entered some bad data!
                        <a href="factorize">Return to Calculator.</a>
                    </c:if>
                        <h5>Factors Found:</h5>                        
                    <c:forEach items="${factorList}" var="factorFound">
                        <c:out value="${factorFound}" />
                    </c:forEach>
                    <br/>
                    <c:if test="${perfectNum==true}">
                        Perfect Number!<br/>
                    </c:if>
                    <c:if test="${primeNum==true}">
                        Prime Number!<br/>
                    </c:if>
                </div>
            </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

