/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package creditcardvalidator;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class CreditCardValidator {

    /**
     * 1) VISA - 16 Digits 2) Mastercard - 16 Digits 3) American Express - 15
     * Digits 4) Discover - 16 Digits
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Integer cardType = 0;
        long userInput = 0;
        boolean validCard = true;

        System.out.println("Please enter your card number (no spaces)");
        userInput = sc.nextLong();

        String temp;
        temp = Long.toString(userInput);

        char[] newNumber = new char[temp.length()];

        for (int i = 0; i < temp.length(); i++) {
            newNumber[i] = temp.charAt(i);
        }

        System.out.println("Pick from the following card types:");
        System.out.println("1.Visa" + " 2.Mastercard" + " 3.American Express " + " 4.Discover");
        cardType = sc.nextInt();

        switch (cardType) {
            case 1:
                if (newNumber.length != 16 || newNumber[0] != 4) {
                    System.out.println("testing");
                    validCard = false;
                }
                break;
            case 2:
                if (newNumber.length != 16 || newNumber[0] != 5){// || newNumber[1] > '5')) {
                    validCard = false;
                }
                break;
            case 3:
                if (newNumber.length != 15 || newNumber[0] != 3){// || newNumber[1] != 4 || newNumber[1] != 7) {
                    validCard = false;
                }
                break;
            case 4:
                if (newNumber.length != 16 || newNumber[0] != 6){                       
                    validCard = false;
                }
                break;
        }

        if (validCard == true) {
            System.out.println("Card number is: ");

            for (int i = 0; i < newNumber.length; i++) {
                System.out.print(newNumber[i] + " ");
            }
        } else {
            System.out.println("Invalid number of digits or incorrect starting digit");
        }

    }

}
