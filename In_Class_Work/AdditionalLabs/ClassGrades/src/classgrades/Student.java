/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classgrades;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class Student {
    private String name;
    private ArrayList<Integer> scoreList = new ArrayList<>();
    
    public Student(String name){
        this.name = name;
    }
    
    public String getName(){
        return this.name;
    }
    
    public ArrayList<Integer> getScores(){
        return this.scoreList;
    }
    
    public void addScore(Integer score){
        scoreList.add(score);
    }
    
    public double average(){
        Integer sum = 0;
        Integer count = 0;
        
        for(Integer x: this.scoreList){
            sum = sum + x;
        }
        
        return sum/scoreList.size();
    }
    
}
