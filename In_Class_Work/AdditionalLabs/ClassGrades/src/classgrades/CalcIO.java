/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classgrades;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class CalcIO {

    Scanner sc = new Scanner(System.in);    
    private int min;
    private int max;

    public int readInt(String userInput) {
        System.out.println(userInput);
        return sc.nextInt();
    }

    public int intAndRange(String s, int min, int max) {
        printPrompt(s);
        int userInput = readInt(" ");
        boolean validInt = false;
        
        while (!validInt) {            
            if (userInput >= min && userInput <= max) {
                validInt = true;
            } else {
                System.out.println("Please enter value between " + min + " and " + max);
                userInput = sc.nextInt();
            }
        }
        return userInput;
    }
    
    public double doubleAndRange(String s, double min, double max) {
        printPrompt(s);
        double userInput = readDouble(" ");
        boolean validDouble = false;
        
        while (!validDouble) {            
            if (userInput >= min && userInput <= max) {
                validDouble = true;
            } else {
                System.out.println("Please enter value between " + min + " and " + max);
                userInput = sc.nextDouble();
            }
        }
        return userInput;
    }
    
    public float floatAndRange(String s, float min, float max) {
        printPrompt(s);
        float userInput = readFloat(" ");
        boolean validFloat = false;
        
        while (!validFloat) {            
            if (userInput >= min && userInput <= max) {
                validFloat = true;
            } else {
                System.out.println("Please enter value between " + min + " and " + max);
                userInput = sc.nextFloat();
            }
        }
        return userInput;
    }

    
    public String readString(String stringInput) {
        System.out.println(stringInput);
        return sc.next();
    }

    public float readFloat(String floatInput) {
        System.out.println(floatInput);
        return sc.nextFloat();
    }    

    public Double readDouble(String doubleInput) {
        System.out.println(doubleInput);
        return sc.nextDouble();
    }    

    public void printPrompt(String userInput) {
        System.out.println(userInput);
    }
}
