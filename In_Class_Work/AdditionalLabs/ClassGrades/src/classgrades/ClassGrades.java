/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classgrades;

import java.util.HashMap;

/**
 *
 * @author apprentice
 */
public class ClassGrades {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Student andrew = new Student("Andrew");
        Student carrie = new Student("carrie");
        Student steve = new Student("steve");
        
        andrew.addScore(88);
        andrew.addScore(90);
        andrew.addScore(70);
        andrew.addScore(95);
        
        //HashMap(String, Student) = new HashMap("Andrew", andrew);
        
        System.out.println(andrew.getScores());
        
        //System.out.println(average(andrew.getScores()));
        System.out.println(andrew.average());
        
    }
    
    
}
