/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.classroster.ops;

import java.io.IOException;

/**
 *
 * @author ahill
 */
public interface ClassRosterController {

    public void run() throws IOException;
    
}
