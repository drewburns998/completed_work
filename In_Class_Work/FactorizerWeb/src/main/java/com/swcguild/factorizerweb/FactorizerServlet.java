/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.factorizerweb;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "FactorizerServlet", urlPatterns = {"/FactorizerServlet"})
public class FactorizerServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String userNumString = request.getParameter("userNum");

        if (userNumString == null) {
            request.setAttribute("badInput", true);
        } else {
            try {
                int userNum = Integer.parseInt(userNumString);
                int sumFactor = 0; // addition of factor sumation
                if (userNum <= 0) {
                    request.setAttribute("badInput", true);
                } else {
                    
                    ArrayList<Integer> factors = new ArrayList<>();
                    for (int i = 1; i < userNum; i++) {                        
                        if (userNum % i == 0) {
                            sumFactor = sumFactor + i;
                            factors.add(i);                            
                        }
                    }
                    request.setAttribute("factorList", factors);
                    if (sumFactor == 1) {
                        request.setAttribute("primeNum", true);
                    }
                    if (sumFactor == userNum) {
                        request.setAttribute("perfectNum", true);
                    }
                    
                }
            } catch (NumberFormatException ex) {
                request.setAttribute("badInput", true);
            }

        }
        request.getRequestDispatcher("result.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
