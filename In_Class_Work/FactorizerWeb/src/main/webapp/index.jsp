<%-- 
    Document   : index
    Created on : Mar 28, 2016, 3:05:26 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" />
    </head>
    <body>
        <div>
            <h1 align="left">Factorizer Web Application!</h1>
        </div>
        <form role="form" method="POST" action="FactorizerServlet">
            <div class="col-xs-12 col-md-2">
                <div class="form-group">                
                    <input type="number" placeholder="Enter Number" min=0 class="form-control" name="userNum">
                </div>        
                <button type="submit" class="btn btn-default">Calculate</button>
            </div>
        </form>


    </body>
</html>
