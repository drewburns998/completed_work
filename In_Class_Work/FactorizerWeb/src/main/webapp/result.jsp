<%-- 
    Document   : result
    Created on : Mar 28, 2016, 3:05:40 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Factorizer Results</title>
    </head>
    <body>
        <h1>Here Are the Results</h1>
        
        <c:if test="${badInput==true}">
            Sorry, you entered some bad data!
            <a href="index.jsp">Return to Flooring Form.</a>
        </c:if>

        <c:forEach items="${factorList}" var="factorFound">
            <c:out value="${factorFound}" />
        </c:forEach>
        <br/>
        <c:if test="${perfectNum==true}">
        Perfect<br/>
        </c:if>
        <c:if test="${primeNum==true}">
        Prime<br/>
        </c:if>
    </body>
</html>
