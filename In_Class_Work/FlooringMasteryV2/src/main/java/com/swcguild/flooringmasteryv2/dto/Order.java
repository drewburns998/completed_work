/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.flooringmasteryv2.dto;

import java.text.DecimalFormat;

/**
 *
 * @author apprentice
 */
public class Order {

    private int orderID;
    private String custName;
    private String state;
    private double taxRate; // make sure this was populated
    private String prodType;
    private double area;
    private double matCostSqFt;
    private double laborCostSqFt;
    private double matCost;
    private double laborCost;
    private double tax;
    private double total;
    private String date;
    private String dl = ",";
    DecimalFormat df = new DecimalFormat("#.00"); 
    
    @Override
    public String toString(){
     //11:: 03112015:: Burns:: OH:: 7.25:: Wood:: 100.00:: 5.00:: 1.00:: 500.00:: 100.00:: 43.50:: 643.50 
        String output = orderID + dl + date + dl + custName.replace(",", "::") + dl + state + dl + taxRate 
                + dl + prodType.replace(",", "::")+ dl + area + dl + df.format(matCostSqFt) + dl + df.format(laborCostSqFt) + dl + df.format(matCost) 
                + dl + df.format(laborCost) + dl + df.format(tax) + dl + df.format(total);
        return output;
    }

    public Order() {
    }
    
    public Order(Order originalOrder){
        this.orderID = originalOrder.orderID;        
        this.custName = originalOrder.custName;
        this.state = originalOrder.state;
        this.taxRate = originalOrder.taxRate;
        this.prodType = originalOrder.prodType;
        this.area = originalOrder.area;
        this.matCostSqFt = originalOrder.matCostSqFt;
        this.laborCostSqFt = originalOrder.laborCostSqFt;
        this.matCost = originalOrder.matCost;
        this.laborCost = originalOrder.laborCost;
        this.tax = originalOrder.tax;
        this.total = originalOrder.total;
        this.date = originalOrder.date;
        
    }

    public Order(int orderID, String custName, String state, double taxRate, String prodType,
            double area, double matCostSqFt, double laborCostSqFt, double matCost,
            double laborCost, double tax, double total, String date) {
        this.orderID = orderID;
        this.custName = custName;
        this.state = state;
        this.taxRate = taxRate;
        this.prodType = prodType;
        this.area = area;
        this.matCostSqFt = matCostSqFt;
        this.laborCostSqFt = laborCostSqFt;
        this.matCost = matCost;
        this.laborCost = laborCost;
        this.tax = tax;
        this.total = total;
        this.date = date;
    }

    public Order(int orderID, String custName, String state, String prodType, double area, String date) {
        this.orderID = orderID;
        this.custName = custName;
        this.state = state;
        this.prodType = prodType;
        this.area = area;
        this.date = date;
    }

    public void completeOrder() {
        matCost = matCostSqFt * area;
        laborCost = laborCostSqFt * area;
        tax = (matCost + laborCost) * taxRate;

        total = matCost + laborCost + tax;
        
        df.format(matCost);
        df.format(laborCost);
        df.format(tax);
        df.format(total);
    }

    public String toSummaryString() {
        
        return "Order ID: " + getOrderID() + " || Customer Name: " + getCustName() 
                + " || Material Ordered: " + getProdType() + " || Total Cost: $" + df.format(getTotal()) +
                " || Date: "+ getDate();
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public String getProdType() {
        return prodType;
    }

    public void setProdType(String prodType) {
        this.prodType = prodType;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getMatCostSqFt() {
        return matCostSqFt;
    }

    public void setMatCostSqFt(double matCostSqFt) {
        this.matCostSqFt = matCostSqFt;
    }

    public double getLaborCostSqFt() {
        return laborCostSqFt;
    }

    public void setLaborCostSqFt(double laborCostSqFt) {
        this.laborCostSqFt = laborCostSqFt;
    }

    public double getMatCost() {
        return matCost;
    }

    public void setMatCost(double matCost) {
        this.matCost = matCost;
    }

    public double getLaborCost() {
        return laborCost;
    }

    public void setLaborCost(double laborCost) {
        this.laborCost = laborCost;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    

}
