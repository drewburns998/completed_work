/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.flooringmasteryv2;

import com.swcguild.flooringmasteryv2.ops.FlooringMasteryController;
import com.swcguild.flooringmasteryv2.ops.FlooringMasteryControllerImpl;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Winona "2dubs" Wixson && Andrew "Burn Unit" Burns
 */
public class FlooringMasteryApp {

    /**
     * @param args the command line arguments
     * 
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        FlooringMasteryController cont = ctx.getBean("controller",FlooringMasteryController.class);
        
        cont.run();
        
    }
}
