/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.flooringmasteryv2.dao;

import com.swcguild.flooringmasteryv2.dto.Order;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public interface FlooringMasteryDAO {
    
    public Object stringToObject(String string);
    public void readRatesXML() throws FileNotFoundException; //trying xml
    public void loadRatesXML()throws IOException; // trying xml
    public String objectToString(Object hashMap); //trying xml
    public void loadConfig() throws FileNotFoundException;
    public void loadTaxRates() throws FileNotFoundException;
    public void loadProductInfo() throws FileNotFoundException;
    public void loadOrders()throws FileNotFoundException;
    public void writeToOrdersFile() throws IOException;
    public ArrayList<Order> getOrders(String date);
    public void addNewOrder(Order order);
    public void addEditedOrder(Order order);
    public Order findOrder(int orderID);
    public int getNextOrderNumber();
    public void removeOrder(int orderID);
    public Set<String> listMaterials();
    public Set<String> listStates();
    public boolean prodEnvironment();
    public Set<Integer> listOrderIds();
    public boolean isAdmin(int userPin);
    
}
