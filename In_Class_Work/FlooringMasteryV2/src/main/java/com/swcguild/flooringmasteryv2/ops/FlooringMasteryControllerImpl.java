/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.flooringmasteryv2.ops;

import com.swcguild.flooringmasteryv2.dao.FlooringMasteryDAO;
import com.swcguild.flooringmasteryv2.dao.FlooringMasteryDAOImpl;
import com.swcguild.flooringmasteryv2.dto.Order;
import com.swcguild.flooringmasteryv2.ui.ConsoleIO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class FlooringMasteryControllerImpl implements FlooringMasteryController {

    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_YELLOW = "\u001B[33m";

    ConsoleIO console;// = new ConsoleIO();
    FlooringMasteryDAO dao;// = new FlooringMasteryDAOImpl();
    int addOrderCounter, removeOrderCounter, editOrderCounter;

    public void setConsole(ConsoleIO console) {
        this.console = console;
    }

    public void setDao(FlooringMasteryDAO dao) {
        this.dao = dao;
    }    
    
    @Override
    public void run() throws FileNotFoundException, IOException {
        boolean keepRunning = true;
        dao.loadConfig();
        dao.loadOrders(); //check test or prod
        dao.loadProductInfo();
        //dao.loadTaxRates();
        dao.loadRatesXML();

        if (!dao.prodEnvironment()) {
            console.println(ANSI_RED + "You are in a test environment.\n" + ANSI_RED + "No data will be saved to file.\n" + ANSI_RESET);
        }
        while (keepRunning) {
            printMenu();
            int userInput = console.readInt("Enter a choice:");
            switch (userInput) {
                case 1:
                    displayOrders();
                    break;
                case 2:
                    addOrder();
                    break;
                case 3:
                    if (isAdmin()) {
                        editOrder();
                    } else {
                        console.print("Sorry Austyn.\n\n\t* Next time try comparing "
                                + "Andrew and Winona.  What's the result?"
                                + "\n\t* Or, if you're really slick you'll dive into "
                                + "your \"_ _ _ _ haxor\" knowledge!!!"
                                + "\n\n\t* If really uncertain, your " + ANSI_RED + "bank pin" + ANSI_RESET + " is valid as well."
                                + "(QUEUE Spooky X Files Theme)\n");
                    }
                    break;
                case 4:
                    removeOrder();
                    break;
                case 5:
                    seeAllOrderIds();
                case 6:
                    saveWork();
                    break;
                case 7:
                    saveWork();
                    console.println("\nSession Summary:\n" + ANSI_GREEN + "You added " + addOrderCounter
                            + " order(s).\n" + ANSI_RED + "You removed " + removeOrderCounter
                            + " order(s).\n" + ANSI_YELLOW + "You edited " + editOrderCounter + " order(s)." + ANSI_RESET + "\n");

                    keepRunning = false;
                    break;
            }
        }
    }

    private void printMenu() {
        console.println("\nWelcome to the Inventory Manager!");
        console.println("1. Display Orders");
        console.println("2. Add an Order");
        console.println("3. Edit an Order");
        console.println("4. Remove an Order");
        console.println("5. See all Order IDs");
        console.println("6. Save Current Work");
        console.println("7. Quit");
    }

    private void seeAllOrderIds() {
        int j = 0;
        console.print("\nOrder IDs: ");
        for (int i : dao.listOrderIds()) {
            if (j % 6 == 0) {
                console.print("\n");
            }
            console.print(i + "\t");
            j++;
        }
        console.print("\n");
    }

    private void displayOrders() {
        String userInput = console.readString("Enter date in the following format MMDDYYYY: ");
        ArrayList<Order> orders = dao.getOrders(userInput);

        for (Order o : orders) {
            console.println(o.toSummaryString());
        }
    }

    private void addOrder() {

        boolean correctOrder = false;
        boolean correctDate = false;
        boolean correctMaterial = false;
        boolean correctState = false;
        boolean keepRunning = true;
        String custName = "";
        String ordDate = "";
        String custState = "";
        String matType = "";
        double area = 0;

        while (!correctOrder && keepRunning) {
            custName = console.readString("Enter customer name: ");

            while (!correctDate && keepRunning) {
                ordDate = console.readString("Enter Order Date MMDDYYYY or exit to stop: ");
                if (ordDate.equalsIgnoreCase("exit")) {
                    keepRunning = false;
                } else {
                    correctDate = dateValid(ordDate);
                }
            }

            while (!correctState && keepRunning) {
                custState = console.readString("Enter purchase State (ex OH,NY, etc) or Exit to stop: ").toUpperCase();
                Set<String> states = dao.listStates();

                if (states.contains(custState)) {
                    correctState = true;
                } else if (custState.equalsIgnoreCase("exit")) {
                    console.println("Quit at the state input section.");
                    keepRunning = false;
                }
            }

            while (!correctMaterial && keepRunning) {
                console.println("Which of the following materials would you like to order?");
                Set<String> materials = dao.listMaterials();
                for (String m : materials) {
                    console.println(m);
                }
                matType = console.readString("Choice, or Exit to stop: ").toLowerCase();
                if (materials.contains(matType)) {
                    correctMaterial = true;
                } else if (matType.equalsIgnoreCase("exit")) {
                    console.println("Quit at the material input section.");
                    keepRunning = false;
                }
            }
            if (keepRunning) {
                area = console.readDouble("Amount of Material in sq ft: ");
                console.println("Customer name: " + custName);
                console.println("Order date: " + ordDate);
                console.println("State: " + custState);
                console.println("Material: " + matType);
                console.println("Area: " + area);

                String userResponse = console.readString("Is this information correct (yes or no)? ");
                if (userResponse.equalsIgnoreCase("yes")) {
                    correctOrder = true;
                } else {
                    String finalUserInput = console.readString("Q to Quit or R to start a new order");
                    if (finalUserInput.equalsIgnoreCase("q")) {
                        keepRunning = false;
                    } else {
                        correctOrder = false;
                        correctDate = false;
                        correctMaterial = false;
                        correctState = false;
                        keepRunning = true;
                    }
                }
            }
        }
        if (keepRunning && correctOrder) {
            Order newOrder = new Order(dao.getNextOrderNumber(), custName, custState, matType, area, ordDate);
            dao.addNewOrder(newOrder);
            console.println("Your order has been added.");
            addOrderCounter++;
        }
    }

    private void editOrder() {
        int orderID = console.readInt("Enter the Order ID to Edit");
        Order orderToEdit = dao.findOrder(orderID);
        if (orderToEdit == null) {
            console.println("Invalid Order Id. Returning to main menu.");
            return;
        }

        Order tempOrder = new Order(orderToEdit);

        String orderDetails = orderToEdit.toString();
        String[] orderRecords = orderDetails.split(",");

        console.println("Here are the order details.");
        for (int i = 1; i < 13; i++) {
            console.println(i + ": " + orderRecords[i]);
        }
        boolean updateOrderValid = false;
        boolean keepEditing = true;
        while (keepEditing) {
            int editChoice = console.readInt("Enter the number of the detail you would like to edit, enter 13 to quit, or enter 14 to complete the edited order: ");

            //orderID + dl + date + dl + custName + dl + state + dl + taxRate 
            //+ dl + prodType + dl + area + dl + matCostSqFt + dl + laborCostSqFt + dl + matCost 
            //+ dl + laborCost + dl + tax + dl + total;
            switch (editChoice) {
                case 1://date
                    String dateEdit = console.readString("Enter new date or press enter to continue(" + orderToEdit.getDate() + "): ");
                    if (!dateEdit.isEmpty()) {
                        tempOrder.setDate(dateEdit);
                    }
                    break;
                case 2://name
                    String nameEdit = console.readString("Enter new name or press enter to continue(" + orderToEdit.getCustName() + "): ");
                    if (!nameEdit.isEmpty()) {
                        tempOrder.setCustName(nameEdit);
                    }
                    break;
                case 3://state
                    String stateEdit = console.readString("Enter new state or press enter to continue(" + orderToEdit.getState() + "): ");
                    if (!stateEdit.isEmpty()) {
                        tempOrder.setState(stateEdit);
                    }
                    break;
                case 4://taxrate
                    double taxRateEdit = console.readDouble("Enter new tax rate or press 0 to continue($" + orderToEdit.getTaxRate() + "): ");
                    if (taxRateEdit != 0) {
                        tempOrder.setTaxRate(taxRateEdit);
                    }
                    break;
                case 5://material
                    String matEdit = console.readString("Enter new material or press enter to continue(" + orderToEdit.getProdType() + "): ");
                    if (!matEdit.isEmpty()) {
                        tempOrder.setDate(matEdit);
                    }
                    break;
                case 6://area
                    double areaEdit = console.readDouble("Enter new tax rate or press 0 to continue(" + orderToEdit.getArea() + "): ");
                    if (areaEdit != 0) {
                        tempOrder.setArea(areaEdit);
                    }
                    break;
                case 7://matcossqft
                    double matSqFtEdit = console.readDouble("Enter new material SqFt Cost or press 0 to continue($" + orderToEdit.getMatCostSqFt() + "): ");
                    if (matSqFtEdit != 0) {
                        tempOrder.setMatCostSqFt(matSqFtEdit);
                    }
                    break;
                case 8://labcostsqft
                    double labSqFtEdit = console.readDouble("Enter new labor SqFt Cost or press 0 to continue($" + orderToEdit.getLaborCostSqFt() + "): ");
                    if (labSqFtEdit != 0) {
                        tempOrder.setLaborCostSqFt(labSqFtEdit);
                    }
                    break;
                case 9://matcost
                    double matCostEdit = console.readDouble("Enter new material Cost or press 0 to continue($" + orderToEdit.getMatCost() + "): ");
                    if (matCostEdit != 0) {
                        tempOrder.setMatCost(matCostEdit);
                    }
                    break;
                case 10://laborcost
                    double labCostEdit = console.readDouble("Enter new labor Cost or press 0 to continue($" + orderToEdit.getLaborCost() + "): ");
                    if (labCostEdit != 0) {
                        tempOrder.setLaborCost(labCostEdit);
                    }
                    break;
                case 11://tax
                    double taxEdit = console.readDouble("Enter new total tax or press 0 to continue($" + orderToEdit.getTax() + "): ");
                    if (taxEdit != 0) {
                        tempOrder.setTax(taxEdit);
                    }
                    break;
                case 12://total
                    double totalEdit = console.readDouble("Enter new total or press 0 to continue($" + orderToEdit.getTotal() + "): ");
                    if (totalEdit != 0) {
                        tempOrder.setTotal(totalEdit);
                    }
                    break;
                case 13: //quit
                    keepEditing = false;
                    break;
                default://break out of the while loop
                    //ask if order is valid or not
                    String validOrder = console.readString("Are you sure you want to edit this order?");
                    if (validOrder.equalsIgnoreCase("yes")) {
                        updateOrderValid = true;
                    }
                    keepEditing = false;
                    break;
            }
        }

        if (updateOrderValid) {
            dao.addEditedOrder(tempOrder);
            editOrderCounter++;
        }

    }

    private void removeOrder() {
        int orderID = console.readInt("What is the ID of the order you would like to remove? ");
        Order orderToRemove = dao.findOrder(orderID);
        console.println(orderToRemove.toSummaryString());
        String userInput = console.readString("Are you sure you would like to remove this order (yes or no)? ");
        if (userInput.equalsIgnoreCase("yes")) {
            dao.removeOrder(orderID);
            removeOrderCounter++;
        } else {
            console.println("The order has not been removed.");
        }

    }

    private void saveWork() throws IOException {
        dao.writeToOrdersFile();
    }

    private boolean dateValid(String inputDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy");
        sdf.setLenient(false); //makes it so we don't allow for invalid dates

        try {
            Date validDate = sdf.parse(inputDate);
            return true;
        } catch (ParseException ex) {
            return false;
        }

    }

    private boolean isAdmin() {
        int userPin = console.readInt("Enter your administrator pin to edit orders.");
        return dao.isAdmin(userPin);
    }

}
