/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.flooringmasteryv2.dao;

import com.swcguild.flooringmasteryv2.dto.Material;
import com.swcguild.flooringmasteryv2.dto.Order;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class FlooringMasteryDAOImpl implements FlooringMasteryDAO {

    HashMap<Integer, Order> allOrders = new HashMap<>();
    HashMap<String, Double> allTaxRates = new HashMap<>();
    HashMap<String, Material> allProductInfo = new HashMap<>();
    HashMap<String, ArrayList<Order>> ordersByDate = new HashMap<>(); // Used for significantly faster file writing.. 2.5n vs n^n 

    String DELIMITER = ",";
    boolean isProdEnvironment = false; //test or production

    //************** XML LOAD XML LOAD XML LOAD XML LOAD********************8
    @Override
    public void loadRatesXML() throws IOException {

        String content = new Scanner(new File("xmlTaxRates.xml")).useDelimiter("end").next();

        HashMap<String, Double> parsedMap = (HashMap) stringToObject(content);
        allTaxRates = parsedMap;
    }

    @Override
    public String objectToString(Object hashMap) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        XMLEncoder xmlEncoder = new XMLEncoder(bos);
        xmlEncoder.writeObject(hashMap);
        xmlEncoder.close();
        return bos.toString();
    }

    @Override
    public void readRatesXML() throws FileNotFoundException {
        String result = objectToString(allTaxRates);

    }

    @Override
    public Object stringToObject(String string) {
        XMLDecoder xmlDecoder = new XMLDecoder(new ByteArrayInputStream(string.getBytes()));
        return xmlDecoder.readObject();
    }

    //************** XML LOAD XML LOAD XML LOAD XML LOAD********************8
    @Override
    public void loadOrders() throws FileNotFoundException {
        if (isProdEnvironment) {
            File dir = new File("./Orders");

            for (File file : dir.listFiles()) {

                Scanner sc = new Scanner(new BufferedReader(new FileReader(file)));

                while (sc.hasNextLine()) {
                    String recordLine = sc.nextLine().trim();
                    String[] recordProperties = recordLine.split(DELIMITER);
                    if (recordProperties.length != 13) {
                        continue;
                    }

                    int orderID = Integer.parseInt(recordProperties[0]);
                    String orderDate = recordProperties[1];
                    String custName = recordProperties[2].replace("::", ",");
                    String custState = recordProperties[3];
                    Double taxRate = Double.parseDouble(recordProperties[4]);
                    String matType = recordProperties[5].replace("::", ",");
                    Double area = Double.parseDouble(recordProperties[6]);
                    Double matCostSqFt = Double.parseDouble(recordProperties[7]);
                    Double labCostSqFt = Double.parseDouble(recordProperties[8]);
                    Double matCost = Double.parseDouble(recordProperties[9]);
                    Double labCost = Double.parseDouble(recordProperties[10]);
                    Double taxAmt = Double.parseDouble(recordProperties[11]);
                    Double totalCost = Double.parseDouble(recordProperties[12]);

                    //int orderID, String custName, String state, double taxRate, String prodType, double area, 
                    //double matCostSqFt, double laborCostSqFt, double matCost, double laborCost, double tax, double total, String date
                    Order orderToAdd = new Order(orderID, custName, custState, taxRate, matType, area,
                            matCostSqFt, labCostSqFt, matCost, labCost, taxAmt, totalCost, orderDate);

                    allOrders.put(orderID, orderToAdd);
                }
            }
        }
    }

    @Override
    public void loadConfig() throws FileNotFoundException { //revisit
        Scanner sc = new Scanner(new BufferedReader(new FileReader("config.txt")));
        while (sc.hasNextLine()) {
            String recordLine = sc.nextLine();

            if (recordLine.equalsIgnoreCase("prod")) {
                isProdEnvironment = true;
            }
        }
    }

    @Override
    public void loadTaxRates() throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader("taxRates.txt")));
        while (sc.hasNextLine()) {
            String recordLine = sc.nextLine();
            String[] recordProperties = recordLine.split(DELIMITER);
            if (recordProperties.length != 2) {
                continue;
            }

            String state = recordProperties[0];
            double taxRate = Double.parseDouble(recordProperties[1]);

            allTaxRates.put(state, taxRate);
        }
    }

    @Override
    public void loadProductInfo() throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader("materials.txt")));
        while (sc.hasNextLine()) {
            String recordLine = sc.nextLine();
            String[] recordProperties = recordLine.split(DELIMITER);
            if (recordProperties.length != 3) {
                continue;
            }

            String material = recordProperties[0];
            double matCostSqFt = Double.parseDouble(recordProperties[1]);
            double labCostSqFt = Double.parseDouble(recordProperties[2]);
            Material mat = new Material(material, matCostSqFt, labCostSqFt);

            allProductInfo.put(material, mat);
        }
    }

    @Override
    public void writeToOrdersFile() throws IOException {
        if (isProdEnvironment) {

            makeOrderByDateMap();

            File dir = new File("./Orders");
            File dirBackup = new File("./OrdersBackup");

            //Create backup copies of orders before writing new orders
            for (File file : dir.listFiles()) {
                String path = "./OrdersBackup";
                Files.copy(file.toPath(),
                        (new File(path, file.getName())).toPath(),
                        StandardCopyOption.REPLACE_EXISTING);
            }

            //Remove all files from Orders
            for (File file : dir.listFiles()) {
                file.delete();
            }

            for (String d : ordersByDate.keySet()) {
                String fileName = "Orders_" + d + ".txt";
                PrintWriter pw = new PrintWriter(new FileWriter(new File("./Orders", fileName)));

                for (Order o : ordersByDate.get(d)) {
                    pw.println(o);
                }

                pw.flush();
                pw.close();
            }
        }
    }

    private void makeOrderByDateMap() {

        ordersByDate = new HashMap<>();

        for (Order o : allOrders.values()) {
            if (ordersByDate.get(o.getDate()) != null) {
                ordersByDate.get(o.getDate()).add(o);
            } else {
                ArrayList<Order> tempList = new ArrayList<>();
                tempList.add(o);
                ordersByDate.put(o.getDate(), tempList);
            }
        }
    }

    @Override
    public ArrayList<Order> getOrders(String date) {
        Collection<Order> orders = allOrders.values();
        ArrayList<Order> ordersOfDate = new ArrayList<Order>(allOrders.values());

        for (Order o : orders) {
            if (!date.equals(o.getDate())) {
                ordersOfDate.remove(o);
            }
        }
        return ordersOfDate;
    }

    @Override
    public void addNewOrder(Order order) {

        order.setLaborCostSqFt(allProductInfo.get(order.getProdType()).getLaborCostPerSqFt());
        order.setMatCostSqFt(allProductInfo.get(order.getProdType()).getCostPerSqFt());
        order.setTaxRate(allTaxRates.get(order.getState()));

        order.completeOrder(); //finishes populating order object
        Order finalOrder = allOrders.put(order.getOrderID(), order);

    }

    @Override
    public void addEditedOrder(Order order) {
        allOrders.put(order.getOrderID(), order);
    }

    @Override
    public Set<String> listMaterials() {
        return allProductInfo.keySet();
    }

    @Override
    public Set<String> listStates() {
        return allTaxRates.keySet();
    }

    @Override
    public Order findOrder(int orderID) {
        return allOrders.get(orderID);
    }

    @Override
    public int getNextOrderNumber() {
        Set<Integer> getAllOrderIds = allOrders.keySet();
        int maxOrderId = 0;

        for (Integer i : getAllOrderIds) {
            if (i > maxOrderId) {
                maxOrderId = i;
            }
        }
        return maxOrderId + 1;
    }

    @Override
    public Set<Integer> listOrderIds() {
        return allOrders.keySet();
    }

    @Override
    public void removeOrder(int orderID) {
        allOrders.remove(orderID);
    }

    @Override
    public boolean prodEnvironment() {
        return isProdEnvironment;
    }

    @Override
    public boolean isAdmin(int userPin) {
        return userPin == "winona".compareTo("andrew")
                || userPin == Integer.parseInt("n".compareTo("a")
                        + "" + "d".compareTo("a") + "" + "h".compareTo("a"));
    }

}
