/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.aspects;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 *
 * @author apprentice
 */
public class LoadMonitor {
    
    public void alertStart(){
        System.out.println("Loading Flooring Store ... ");
    }
    
    public void alertFinish(){
        System.out.println("Flooring Store Shut Down.");        
    }
    
    public Object timeMethod(ProceedingJoinPoint jp){
        Object ret = null;        
        
        try {
            long start = System.currentTimeMillis();
            ret = jp.proceed();
            long end = System.currentTimeMillis();
            System.out.println("*************");
            System.out.println("Flooring App Accessed for " + (end - start)/1000 + " seconds");
            System.out.println("*************");
            
        } catch (Throwable ex) {
            System.out.println("Exception occurred in SimpleTimerAdvice.timeMethod()");
        }        
        
        return ret;
        
    }
}
