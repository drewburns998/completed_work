/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flooringmastery.dao;

import com.swcguild.flooringmasteryv2.dao.FlooringMasteryDAOImpl;
import com.swcguild.flooringmasteryv2.dto.Order;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class FlooringMasteryDAOImplTest {

    FlooringMasteryDAOImpl testObj = new FlooringMasteryDAOImpl();

    public FlooringMasteryDAOImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        //orderID, String custName, String state, double taxRate, String prodType,
        //double area, double matCostSqFt, double laborCostSqFt, double matCost,
        //double laborCost, double tax, double total, String date
//        Order testOrder = new Order(300, "Andrew", "OH", 0.0725, "wood",
//        100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03192015");
//        
//        Order testOrder2 = new Order(400, "Winona", "OH", 0.0725, "wood",
//        100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03202015");
//        
//        testObj.allOrders.put(testOrder.getOrderID(), testOrder);
//        testObj.allOrders.put(testOrder2.getOrderID(), testOrder2);
//        
//        Material testMaterial = new Material("wood", 1.00, 2.00);
//        testObj.allProductInfo.put(testMaterial.getProductType(), testMaterial);
//        
//        testObj.allTaxRates.put("OH", .075);        

    }

    @After
    public void tearDown() {
    }

    @Test
    public void getOrdersWhenNoOrdersHaveBeenAdded() {
        ArrayList<Order> orderContainer = testObj.getOrders("03192015");
        Assert.assertTrue(orderContainer.size() == 0);
    }

    @Test
    public void getOrdersWhenOneOrderHaveBeenAdded() {
        Order testOrder = new Order(300, "Andrew", "OH", 0.0725, "wood",
                100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03192015");

        testObj.addEditedOrder(testOrder);

        ArrayList<Order> orderContainer = testObj.getOrders("03192015");

        Assert.assertTrue(orderContainer.get(0).getOrderID() == 300);
        Assert.assertEquals(testOrder, orderContainer.get(0));
    }

    @Test
    public void getOrdersWithWrongDate() {
        Order testOrder = new Order(300, "Andrew", "OH", 0.0725, "wood",
                100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03192015");

        testObj.addEditedOrder(testOrder);

        ArrayList<Order> orderContainer = testObj.getOrders("03202015");

        Assert.assertTrue(orderContainer.isEmpty());
    }

    @Test
    public void getOneOrderWithMultOrdDatesExisting() {
        Order testOrder = new Order(300, "Andrew", "OH", 0.0725, "wood",
                100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03192015");

        Order testOrder2 = new Order(301, "Winona", "OH", 0.0725, "wood",
                100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03202015");

        testObj.addEditedOrder(testOrder);
        testObj.addEditedOrder(testOrder2);

        ArrayList<Order> orderContainer = testObj.getOrders("03202015");
        ArrayList<Order> orderContainer2 = testObj.getOrders("03192015");

        Assert.assertEquals(orderContainer.get(0), testOrder2);
        Assert.assertEquals(orderContainer2.get(0), testOrder);
    }

    @Test
    public void testAddOneOrder() {

        Order testOrder = new Order(300, "Andrew", "OH", 0.0725, "wood",
                100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03192015");

        testObj.addEditedOrder(testOrder);
        Order foundOrder = testObj.findOrder(300);

        Assert.assertEquals(testOrder, foundOrder);
    }

    @Test
    public void testAddOrderAndLookUpWrongID() {
        Order testOrder = new Order(300, "Andrew", "OH", 0.0725, "wood",
                100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03192015");

        testObj.addEditedOrder(testOrder);
        Order foundOrder = testObj.findOrder(301);

        Assert.assertNotSame(testOrder, foundOrder);
        Assert.assertNull(foundOrder);
    }

    @Test
    public void testAddMultOrdersAndLookUpCorrectIDWithFindOrderMethod() {
        Order testOrder = new Order(300, "Andrew", "OH", 0.0725, "wood",
                100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03192015");

        Order testOrder2 = new Order(301, "Winona", "OH", 0.0725, "wood",
                100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03202015");

        testObj.addEditedOrder(testOrder);
        testObj.addEditedOrder(testOrder2);

        Order foundOrder = testObj.findOrder(301);
        Order foundOrder2 = testObj.findOrder(300);

        Assert.assertEquals(testOrder, foundOrder2);
        Assert.assertEquals(testOrder2, foundOrder);
    }

    @Test
    public void testListMaterials() {
        Assert.assertTrue(testObj.listMaterials().isEmpty());

    }

    @Test
    public void testListMaterialsLoadingMaterials() throws FileNotFoundException {
        testObj.loadProductInfo();
        Assert.assertFalse(testObj.listMaterials().isEmpty());
    }

    @Test
    public void testListStates() {
        Assert.assertTrue(testObj.listStates().isEmpty());
    }

    @Test
    public void testListStates2() {
        Assert.assertTrue(testObj.listStates().isEmpty());
    }

    @Test
    public void testListStatesLoadingStates() throws IOException {
        testObj.loadRatesXML();
        Assert.assertFalse(testObj.listStates().isEmpty());
        Assert.assertEquals(testObj.listStates().size(), 50);
    }

    @Test
    public void testGetNextOrderNumberWithNoOrders() {
        Assert.assertEquals(testObj.getNextOrderNumber(), 1);
    }

    @Test
    public void testGetNextOrderNumberWithOneOrder() {
        Order testOrder = new Order(1, "Andrew", "OH", 0.0725, "wood",
                100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03192015");

        testObj.addEditedOrder(testOrder);

        Assert.assertEquals(testObj.getNextOrderNumber(), 2);
    }

    @Test
    public void testGetNextOrderNumberWithMultipleOrders() {

        Order testOrder = new Order(100, "Andrew", "OH", 0.0725, "wood",
                100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03192015");

        Order testOrder2 = new Order(301, "Winona", "OH", 0.0725, "wood",
                100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03202015");

        testObj.addEditedOrder(testOrder);
        testObj.addEditedOrder(testOrder2);

        Assert.assertEquals(testObj.getNextOrderNumber(), 302);
    }

    @Test
    public void testRemoveOrderNoOrders() {
        testObj.removeOrder(100);

        Assert.assertNull(testObj.findOrder(100));
    }

    @Test
    public void testRemoveOrderWithOneOrders() {
        Order testOrder = new Order(100, "Andrew", "OH", 0.0725, "wood",
                100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03192015");

        testObj.addEditedOrder(testOrder);

        testObj.removeOrder(100);

        Assert.assertNull(testObj.findOrder(100));
    }

    @Test
    public void testRemoveOrderWithManyOrders() {
        Order testOrder = new Order(100, "Andrew", "OH", 0.0725, "wood",
                100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03192015");

        Order testOrder2 = new Order(301, "Winona", "OH", 0.0725, "wood",
                100.0, 5.00, 1.00, 500.00, 100.00, 43.50, 643.50, "03202015");

        testObj.addEditedOrder(testOrder);
        testObj.addEditedOrder(testOrder2);

        testObj.removeOrder(100);

        Assert.assertNull(testObj.findOrder(100));
        Assert.assertEquals(testObj.findOrder(301), testOrder2);
    }

    @Test
    public void testProdEnvironment() {
        Assert.assertFalse(testObj.prodEnvironment());
    }

    @Test
    public void testProdEnvironmentLoadConfig() throws FileNotFoundException {
        testObj.loadConfig();

        Assert.assertNotNull(testObj.prodEnvironment());
    }

}
