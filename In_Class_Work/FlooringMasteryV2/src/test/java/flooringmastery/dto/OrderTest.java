/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flooringmastery.dto;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import com.swcguild.flooringmasteryv2.dto.Order;

/**
 *
 * @author apprentice
 */
public class OrderTest {

    Order testObj = new Order(300, "Andrew", "OH", 0.0725, "wood",
            100.0, 5.00, 1.00, 500, 100.00, 43.50, 643.50, "03192015");

    public OrderTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testToString() {
        String testString = testObj.toString();

        String expectedOutput = "300,03192015,Andrew,OH,0.0725,wood,"
                + "100.0,5.00,1.00,500.00,100.00,43.50,643.50";

        Assert.assertEquals(testString, expectedOutput);
    }

    @Test
    public void testToSummaryString() {
//        "Order ID: " + getOrderID() + " || Customer Name: " + getCustName() 
//                + " || Material Ordered: " + getProdType() + " || Total Cost: " + getTotal() +
//                " || Date: "+ getDate();
        String testString = testObj.toSummaryString();
        String expectedOutput = "Order ID: 300 || Customer Name: Andrew || Material"
                + " Ordered: wood || Total Cost: $643.50 || Date: 03192015";

        Assert.assertEquals(testString, expectedOutput);
    }

}
