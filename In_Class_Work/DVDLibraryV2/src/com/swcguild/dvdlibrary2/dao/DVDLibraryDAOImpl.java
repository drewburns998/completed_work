/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dvdlibrary2.dao;

import com.swcguild.dvdlibrary2.dto.DVD;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class DVDLibraryDAOImpl implements DVDLibraryDAO {

    HashMap<String, DVD> DVDList = new HashMap<>();

    public static final String DVD_LIBRARY_FILE = "DVDLibrary.txt";
    private static final String DELIMITER = "::";
    

    @Override
    public void addDVD(DVD newDVD) {
        DVDList.put(newDVD.getTitle(), newDVD);
    }

    @Override
    public DVD removeDVD(String title) {
        return DVDList.remove(title);
    }

    @Override
    public DVD findDVD(String title) {
        return DVDList.get(title);
    }

    @Override
    public List<DVD> searchDVDs(Predicate<DVD> dvdSearch) {
        
        List<DVD> result = DVDList.values()
                .stream()
                .filter(dvdSearch)
                .collect(Collectors.toList());
        return result;
    }

    @Override
    public void loadDVDLibrary() throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader(DVD_LIBRARY_FILE)));

        while (sc.hasNextLine()) {
            String recordLine = sc.nextLine();
            String[] recordProperties = recordLine.split(DELIMITER);

            if (recordProperties.length != 6) {
                continue;
            }

            String title = recordProperties[0];
            String studio = recordProperties[1];
            int releaseDate = Integer.parseInt(recordProperties[2]);
            String directorName = recordProperties[3];
            String rating = recordProperties[4];
            String notes = recordProperties[5];

            DVD dvdFromFile = new DVD(title, studio, releaseDate, directorName,
                    rating, notes);

            DVDList.put(dvdFromFile.getTitle(), dvdFromFile);
        }
    }

    @Override
    public void writeDVDLibrary() throws IOException {
        PrintWriter writer = new PrintWriter(new FileWriter(DVD_LIBRARY_FILE));

        Collection<DVD> allDVDsInHashMap = DVDList.values();

        allDVDsInHashMap.stream().forEach((d) -> {
            writer.println(d.getTitle() + DELIMITER
                    + d.getStudio() + DELIMITER
                    + d.getReleaseDate() + DELIMITER + d.getDirectorName()
                    + DELIMITER + d.getRating() + DELIMITER + d.getNotes());
        });
        writer.flush();
        writer.close();
    }

    @Override
    public Collection<DVD> getAllDVDs() {
        Collection<DVD> allTitles 
                = DVDList.values()
                  .stream()
                  .collect(Collectors.toList());
        
        return allTitles;
    }

}
