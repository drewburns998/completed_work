/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dvdlibrary2.dao;

import com.swcguild.dvdlibrary2.dto.DVD;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

/**
 *
 * @author apprentice
 */
public interface DVDLibraryDAO {

    public void addDVD(DVD newDVD);

    public DVD removeDVD(String title);

    public DVD findDVD(String title);

    public List<DVD> searchDVDs(Predicate<DVD> dvdSearch);

    public void loadDVDLibrary() throws FileNotFoundException;

    public void writeDVDLibrary() throws IOException;

    public Collection<DVD> getAllDVDs();
}
