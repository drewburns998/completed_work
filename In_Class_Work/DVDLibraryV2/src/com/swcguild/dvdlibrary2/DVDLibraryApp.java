/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dvdlibrary2;

import com.swcguild.dvdlibrary2.ops.DVDLibraryController;
import java.io.IOException;

/**
 *
 * @author apprentice
 */
public class DVDLibraryApp {
    
    public static void main(String[] args) throws IOException {
        
        DVDLibraryController controller = new DVDLibraryController();

        controller.run();
    }
}
