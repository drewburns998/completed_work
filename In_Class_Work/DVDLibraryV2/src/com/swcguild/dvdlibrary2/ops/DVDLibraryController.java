/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dvdlibrary2.ops;

import com.swcguild.dvdlibrary2.dao.DVDLibraryDAO;
import com.swcguild.dvdlibrary2.dao.DVDLibraryDAOImpl;
import com.swcguild.dvdlibrary2.dto.DVD;
import com.swcguild.dvdlibrary2.ui.ConsoleIO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class DVDLibraryController {

    private ConsoleIO console = new ConsoleIO();
    private DVDLibraryDAO daoLayer = new DVDLibraryDAOImpl();

    public void run() throws IOException {

        boolean keepRunning = true;
        
        try {
            daoLayer.loadDVDLibrary();
        } catch (FileNotFoundException ex) {
            keepRunning = false;
            console.print("DVD for which you are looking is not currently available");
        }

        while (keepRunning) {
            printMenu();
            int menuSelect = console.readInt("Select an option...");
            switch (menuSelect) {

                case 1:
                    addDVD();
                    break;

                case 2: //Remove DVD from Collection
                    removeDVD();
                    break;

                case 3: //List All Titles in Collection
                    getAllDVDs();
                    break;

                case 4: //Keyword Search for particular DVD's
                    searchDVDs();
                    break;

                case 5: //exit
                    daoLayer.writeDVDLibrary();
                    keepRunning = false;
                    break;
            }
        }

    }

    private void printMenu() {
        console.print("Main Menu");
        console.print("1. Add DVD to Collection");
        console.print("2. Remove DVD from Collection");
        console.print("3. List All Titles in Collection");
        console.print("4. Flexible Search for different DVD criteria");
        console.print("5. Exit");
    }

    private void getAllDVDs() {
        console.print("Full Collection");
        Collection<DVD> allTitles = daoLayer.getAllDVDs();

        allTitles.stream().forEach((d) -> {
            titleSummary(d);
        });

    }

    private void titleSummary(DVD title) {
        console.print(title.getTitle() + " | Release Date: " + title.getReleaseDate()
                + " | MPAA Rating: " + title.getRating() + " | Director: " + title.getDirectorName()
                + " | Studio: " + title.getStudio() + " | Notes: \"" + title.getNotes() + "\"\n");
    }

    private void searchDVDs() {
        List<DVD> titleResult;
        Predicate<DVD> dvdPred;
        int year = Calendar.getInstance().get(Calendar.YEAR);
        console.print("Search Options");

        console.print("1. Find All Movies released in the last N Years.");
        console.print("2. Find All Movies with a given MPAA Rating");
        console.print("3. Find movies by a given director.");
        console.print("4. Find all movies from a certain studio.");
        console.print("5. Average age of movies in collection");
        console.print("6. Newest movie in collection");
        console.print("7. Oldest movie in collection");
        console.print("8. Average number of notes for each movie");
        console.print("9. Exit Search");

        int userChoice = console.readInt("Enter a choice from the menu above.");

        switch (userChoice) {
            case 1:
                int yearsBack = console.readInt("Enter number of years to search back");
                dvdPred = a -> a.getReleaseDate() >= (year - yearsBack);
                titleResult = daoLayer.searchDVDs(dvdPred);
                if(titleResult.isEmpty()){
                    console.print("No Results found!");
                }
                titleResult.stream().forEach((d) -> {
                    titleSummary(d);
                });
                break;

            case 2:
                String mpaaRating = console.readString("Enter the MPAA rating");
                dvdPred = a -> a.getRating().equalsIgnoreCase(mpaaRating);
                titleResult = daoLayer.searchDVDs(dvdPred);
                if(titleResult.isEmpty()){
                    console.print("No Results found!");
                }
                titleResult.stream().forEach((d) -> {
                    titleSummary(d);
                });
                break;

            case 3:
                String director = console.readString("Enter the Director");
                dvdPred = a -> a.getDirectorName().equalsIgnoreCase(director);
                titleResult = daoLayer.searchDVDs(dvdPred);
                if(titleResult.isEmpty()){
                    console.print("No Results found!");
                }
                titleResult.stream().forEach((d) -> {
                    titleSummary(d);
                });
                break;

            case 4:
                String studio = console.readString("Enter the studio");
                dvdPred = a -> a.getStudio().equalsIgnoreCase(studio);
                titleResult = daoLayer.searchDVDs(dvdPred);
                if(titleResult.isEmpty()){
                    console.print("No Results found!");
                }
                titleResult.stream().forEach((d) -> {
                    titleSummary(d);
                });
                break;

            case 5:
                //avg age of movies in collection
                double averageYear = daoLayer.getAllDVDs()
                        .stream()
                        .mapToLong(DVD::getReleaseDate)
                        .average()
                        .getAsDouble();
                double averageAge = year - averageYear;

                console.print("Average age: " + averageAge);
                break;

            case 6:
                //newest
                DVD newestDVD = daoLayer.getAllDVDs()
                        .stream()
                        .max(Comparator.comparing(d -> d.getReleaseDate()))
                        .get();
                titleSummary(newestDVD);
                break;

            case 7:
                //oldest
                DVD oldestDVD = daoLayer.getAllDVDs()
                        .stream()
                        .min(Comparator.comparing(d -> d.getReleaseDate()))
                        .get();
                titleSummary(oldestDVD);
                break;

            case 8:
                //average number of notes
                break;
            case 9://exit
                break;
        }

    }

    private void addDVD() {
        String title = console.readString("Enter the title of the DVD you wish to add: ");
        String studio = console.readString("Enter the studio: ");
        int releaseDate = console.readInt("Enter the release year: ");
        String directorName = console.readString("Enter the director: ");
        String rating = console.readString("Enter the rating: ");
        String notes = console.readString("Enter notes: ");

        DVD dvdToAdd = new DVD(title, studio, releaseDate, directorName, rating, notes);

        daoLayer.addDVD(dvdToAdd);

        String doAgain = console.readString("To continue adding DVDs, type \"A\".  Otherwise, press any other letter");

        if (doAgain.equals("A")) {
            addDVD();
        }

    }

    private void removeDVD() {
        String title = console.readString("Enter the title of the DVD you wish to remove: ");
        DVD removedDVD = daoLayer.removeDVD(title);
        if (removedDVD == null) {
            console.print("This DVD was not found in the library.");
        } else {
            console.print(removedDVD.getTitle() + " was successfully removed from the library.");
        }

        String doAgain = console.readString("To remove another DVD, type \"A\".  Otherwise, press any other letter");

        if (doAgain.equals("A")) {
            removeDVD();
        }
    }
}
