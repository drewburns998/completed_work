<%-- 
    Document   : index
    Created on : Mar 28, 2016, 9:35:26 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <title>Tip Calculator</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" />
</head>
<body>
    <div>
        <h1 align="left">&nbsp;Tip Calculator</h1>
    </div>
    
    <form role="form" method="POST" action="TipCalculatorServlet">
        <div class="col-xs-12 col-md-2">
            <div class="form-group">                                
                <input type="number" placeholder="$ Total Bill" min=0 class="form-control" name="billAmount">
                <input type="number" placeholder="Tip Percent" min=0 class="form-control" name="tipPercent">                
            </div>        
            <button type="submit" class="btn btn-default">Calculate</button>
        </div>
    </form>


</body>
</html>