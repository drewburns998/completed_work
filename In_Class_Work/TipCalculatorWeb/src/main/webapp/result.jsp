<%-- 
    Document   : result
    Created on : Mar 28, 2016, 9:35:35 PM
    Author     : apprentice
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <style type="text/css">
            #tableid{
                width: auto;
            }
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tip Calculation</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" />
    </head>
    <body>
        <h1>Bill Summary</h1>
        <table id="tableid" class="table table-bordered">
            <tr><td>Subtotal</td><td>$<c:out value="${subTotal}" default="n/a" /></td></tr>
            <tr><td>Tip Percentage:</td><td><c:out value="${tipPercent}" default="n/a" />%</td> </tr> 
            <tr><td>Tip Amount:</td><td>$<c:out value="${tipAmount}" default="n/a" /></td> </tr>
            <tr><td>Grand Total:</td><td>$<c:out value="${grandTotal}" default="n/a" /></td> </tr>
        </table>

    </body>
</html>
