/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.classroster.dao;

import com.swcguild.classroster.dto.Student;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Scanner;

/**
 * This class is responsible for adding/deleting/updating/retrieving Student
 * objects contained in the roster. It is also responsible for loading the
 * roster from a file and writing the roster to a file.
 */
public class ClassRosterDAOImpl implements ClassRosterDAO {
    
    // Create a constant for the name of the roster file.  This helps ensure that
    // readability and consistancy - We need to read & write to/from the same file!
    public static final String ROSTER_FILE = "ClassRoster.txt";
    
    // Similarly, make the delimiter a constant - we want to break on the same places we write to!
    private static final String DELIMITER = "::";
    
    // HashMap to store all of our student objects
    // The String is the type of the ID (our unique element) to index our Student objects.
    private HashMap<String, Student> students = new HashMap<>();
    
     /**
     * Add the given Student to the roster and as associated with it's id
     */
    public void addStudent(Student student){
        // NOTE FOR APPRENTICES: In this method we simple add the give student
        // to the student map using the given studentId as the key.  
        students.put(student.getStudentId(), student);
    }
    
    /**
     * Returns a Collection of all students in the roster.
     *
     * @return Collection containing all the students in the roster
     */
    public Collection<Student> getAllStudents(){
        return students.values();
    }
    
    public Student getStudentById(String id){
        return students.get(id);
    }
    
    /**
     * Loads students into roster from ROSTER_FILE 
     * Each line in the file represents one student record. 
     * The format of each record is:
     * StudentId::First Name::Last Name::GradeLeve
     * 
     * @throws FileNotFoundException if ROSTER_FILE is not present 
     */
    public void loadRoster() throws FileNotFoundException {
        // Create Scanner for reading the file
        Scanner sc = new Scanner(new BufferedReader(new FileReader(ROSTER_FILE)));
        
        // recordLine holds the most recent line read from the file
        String recordLine;
        // 1234::Joe::Smith::12
        // then it will be broken into will be a String array the looks like this:
        // |1234|Joe|Smith| 12 |
        // ---------------------
        //  [0]  [1]  [2]   [3]
       
        String[] recordProperties;
        
        // Go through ROSTER_FILE line by line, decoding each line into a Student object
        // process while we have more lines in the file
        
        while(sc.hasNextLine()){
            recordLine = sc.nextLine();
            recordProperties = recordLine.split(DELIMITER);
            
            // If the split string doesn't have exactly 4 parts the record is malformed
            // skip it, and go onto the next line.
            if(recordProperties.length != 4)
                continue;
            
            // Create a new Student object and put it into the map of students
            Student studentFromFile = new Student();
            studentFromFile.setStudentId(recordProperties[0]);
            studentFromFile.setFirstName(recordProperties[1]);
            studentFromFile.setLastName(recordProperties[2]);
            studentFromFile.setGradeLevel(Integer.parseInt(recordProperties[3]));
            
            // Put the newly created student into the map using studentID as the key
            students.put(studentFromFile.getStudentId(),studentFromFile);
        }
        
        // close scanner
        sc.close();
    }
    
    /**
     * Writes all students in the roster out to a ROSTER_FILE.
     * @throws IOException if an error occurs writing to the file 
     */
    public void writeRoster() throws IOException {
        PrintWriter writer = new PrintWriter(new FileWriter(ROSTER_FILE));
        
        // Write out the Student objects to the roster file.
        // Retrieve all of the students from the HashMap
        Collection<Student> allStudentsInHashMap = students.values();
        
        // Since the collection is an unordered group, we will use an 
        // enhanced for loop instead of a normal for loop, since Collections
        // don't have a set index for each member of the collection!
        for(Student s : allStudentsInHashMap){
            // Each student object is temporarily stored in the variable named s
            // Access the information and write it out in order to the file, one per line
            writer.println(s.getStudentId() + DELIMITER +
                            s.getFirstName() + DELIMITER +
                            s.getLastName() + DELIMITER +
                            s.getGradeLevel());
        }
        
        writer.flush(); // force PrintWriter to write any waiting buffer to file
        writer.close(); // Clean up 
    }
    
    /**
     * Removes from the roster the student associated with the given id. 
     * Returns the Student object that is being removed or null 
     * if there is no student associated with the given id.
     */
    public Student removeStudent(String idOfStudent){
        Student removedStudent = students.remove(idOfStudent);
        return removedStudent;
    }
}
