/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.classroster.dao;

import com.swcguild.classroster.dto.Student;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

/**
 *
 * @author ahill
 */
public interface ClassRosterDAO {
    
    public void writeRoster() throws IOException;
    public void loadRoster() throws FileNotFoundException;
    public Student getStudentById(String id);
    public Collection<Student> getAllStudents();
    public Student removeStudent(String id);
    public void addStudent(Student student);
    
}
