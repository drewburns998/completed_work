/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.classroster.ops;

import com.swcguild.classroster.dao.ClassRosterDAO;
import com.swcguild.classroster.dao.ClassRosterDAOImpl;
import com.swcguild.classroster.dto.Student;
import com.swcguild.classroster.ui.ConsoleIO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author apprentice
 */
public class ClassRosterControllerImpl implements ClassRosterController {
    
    private ConsoleIO console = new ConsoleIO();
    private ClassRosterDAO daoLayer;// = new ClassRosterDAOImpl();
    
    @Override
    public void run() throws IOException{
        
        boolean keepRunning = true;
        int menuSelect = 0;
        
        try {
            daoLayer.loadRoster();
        } catch (FileNotFoundException ex) {
            keepRunning = false;
            console.print("Roster file not found, please contact system administrator.");
        }
        
        while(keepRunning){
            printMenu();
            menuSelect = console.readInt("Please select from the above menu choices: ", 1, 6);
            
            switch(menuSelect){
                
                case 1: // Add Student
                    addStudent();
                    break;
                    
                case 2: // Update Student
                    updateStudent();
                    break;
                
                case 3: // List All students
                    listAllStudents();
                    break;
                
                case 4: // Remove student
                    removeStudent();
                    break;
                    
                case 5: // Find Student by Last Name
                    findStudent();
                    break;
                    
                case 6: // Exit
                    keepRunning = false;
                    daoLayer.writeRoster();
                    break;
                
            }
            
            
        }
        
    }
    
    private void printMenu(){
        console.print("Main Menu");
        console.print("1. Enter New Student");
        console.print("2. Update Student");
        console.print("3. List All Students");
        console.print("4. Remove Student");
        console.print("5. Find Student By Name");
        console.print("6. Exit");
    }
    
    private void updateStudent(){
        
        String idOfStudent = console.readString("Please enter ID of student you wish to update.");
        Student studentToEdit = daoLayer.getStudentById(idOfStudent);
        
        if(studentToEdit == null){
            console.print("There is no student by that id in our roster.");
        } else{
            boolean studentUpdated = false;
            String updatedFirstName = console.readString("Enter new First Name for records"
                                                        + " or press enter to keep current value " 
                                                        + "(" + studentToEdit.getFirstName() + "):");
            
            if(!updatedFirstName.isEmpty()){
                studentToEdit.setFirstName(updatedFirstName);
                studentUpdated = true;
            }
            
            String updatedLastName = console.readString("Enter new Last Name for records"
                                                        + " or press enter to keep current value " 
                                                        + "(" + studentToEdit.getLastName() + "):");
            
            if(!updatedLastName.isEmpty()){
                studentToEdit.setLastName(updatedLastName);
                studentUpdated = true;
            }
            
            String updatedGradeLevel = console.readString("Enter new Grade Level for records"
                                                        + " or press enter to keep current value " 
                                                        + "(" + studentToEdit.getGradeLevel() + "):");
            
            if(!updatedGradeLevel.isEmpty()){
                studentToEdit.setGradeLevel(Integer.parseInt(updatedGradeLevel));
                studentUpdated = true;
            }
            
            if(studentUpdated){
                console.print("Student has been successfully updated with new information.");
            } else{
                console.print("Student has kept original information.");
            }
            
        }
        
        
        console.readString("Please hit enter to continue...");
        
    }
    
    private void listAllStudents(){
        
        Collection<Student> students = daoLayer.getAllStudents();
        for(Student stud: students){
            console.print(stud.getFirstName() + " " + stud.getLastName());
            console.print(stud.getStudentId() + ", " + stud.getGradeLevel());
        }
        
    }
    
    private void findStudent(){
        
        String lastNameToSearchFor = console.readString("Please enter the last name of the student you wish to search for: ");
        Collection<Student> allStudents = daoLayer.getAllStudents();
        ArrayList<Student> matchingStudents = new ArrayList<>();
        
        for( Student curStudent : allStudents){
            
            String curLastName = curStudent.getLastName();
            
            if(curLastName.equalsIgnoreCase(lastNameToSearchFor))
                matchingStudents.add(curStudent);
            
        }
        
        if(matchingStudents.isEmpty()){
            console.print("There were no students matching that last name in our roster.");
        } else{
            console.print("These are the students that matched your search request: ");
            for (int i = 0; i < matchingStudents.size(); i++) {
                Student curStudent = matchingStudents.get(i);
                console.print( curStudent.getStudentId() + " : " + curStudent.getFirstName() + " " + curStudent.getLastName() );
            }
        }
        
        console.readString("Please hit enter to continue...");
        
    }
    
    private void removeStudent(){
        String studentIdToRemove = console.readString("Please Enter Student ID of student record to delete: ");
        Student removedStudentFromDAO = daoLayer.removeStudent(studentIdToRemove);
        console.print(removedStudentFromDAO.getFirstName() + " " + removedStudentFromDAO.getLastName() 
                + " has been removed from the student roster.");
        console.readString("Please press enter to continue...");
    }
    
    private void addStudent(){
        String studentId = console.readString("Please enter Student ID: ");
        String firstName = console.readString("Please enter Student's First Name: ");
        String lastName = console.readString("Please enter Student's Last Name: ");
        int gradeLevel = console.readInt("Please enter grade level: ", 1, 12);
        
        Student newStudent = new Student(studentId, firstName, lastName, gradeLevel);
        daoLayer.addStudent(newStudent);
        console.readString("Student successfully added. Please hit enter to continue.");
        
    }
    
}
