<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Dictionary</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>

                </ul>    
            </div>
            <h2>Home Page</h2>

            <!-- Placed at the end of the document so the pages load faster -->
            <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

            <div class="col-md-offset-1 col-md-5">
                <form class="form-horizontal" role="form" 
                      action="displayWords" method="POST">
                    <div class="form-group">
                        <label for="add-term" class="col-md-4 control-label">Word Added</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="search-first-name" name="firstName" placeholder="Word" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-definition" class="col-md-4 control-label">Definition</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="search-last-name" name="lastName" placeholder="Definition" />
                        </div>
                    </div>
                    <div class="form-group">                    
                        <div class="col-md-offset-4 col-md-8">
                            <button type="submit" id="submit-button" class=""btn btb-default">Submit Word</button>
                        </div>
                    </div>

                </form>
            </div>
            <div class="col-md-offset-1 col-md-4">
                <h1>Insert Results Here</h1>

            </div>

        </div>
    </body>
</html>

