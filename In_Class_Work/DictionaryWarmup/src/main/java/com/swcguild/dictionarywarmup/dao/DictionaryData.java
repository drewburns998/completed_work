/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dictionarywarmup.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class DictionaryData {
    HashMap<String, String> dictionaryList = new HashMap<>();
    
    public DictionaryData(){
        dictionaryList.put("dog", "type of animal");
        dictionaryList.put("bird", "type of animal");
        dictionaryList.put("andrew", "type of human");        
    }
    
    public List<String> getAllWords() {
        Collection<String> c = dictionaryList.keySet();
        return new ArrayList(c);
    }
    
}
