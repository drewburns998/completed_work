/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.unitconverterweb;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */

@Controller
public class unitConverterController {
    
    @RequestMapping(value = "/convertUnits", method = RequestMethod.POST)
    public String convertValue(HttpServletRequest req, Model model) {
        String celsInputString = req.getParameter("celsInput");
        String fahrInputString = req.getParameter("fahrInput");
        String poundsInputString = req.getParameter("poundsInput");
        String kiloInputString = req.getParameter("kiloInput");
        
        Double sanitizedInput = sanitizeUnit(celsInputString);
        if (sanitizedInput >= 0) {
            model.addAttribute("convertFrom", "Celsius");
            model.addAttribute("convertFromValue", sanitizedInput);
            model.addAttribute("convertResult", sanitizedInput*(1.8)+32);
        }
        sanitizedInput = sanitizeUnit(fahrInputString);
        if (sanitizedInput >= 0) {
            model.addAttribute("convertFrom", "Fahrenheit");
            model.addAttribute("convertFromValue", sanitizedInput);
            model.addAttribute("convertResult", (sanitizedInput-32)/1.8);
        }
        sanitizedInput = sanitizeUnit(poundsInputString);
        if (sanitizedInput >= 0) {
            model.addAttribute("convertFrom", "Pounds");
            model.addAttribute("convertFromValue", sanitizedInput);
            model.addAttribute("convertResult", sanitizedInput*(0.453592));
        }
        if (sanitizedInput >= 0) {
            model.addAttribute("convertFrom", "Pounds");
            model.addAttribute("convertFromValue", sanitizedInput);
            model.addAttribute("convertResult", sanitizedInput*(2.20462));
        }
        
        return "results";
    }
    
    private double sanitizeUnit(String unitToConvertParam) {
        double unitToConvertInt;
        try {
            unitToConvertInt = Double.parseDouble(unitToConvertParam);
        } catch (NumberFormatException ex) {
            unitToConvertInt = -1;
        }
        return unitToConvertInt;
    }
    
}
