<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Unit Converter Application</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
        <script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".radio-inline3").hide();
                $(".radio-inline2").hide();
                $(".celsInputDiv").hide();
                $(".fahrInputDiv").hide();
                $(".poundsInputDiv").hide();
                $(".kiloInputDiv").hide();
                $(".convButton").hide();
                $('input[type="radio"]').click(function () {
                    if ($(this).attr("value") === "temp") {
                        $(".radio-inline2").show();
                        $(".radio-inline3").hide();
                    }
                    if ($(this).attr("value") === "mass") {
                        $(".radio-inline3").show();
                        $(".radio-inline2").hide();
                    }
                    if ($(this).attr("value") === "celsius") {
                        $(".celsInputDiv").show();
                        $(".convButton").show();
                        $(".fahrInputDiv").hide();
                        $(".poundsInputDiv").hide();
                        $(".kiloInputDiv").hide();
                    }
                    if ($(this).attr("value") === "fahrenheit") {
                        $(".fahrInputDiv").show();
                        $(".convButton").show();
                        $(".celsInputDiv").hide();
                        $(".poundsInputDiv").hide();
                        $(".kiloInputDiv").hide();
                    }
                    if ($(this).attr("value") === "pounds") {
                        $(".poundsInputDiv").show();
                        $(".convButton").show();
                        $(".celsInputDiv").hide();
                        $(".fahrInputDiv").hide();
                        $(".kiloInputDiv").hide();
                    }
                    if ($(this).attr("value") === "kilograms") {
                        $(".kiloInputDiv").show();
                        $(".convButton").show();
                        $(".celsInputDiv").hide();
                        $(".fahrInputDiv").hide();
                        $(".poundsInputDiv").hide();
                    }

                });
            });
        </script>


    </head>
    <body>
        <div class="container">
            <h1>Unit Converter Application</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>                    
                </ul>    
            </div>
            <h2>Start Your Conversion</h2>

            <p>Choose the conversion type below</p>

            <div>
                <label class="radio-inline">
                    <input type="radio" name="optradio" value="temp">Temperature
                </label>
                <label class="radio-inline">
                    <input type="radio" name="optradio" value="mass">Mass
                </label>
            </div>
            </br>
            <div>
                <div>
                    <label class="radio-inline2">
                        <input type="radio" name="optradio2" value = "celsius" id="temp">&nbsp;Celsius -> Fahrenheit                        
                    </label>
                </div>
                <div>
                    <label class="radio-inline2">
                        <input type="radio" name="optradio2" value = "fahrenheit" id="mass">&nbsp;Fahrenheit -> Celsius                        
                    </label>
                </div>

                <div>                        
                    <label class="radio-inline3">
                        <input type="radio" name="optradio3" value = "pounds" id="temp">&nbsp;Pounds (lbs) -> Kilograms (Kg)                        
                    </label>
                </div>
                <div>
                    <label class="radio-inline3">
                        <input type="radio" name="optradio3" value = "kilograms" id="mass">&nbsp;Kilograms (Kg) -> Pounds (lbs)                        
                    </label>
                </div>
            </div>
            </hr>
            <form action="/convertUnits" method="POST">
                <div class="celsInputDiv">                
                    </br>Enter Celsius Value:
                    <input type="text" name="celsInput" id="baseCels">                
                </div>
                <div class="fahrInputDiv">                
                    </br>Enter Fahrenheit Value:
                    <input type="text" name="fahrInput" id="baseFahr">                
                </div>
                <div class="poundsInputDiv">                
                    </br>Enter Pounds Value:
                    <input type="text" name="poundsInput" id="basePounds">                
                </div>
                <div class="kiloInputDiv" >                
                    </br>Enter Kilogram Value:
                    <input type="text" name="kiloInput" id="baseKilo">                
                </div>
                <div class="convButton">
                    <button type="submit" id="conv-button" class=""btn btb-default">Convert</button>
                </div>
            </form>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

