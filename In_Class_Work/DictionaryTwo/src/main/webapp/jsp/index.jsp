<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>My Personal Dictionary</h1>
            <hr/>
            
            <div class="col-md-offset-1 col-md-5">
                <h1>Add New Word</h1>
                <form class="form-horizontal" role="form" 
                      action="" method="POST">
                    <div class="form-group">
                        <label for="add-term" class="col-md-4 control-label">Word Added</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="search-first-name" name="word" placeholder="Word" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-definition" class="col-md-4 control-label">Definition</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="search-last-name" name="definition" placeholder="Definition" />
                        </div>
                    </div>
                    <div class="form-group">                    
                        <div class="col-md-offset-4 col-md-8">
                            <button type="submit" id="submit-button" class=""btn btb-default">Submit Word</button>
                        </div>
                    </div>

                </form>
            </div>
            <div class="col-md-offset-1 col-md-4">
                <h1>Dictionary Results</h1>
                <ul>
                    <c:forEach items="${words}" var="wordObj">
                        <li><b>Word:</b> ${wordObj.word}</li>
                        <li><b>Definition:</b><i> ${wordObj.definition}</i></li>
                        <br/>
                    </c:forEach>
                </ul>
            </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

