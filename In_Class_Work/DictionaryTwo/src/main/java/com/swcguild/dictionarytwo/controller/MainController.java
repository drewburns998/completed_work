/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dictionarytwo.controller;

import com.swcguild.dictionarytwo.dao.DictionaryDao;
import com.swcguild.dictionarytwo.dao.DictionaryDaoImpl;
import com.swcguild.dictionarytwo.model.Word;
import java.util.List;
import java.util.function.Predicate;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
public class MainController {

    DictionaryDao dao = new DictionaryDaoImpl();

    @RequestMapping(value = "/", method=RequestMethod.GET)
    public String displayMainPage(Model model) {
        model.addAttribute("words", dao.getAllWords());
        return "index";
    }

    @RequestMapping(value = "/", method=RequestMethod.POST)
    public String addNewWord(HttpServletRequest request, Model model) {

        String wordParam = request.getParameter("word");
        String defParam = request.getParameter("definition");

        if (wordParam != null && !wordParam.equalsIgnoreCase("")
                && defParam != null && !defParam.equalsIgnoreCase("")) {
            Word newWord = new Word(wordParam, defParam);

            dao.addWord(newWord);
        }

        model.addAttribute("words", dao.getAllWords());
        return "index";
    }
    
    @RequestMapping(value="/word/{wordParam}", method=RequestMethod.GET)
    @ResponseBody
    public Word getWord(@PathVariable("wordParam")String wordToGet){
        return new Word("Llama", "The fuzziest");
    }
    
    @RequestMapping(value="/list/{wordParam}", method=RequestMethod.GET)
    @ResponseBody
    public List getList(@PathVariable("wordParam")String wordToGet){
        
        List<Word> newList = dao.getAllWords();
        return newList;
    }

}
