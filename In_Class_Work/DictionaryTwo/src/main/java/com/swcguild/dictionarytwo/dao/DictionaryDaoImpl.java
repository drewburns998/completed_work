/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dictionarytwo.dao;

import com.swcguild.dictionarytwo.model.Word;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class DictionaryDaoImpl implements DictionaryDao {
    
    ArrayList<Word> dictionary = new ArrayList<>();
    
    @Override
    public Word addWord(Word word) {
        dictionary.add(word);
        return word;
    }

    @Override
    public List<Word> getAllWords() {
        return dictionary;
    }
    
}
