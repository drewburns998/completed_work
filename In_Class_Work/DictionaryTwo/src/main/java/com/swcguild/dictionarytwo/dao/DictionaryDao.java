/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dictionarytwo.dao;

import com.swcguild.dictionarytwo.model.Word;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface DictionaryDao {
    
    public Word addWord(Word word);
    public List<Word> getAllWords();
}
