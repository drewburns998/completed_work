/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.luckysevensweb;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "LuckySevensServlet", urlPatterns = {"/LuckySevensServlet"})
public class LuckySevensServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String userBetString = request.getParameter("userBet");
        int maxRound = 0;
        double currentMoney;
        int totalRollCounter = 0;
        int diceSum;
        double maxMoney;
        Random random1 = new Random();

        if (userBetString == null) {
            request.setAttribute("badInput", true);
        } else {
            try {
                double userBet = Double.parseDouble(userBetString);

                if (userBet <= 0) {
                    request.setAttribute("badInput", true);
                } else {
                    currentMoney = userBet;
                    maxMoney = userBet;
                    while (currentMoney > 0) {
                        int dice1 = random1.nextInt(5) + 1;
                        int dice2 = random1.nextInt(5) + 1;
                        totalRollCounter++;
                        diceSum = dice1 + dice2;

                        if (diceSum == 7) {
                            currentMoney += 4;
                        } else {
                            currentMoney -= 1;
                        }

                        if (currentMoney > maxMoney) {
                            maxMoney = currentMoney;
                            maxRound = totalRollCounter;
                        }

                    }
                    request.setAttribute("maxRound", maxRound);
                    request.setAttribute("maxMoney", maxMoney);
                    request.setAttribute("totalRounds", totalRollCounter);

                }

            } catch (NumberFormatException ex) {
                request.setAttribute("badInput", true);
            }
        }
        request.getRequestDispatcher("result.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
