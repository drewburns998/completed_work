<%-- 
    Document   : result
    Created on : Mar 29, 2016, 9:23:01 AM
    Author     : apprentice
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <style type="text/css">
            #tableid{
                width: auto;
            }
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lucky Sevens Results</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" />
    </head>
    <body>
        <h1>Game Summary</h1>
        <table id="tableid" class="table table-bordered">
            <tr><td>Max Money:</td><td>$<c:out value="${maxMoney}" default="n/a" /></td></tr>
            <tr><td>Max Round:</td><td><c:out value="${maxRound}" default="n/a" /></td> </tr> 
            <tr><td>Total Rounds:</td><td><c:out value="${totalRounds}" default="n/a" /></td> </tr>
            
        </table>
    </body>
</html>
