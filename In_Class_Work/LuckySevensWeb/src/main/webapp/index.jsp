<%-- 
    Document   : index
    Created on : Mar 29, 2016, 9:22:49 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <title>Lucky Sevens</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" />
</head>
<body>
    <div class="container">
        <div>
            <h1 align="left">&nbsp;Lucky Sevens</h1>
        </div>

        <div class="col-xs-12 col-md-2">
            <form role="form" method="POST" action="LuckySevensServlet">


                <input type="number" placeholder="$ Starting Bet" min=0 class="form-control" name="userBet">                                                
                <button type="submit" class="btn btn-default">Calculate</button>


            </form>
        </div>
    </div>

</body>
</html>
