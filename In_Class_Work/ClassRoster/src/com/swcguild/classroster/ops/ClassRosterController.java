/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.classroster.ops;

import com.swcguild.classroster.dao.ClassRosterDAO;
import com.swcguild.classroster.dto.Student;
import com.swcguild.classroster.ui.ConsoleIO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

/**
 *
 * @author apprentice
 */
public class ClassRosterController {

    private ConsoleIO console = new ConsoleIO();
    private ClassRosterDAO daoLayer = new ClassRosterDAO();

    public void run() throws IOException {
        boolean keepRunning = true;
        int menuSelect = 0;

        try {
            daoLayer.loadRoster();
        } catch (FileNotFoundException ex) {
            keepRunning = false;
            console.print("Roster file not found");
        }

        while (keepRunning) {
            printMenu();
            menuSelect = console.readInt("Please select from the above menu choices", 1, 1);

            switch (menuSelect) {
                case 1:
                    addStudent();
                    break;
                case 2:
                    //updateStudent();
                    break;
                case 3:
                    listAllStudents();
                    break;
                case 4:
                    //removeStudent();
                    break;
                case 5:
                    //findStudentByName();
                    break;
                case 6: //exit
                    keepRunning = false;
                    daoLayer.writeRoster();
                    break;
                default:
                    break;
            }
        }

    }

    private void printMenu() {
        console.print("Main Menu");
        console.print("1. Enter New Student");
        console.print("2. Update Student");
        console.print("3. List All Students");
        console.print("4. Remove Student");
        console.print("5. Find Student By Name");
        console.print("6. Exit");

    }

    private void listAllStudents() {

        Collection<Student> students = daoLayer.getAllStudents();

        for (Student stud : students) {
            console.print(stud.getFirstName() + " " + stud.getLastName());
            console.print(stud.getStudentId() + " " + stud.getGradeLevel());
        }
    }
    
    private void removeStudent(){
        String studentIDToRemove = console.readString("Enter ID of Student Record to delete: ");
        Student removedStudentFromDAO = daoLayer.removeStudent(studentIDToRemove);
        
        console.print(removedStudentFromDAO.getFirstName() + " " + removedStudentFromDAO.getLastName()
            + " has been removed from the student roster.");
        console.readString("Please press enter to continue...");
        
    }

    private void addStudent() {
        String studentId = console.readString("Please enter Student ID:");
        String firstName = console.readString("Please enter the first name:");
        String lastName = console.readString("Please enter the last name:");
        int gradeLevel = console.readInt("Please enter grade level:", 1, 12);

        Student newStudent = new Student(studentId, firstName, lastName, gradeLevel);
        daoLayer.addStudent(newStudent);
        console.readString("Student Successfully added.  Please hit enter to continue");

    }
}
