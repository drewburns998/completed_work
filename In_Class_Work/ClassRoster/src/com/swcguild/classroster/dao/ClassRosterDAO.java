/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.classroster.dao;

import com.swcguild.classroster.dto.Student;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ClassRosterDAO {

    private HashMap<String, Student> students = new HashMap<>();
    public static final String ROSTER_FILE = "ClassRoster.txt";
    private static final String DELIMITER = "::";

    public void addStudent(Student student) {
        students.put(student.getStudentId(), student);
    }

    public Collection<Student> getAllStudents() {
        return students.values();
    }

    public void loadRoster() throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader(ROSTER_FILE)));

        while (sc.hasNextLine()) {
            String recordLine = sc.nextLine();
            String[] recordProperties = recordLine.split(DELIMITER);

            if (recordProperties.length != 4) {
                continue;
            }

            Student studentFromFile = new Student();
            studentFromFile.setStudentId(recordProperties[0]);
            studentFromFile.setFirstName(recordProperties[1]);
            studentFromFile.setLastName(recordProperties[2]);
            studentFromFile.setGradeLevel(Integer.parseInt(recordProperties[3]));

            students.put(studentFromFile.getStudentId(), studentFromFile);
        }

    }

    public void writeRoster() throws IOException {

        PrintWriter writer = new PrintWriter(new FileWriter(ROSTER_FILE));

        Collection<Student> allStudentsInHashMap = students.values();

        for (Student s : allStudentsInHashMap) {
            writer.println(s.getStudentId() + DELIMITER
                    + s.getFirstName() + DELIMITER
                    + s.getLastName() + DELIMITER + s.getGradeLevel());

        }
        writer.flush();
        writer.close();
    }
    
    public Student removeStudent(String idOfStudent){
        return students.remove(idOfStudent);
    }
}
