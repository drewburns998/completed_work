/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.classroster;

import com.swcguild.classroster.ops.ClassRosterController;
import java.io.IOException;

/**
 *
 * @author apprentice
 */
public class ClassRosterApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        ClassRosterController controller = new ClassRosterController();
        controller.run();
    }
    
}
