/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windowsmastery;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WindowsMastery {

    //Scanner sc = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    Scanner globalScanner = new Scanner(System.in);

    public static double takeDoubleInput(Scanner bob) {
        String userInput = bob.nextLine();
        return Double.parseDouble(userInput);
    }
    

    public static void main(String[] args) {

        double height = 0; //expected in feet
        double width = 0; // expected in feet
        double perimeter = 0;
        double area = 0;
        double totalCost = 0;
        double glassPrice = 0; //per sq ft
        double trimPrice = 0; //per ft

        Scanner sc = new Scanner(System.in);

        String userInput = "";

        System.out.println("Please input the width of your window in ft: ");
        width = takeDoubleInput(sc);

        System.out.println("Please input the height of your window in ft: ");

        height = takeDoubleInput(sc);

        System.out.println("Please input the cost of glass per sqft: ");
        userInput = sc.nextLine();
        glassPrice = Double.parseDouble(userInput);

        System.out.println("Please input the cost of trim per ft: ");
        userInput = sc.nextLine();
        trimPrice = Double.parseDouble(userInput);

        //calculate the area of the window
        area = height * width;

        perimeter = 2 * (height + width);

        System.out.println("The height of the window is: " + height + "ft");
        System.out.println("The width of the window is: " + width + "ft");

        System.out.println("The area of the window is: " + area + "sqft");
        System.out.println("The perimeter of the window is: " + perimeter + "ft");

        totalCost = perimeter * trimPrice + area * glassPrice;

        System.out.println("The total cost is $" + totalCost);
        System.out.println("Total cost is based on glass price of $" + glassPrice
                + "/sqft and trim price of $" + trimPrice + "/ft");

    }

}
