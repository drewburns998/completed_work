/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oo.fileio;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class MTGApp {

    public static void main(String[] args) throws IOException {

        ArrayList<MagicCard> deck; 
                
        deck = decodeDeck();        
        if(deck.size() < 1){
            deck = generateCards();
        } else{
            for(MagicCard bob : deck){
                System.out.println(bob.getType() + " " + bob.getCost());
            }
        }
        
        encodeDeck(deck);
        

    }

    private static ArrayList<MagicCard> decodeDeck() throws FileNotFoundException {
        ArrayList<MagicCard> deck = new ArrayList<>();

        Scanner sc = new Scanner(new BufferedReader(new FileReader("myMagicDeck.txt")));

        while (sc.hasNextLine()) {
            MagicCard card = new MagicCard();
            String cardRecord = sc.nextLine();
            String[] tokens = cardRecord.split("::");

            if (tokens.length != 3) {
                continue;
            }

            card.setType(tokens[0]);
            int cost;

            cost = Integer.parseInt(tokens[2]);
            card.setCost(cost);
            
            String color = tokens[1];
            
            if (color.equals("RED")) {
                card.setColor(Color.red);
            } else if (color.equals("BLACK")) {
                card.setColor(Color.black);
            } else if (color.equals("BLUE")) {
                card.setColor(Color.blue);
            } else if (color.equals("WHITE")){
                card.setColor(Color.white);
            } else {
                continue;
            }
            deck.add(card);
        }
        return deck;
    }

    private static void encodeDeck(ArrayList<MagicCard> deck) throws IOException {
        //FileWriter fOut = new FileWriter("myMagicDeck");
        PrintWriter pOut = new PrintWriter(new FileWriter("myMagicDeck.txt"));

        for (MagicCard card : deck) {
            String type = card.getType();
            Color color = card.getColor();
            int cost = card.getCost();

            String colorString = "COLORLESS";

            if (color.equals(Color.red)) {
                colorString = "RED";
            } else if (color.equals(Color.black)) {
                colorString = "BLACK";
            } else if (color.equals(Color.blue)) {
                colorString = "BLUE";
            } else {
                colorString = "WHITE";
            }
            pOut.println(type + "::" + colorString + "::" + cost);
        }
        pOut.flush();
        pOut.close();
    }

    private static ArrayList<MagicCard> generateCards() {
        ArrayList<MagicCard> deck = new ArrayList<>();
        MagicCard card;
        Random r = new Random();

        for (int i = 0; i < 10; i++) {
            card = new MagicCard();
            switch (r.nextInt(4)) {
                case 1:
                    card.setType("Creature");
                    card.setColor(Color.red);
                    card.setCost(1);
                    break;
                case 2:
                    card.setType("Instant");
                    card.setColor(Color.blue);
                    card.setCost(2);
                    break;
                case 3:
                    card.setType("Enchantment");
                    card.setColor(Color.white);
                    card.setCost(7);
                    break;
                case 0:
                    card.setType("Mana");
                    card.setColor(Color.black);
                    card.setCost(0);
                    break;
            }
            deck.add(card);
        }
        return deck;
    }
}
