/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classmaterialdemo;

/**
 *
 * @author apprentice
 */
public class Loops {
    public static void main(String[] args) {
        
        int a = 0;
        while(a >10){
            System.out.println(" Will i execute");
            a = a+1;
        }
        do{
            System.out.println("What about this one");
            a = a +1;            
        } while(a > 10);
        
        while(a<10){
            System.out.println("A is " + a);
            a= a+1;
        }
        
        for (int i = 0; i < 10; i++) {
            a += 2;
            System.out.println("A is " + a);
        }
    }
    
}
