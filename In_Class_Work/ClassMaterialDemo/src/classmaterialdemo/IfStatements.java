/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classmaterialdemo;

/**
 *
 * @author apprentice
 */
public class IfStatements {
    public static void main(String[] args) {
        if(true){
            System.out.println("This will always happen");
        }
        
        if(false){
            System.out.println("This will never happen");
        } else {
            System.out.println("But this will always happen");
        }
        
        int a = 6;
        if(a ==6){
            System.out.println("A is 6!");
        }
        if(a != 5){
            System.out.println("A is not 5");
        }
    }
}
