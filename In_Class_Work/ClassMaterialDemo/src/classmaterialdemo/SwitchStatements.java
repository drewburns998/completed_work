/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classmaterialdemo;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class SwitchStatements {
    public static void main(String[] args) {
        
        Random r = new Random();
        int input = r.nextInt(5);
        
        switch(input){
            case 1:
                System.out.println("The number was 1");
                break;            
            case 2:
                System.out.println("The number was 2");
            case 3:
                System.out.println("The number was 3");
            default:
                System.out.println("I don't like that number");
        }
    }
}
