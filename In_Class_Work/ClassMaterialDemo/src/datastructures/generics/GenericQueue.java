/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.generics;

import datastructures.stack.LinkedList;
import datastructures.stack.LinkedListNodeImpl;
import java.util.Iterator;

/**
 *
 * @author apprentice
 * @param <E>
 */
public class GenericQueue<E> implements Queue<E> {
    LinkedList items = new LinkedListNodeImpl();
    
    @Override
    public void enqueue(E item) {
        items.append(item);
    }

    @Override
    public E dequeue() {
        Object item = items.remove(0);
        
        if(item != null){
            return (E)item;
        }
        
        return null;
    }

    @Override
    public E peek() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int size() {
        return items.size();
    }

    @Override
    public Boolean isEmpty() {
        return items.isEmpty();
    }

    @Override
    public String getAuthorName() {
        return "Andrew";
    }

    @Override
    public Iterator<E> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
