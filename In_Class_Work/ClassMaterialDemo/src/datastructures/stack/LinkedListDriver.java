/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.stack;

/**
 *
 * @author apprentice
 */
public class LinkedListDriver {
    public static void main(String[] args) {
        
        LinkedList list = new LinkedListNodeImpl();
        
        list.append("Llama");
        list.append("Water Buffalo");
        list.append(4);        
        list.append(4.9);
        list.append(true);
        
        LinkedList integerList = new LinkedListNodeImpl();
        integerList.append(1);
        integerList.append(34);
        integerList.append(4);        
        integerList.append(23);
        integerList.append(7);
        
        integerList.append("Llama");
        
        int sum = 0;
        for (int i = 0; i < integerList.size(); i++) {
            Object integerToCast = integerList.get(i);
            Integer castInteger = (Integer) integerToCast;
            sum = sum + castInteger;
        }
        
        System.out.println("Total is " + sum);
        
        
    }
}
