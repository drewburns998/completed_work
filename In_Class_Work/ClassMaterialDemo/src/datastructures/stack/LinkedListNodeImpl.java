/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.stack;

import datastructures.stack.LinkedList;

/**
 *
 * @author apprentice
 */
public class LinkedListNodeImpl implements LinkedList {

    private int numItems = 0;
    private Node first;

    @Override
    public int size() {
        return numItems;
    }

    @Override
    public boolean isEmpty() {
        return numItems <= 0;
    }

    @Override
    public void append(Object item) {
        Node aNode = new Node();
        aNode.item = item;

        Node temp = first;
        if (temp == null) {
            first = aNode;
        } else {
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = aNode;
        }

        numItems++;
    }

    @Override
    public void prepend(Object item) {
        Node tempNode = new Node();
        tempNode.item = item;
        tempNode.next = this.first;
        this.first = tempNode;
        numItems++;
    }

    @Override
    public void insert(int index, Object item) {
        if (index >= numItems || index < 0) {
            return;
        }

        if (index == 0) {
            this.prepend(item);
        } else {
            int count = 0;
            Node temp = first;

            while (count + 1 < index) {
                temp = temp.next;
                count++;
            }

            Node aNode = new Node();
            aNode.item = item;
            aNode.next = temp.next;
            temp.next = aNode;
            numItems++;
        }

    }

    @Override
    public Object remove(int index) {

        if (index >= numItems || index < 0) {
            return null;
        }

        Node nodeToRemove;
        if (index == 0) {
            nodeToRemove = first;
            first = first.next;
        } else {

            Node nodeBeforeRemoved = this.getNode(index - 1);
            nodeToRemove = nodeBeforeRemoved.next;
            nodeBeforeRemoved.next = nodeToRemove.next;
        }

        numItems--;
        return nodeToRemove.item;

    }

    private Node getNode(int index) {
        if (index >= numItems || index < 0) {
            return null;
        }

        int count = 0;
        Node kid = first;

        while (count < index) {
            kid = kid.next;
            count++;
        }

        return kid;
    }

    @Override
    public Object get(int index) {

        if (index >= numItems || index < 0) {
            return null;
        }

        int count = 0;
        Node kid = first;

        while (count < index) {
            kid = kid.next;
            count++;
        }

        return kid.item;
    }

    private class Node {

        Object item;
        Node next;

    }
}
