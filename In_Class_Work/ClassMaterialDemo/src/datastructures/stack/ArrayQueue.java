/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.stack;

import java.util.Iterator;

/**
 *
 * @author apprentice
 */
public class ArrayQueue implements Queue {

    private Object[] items;
    private int numItems;
    private int head = -1;
    private int tail = -1;
    private static final int DEFAULT_INITIAL_SIZE = 4;

    public ArrayQueue() {
        this.items = new Object[DEFAULT_INITIAL_SIZE];
        this.numItems = 0;
    }

    @Override
    public void enqueue(Object item) {
        if (numItems >= items.length) {
            resize(items.length * 2);
        }

        if (numItems == 0) {
            this.head = 0;
            this.tail = 0;
        } else if (tail + 1 == items.length) {
            this.tail = 0;
        } else {
            tail++;
        }

        items[tail] = item;
        numItems++;

    }

    @Override
    public Object dequeue() {
        Object item = null;

        if (numItems != 0) {
            if (numItems < items.length / 4) {
                resize(items.length / 2);
            }
            if (head + 1 > items.length) {
                item = items[head];
                this.head = 0;

            }

            item = items[head];
            head++;

            numItems--;
        }

        return item;
    }

    private void resize(int newSize) {
        Object[] temp = new Object[newSize];

        if (head > tail) {
            int j = 0;
            for (int i = head; i < items.length; i++) {
                temp[j] = items[i];
                j++;
            }
            for (int i = 0; i < tail; i++) {
                temp[j] = items[i];
                j++;
            }
        } else {
            int j = 0;
            for (int i = head; i < head + numItems; i++) {
                temp[j] = items[i];
                j++;
            }
        }

        head = 0;
        tail = items.length - 1;
        items = temp;

    }

    @Override
    public boolean isEmpty() {
        return numItems == 0;
    }

    @Override
    public int size() {
        return numItems;
    }

    @Override
    public String toString() {
        String queue = "]";
        for (int i = 0; i < items.length; i++) {
            queue = "|" + queue;
            if (i == head) {
                queue = "h" + queue;
            }

            if (items[i] == null) {
                queue = "  " + queue;
            } else {
                queue = items[i].toString() + queue;
            }

            if (i == tail) {
                queue = "t" + queue;
            }
        }
        queue = "[" + queue.replace("|]", "]");

        return queue;
    }

    @Override
    public Iterator iterator() {
        return new QueueIterator();
    }
    
    
    private class QueueIterator implements Iterator {
        
        int index;
        int visited;
        
        public QueueIterator(){
            index = head;
            visited = 0;
        }
        
        @Override
        public boolean hasNext() {
            return visited < numItems;
        }

        @Override
        public Object next() {
            Object item;
            
            if(index >= items.length){
                index = 0;
            }
            
            item = items[index];
            index++;
            visited++;
            
            return item;
        }
    }
}
