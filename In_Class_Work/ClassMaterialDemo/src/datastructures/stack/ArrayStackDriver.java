/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.stack;

import java.util.Iterator;

/**
 *
 * @author apprentice
 */
public class ArrayStackDriver {
    public static void main(String[] args) {
        
        ArrayStack myStack = new ArrayStack();
        myStack.push(1);
        myStack.push(2);
        myStack.push(3);
        myStack.push(4);
                
                
        myStack.push(5);
        myStack.push(6);
        myStack.push(7);
        myStack.push(8);
        
        
        
        
        System.out.println(myStack.pop());
        System.out.println(myStack.pop());
        System.out.println(myStack.pop());
        System.out.println(myStack.pop());
        System.out.println(myStack.pop());
        System.out.println(myStack.pop());
        
        myStack.push(10);
        myStack.push(11);
        myStack.push(12);
        myStack.push(13);
        myStack.push(14);
        
        Iterator dandelionOne = myStack.iterator();
        
        while(dandelionOne.hasNext()){
            System.out.println(dandelionOne.next());
        }
        
        for(Object x : myStack){
            System.out.println(x);
        }
        
        
    }
    
}
