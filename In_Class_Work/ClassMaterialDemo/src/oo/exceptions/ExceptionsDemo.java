/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oo.exceptions;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class ExceptionsDemo {
    public static void main(String[] args) throws Exception {
        
        try {
            FileReader fReader = new FileReader("file.txt");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExceptionsDemo.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Something went wrong - talk to your admin.");
        }
        confusedMethod(null);
    }
    
    static public void confusedMethod(String name)throws Exception{
        if(name == null){
            throw new Exception();
        }
        System.out.println(name.concat("?!?!?!"));
    }
}
