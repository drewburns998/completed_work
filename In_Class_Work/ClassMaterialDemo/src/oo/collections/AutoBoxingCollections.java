/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oo.collections;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class AutoBoxingCollections {
    public static void main(String[] args) {
        
        ArrayList<Integer> integerList = new ArrayList<>();
        Integer three = new Integer(3);
        integerList.add(three);
        
        integerList.add(3);
        
        
        Integer x = integerList.get(1);
        
        integerList.remove(0);
        
        
    }
}
