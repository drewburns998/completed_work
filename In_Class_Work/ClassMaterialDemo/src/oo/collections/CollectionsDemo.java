/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oo.collections;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author apprentice
 */
public class CollectionsDemo {
    public static void main(String[] args) {
        
        ArrayList<String>  bob = new ArrayList<>();
        
        // Ask bob how big he is?
        
        System.out.println("Bob is this big " + bob.size());
        
        bob.add("Llama");
        
        System.out.println("Bob is this big " + bob.size());
        
        bob.add("Hey There");
        bob.add("Marmot");
        bob.add("Pikachu");
        bob.add("Chocolate chip cookie");
        bob.add("North East Main Street");
        
        System.out.println("Bob has grown to " + bob.size());
        
        System.out.println("The first thing I put in bob is " + bob.get(0));
        
        bob.remove(3);
        
        System.out.println("Bob has shrunk");
        
        for(String s : bob){
            System.out.println(s);
        }
        
        Iterator<String> dandelion = bob.iterator();
        
        while(dandelion.hasNext()){
            String s = dandelion.next();
            System.out.println(s);
        }
        
    }
    
}
