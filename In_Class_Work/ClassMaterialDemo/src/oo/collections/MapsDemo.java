/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oo.collections;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class MapsDemo {

    public static void main(String[] args) {

        HashMap< String, Double> favoriteNumbers = new HashMap<>();

        favoriteNumbers.put("Rene", 3.346);
        favoriteNumbers.put("Patrick", 3.1415926);
        favoriteNumbers.put("Solomon", 0.0);
        favoriteNumbers.put("Winona", 42.0);
        favoriteNumbers.put("Carrie", 18.0);
        favoriteNumbers.put("Andrew", 42.0);
        favoriteNumbers.put("Austyn", 13.0);

        System.out.println("Rene's favorite number is: " + favoriteNumbers.get("Rene"));
        System.out.println("Austyn's favorite number is " + favoriteNumbers.get("Austyn"));

        System.out.println("There are " + favoriteNumbers.size() + " things store in this Map.");
        favoriteNumbers.put("Austyn", 2.0);

        System.out.println("There are " + favoriteNumbers.size() + " things store in this Map.");

        Set<String> keys = favoriteNumbers.keySet();

        for (String name : keys) {
            System.out.println(name + ", ");
        }

        Collection<Double> numbers = favoriteNumbers.values();

        for (Double num : numbers) {
            System.out.println(num + ", ");
        }

        Set<Entry<String, Double>> bothItems = favoriteNumbers.entrySet();

        for (Entry<String, Double> entry: bothItems ) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }
}
