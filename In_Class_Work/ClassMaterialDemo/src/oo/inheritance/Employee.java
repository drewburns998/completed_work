/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oo.inheritance;

/**
 *
 * @author apprentice
 */
public abstract class Employee {

    private String employeeID;
    private String name;
    private String jobDescription;
    protected int salary;

    public Employee(String employeeId){
        this.employeeID = employeeId;
    }
    
    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getName() {
        whoAmI();
        return name;        
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }
    
    public void doWork(){
        System.out.println("Work work work ...");
    }
    
    protected void takeLunch(){
        System.out.println("Om nom nom...");
    }
    
    private void whoAmI(){
        System.out.println("I am " + name);
    }
    
    abstract public void takeRaise();
    
    public int getSalary(){
        return salary;
    }
}
