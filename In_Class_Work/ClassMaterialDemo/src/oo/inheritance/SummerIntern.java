/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oo.inheritance;

/**
 *
 * @author apprentice
 */
public class SummerIntern extends Employee {

    private boolean flipFlops;

    public SummerIntern(String employeeId) {
        super(employeeId);
        flipFlops = true;
        this.salary = 0;
    }

    public SummerIntern(String employeeId, boolean flipFlops) {
        super(employeeId);
        this.flipFlops = flipFlops;
    }

    public void getCoffee() {
        System.out.println("I'll be right back, Starbucks trip...!!!");
    }

    public boolean getFlipFlops() {
        return flipFlops;
    }

    public void setFlipFlops(boolean flipFlops) {
        this.flipFlops = flipFlops;
    }
    
    public void takeLongLunch(){
        takeLunch();
        takeLunch();
    }
    
    public void takeRaise(){
        salary += 100;
    }
}
