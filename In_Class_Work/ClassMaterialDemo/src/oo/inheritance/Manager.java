/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oo.inheritance;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class Manager extends Employee {

    private List<Employee> directReports; // = new ArrayList<Employee>();
    private String department;
    
    public Manager(String employeeId) {
        super(employeeId);
        department = "General";
        directReports = new ArrayList();
        this.salary = 600000;
    }

    public Manager(String employeeId, String department){
        super(employeeId);
        this.department = department;
    }

    public List<Employee> getDirectReports() {
        return directReports;
    }

    public void setDirectReports(List<Employee> directReports) {
        this.directReports = directReports;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
    
    @Override
    public String getJobDescription() {
        return "I manage all the peoples";
    }
    
    @Override
    public void takeRaise(){
        salary *= 1.5;
    }
}
