/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oo.interfaces;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class Manager extends Employee
        implements Promotable, Fireable {

    List<SummerIntern> directReports = new ArrayList<>();

    public Manager(String employeeId) {
        super(employeeId);
    }

    @Override
    public boolean userCardToEnterBuilding() {
        System.out.println(this.getName() + " : " + this.getEmployeeId()
                + "shows the badge to the front gate guard ...");
        return true;
    }

    @Override
    public void updateCard(String cardToken) {
        this.setEmployeeId(cardToken);
    }

    @Override
    public void promote() {
        System.out.println(this.getName() + " has been promoted!");
        this.setSalary(this.getSalary() * 1.5);
    }

    @Override
    public void fire() {
        System.out.println(this.getName() + " has ticked off people and gotten fired!");
        this.setSalary(0);
    }

}
