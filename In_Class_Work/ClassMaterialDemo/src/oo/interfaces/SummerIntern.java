/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oo.interfaces;

/**
 *
 * @author apprentice
 */
public class SummerIntern extends Employee{

    private boolean wearingFlipFlops = true;
    
    public SummerIntern(String employeeId) {
        super(employeeId);
    }

    @Override
    public boolean userCardToEnterBuilding() {
        System.out.println(this.getName() + " : " + this.getEmployeeId()
                + "waits for their manager to let them in the gate ...");
        return false;
    }

    @Override
    public void updateCard(String cardToken) {
        this.setEmployeeId(cardToken);
    }

    public boolean isWearingFlipFlops() {
        return wearingFlipFlops;
    }

    public void setWearingFlipFlops(boolean wearingFlipFlops) {
        this.wearingFlipFlops = wearingFlipFlops;
    }
    
    
    
}
