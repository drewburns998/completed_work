/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oo.company;

import java.util.ArrayList;
import java.util.List;
import oo.inheritance.Employee;
import oo.inheritance.Manager;
import oo.inheritance.SummerIntern;

/**
 *
 * @author apprentice
 */
public class LlamasLimited {

    public static void main(String[] args) {

        Employee bob = new SummerIntern("1337-bob");
        //bob.takeLunch();

        Manager sally = new Manager("aserw-s4lly");
        System.out.println("sally does this: " + sally.getJobDescription());
        sally.setJobDescription("Manager of Silly Walks");

        SummerIntern notBob = new SummerIntern("n0t-b0b");
        notBob.setJobDescription("gettter of coffees");

        notBob.takeLongLunch();

        List<Employee> llamasLimitedEmployees = new ArrayList<Employee>();

        llamasLimitedEmployees.add(bob);
        llamasLimitedEmployees.add(notBob);
        llamasLimitedEmployees.add(sally);

        //Employee notBobAgain = llamasLimitedEmployees.get(1);        
        //SummerIntern notBobAgain = (SummerIntern)llamasLimitedEmployees.get(1);
        //SummerIntern actuallyBob = (SummerIntern)llamasLimitedEmployees.get(0);
        if (llamasLimitedEmployees.get(1).getClass().equals(SummerIntern.class)) {
            SummerIntern notBobAgain = (SummerIntern) llamasLimitedEmployees.get(1);
            notBobAgain.takeLongLunch();
        }

        System.out.println("Not Bob does this " + llamasLimitedEmployees.get(1).getJobDescription());
        System.out.println("Sally " + llamasLimitedEmployees.get(2).getJobDescription());
        
        for(Employee e : llamasLimitedEmployees){
            e.takeRaise();
            System.out.println(e.getEmployeeID() + " : " + e.getSalary());
        }

    }
}
