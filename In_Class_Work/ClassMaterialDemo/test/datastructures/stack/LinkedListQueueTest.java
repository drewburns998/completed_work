/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.stack;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class LinkedListQueueTest {

    LinkedListQueue testObj = new LinkedListQueue();
    
    public LinkedListQueueTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void isEmptyTestWithEmptyStack() {

        Assert.assertTrue(testObj.isEmpty());

    }

    @Test
    public void isEmptyTestWithManyItemsInStack() {
        testObj.enqueue(1);
        testObj.enqueue(2);
        testObj.enqueue(3);
        testObj.enqueue(4);
        Assert.assertFalse(testObj.isEmpty());

    }

    @Test
    public void isEmptyTestWithManyItemsRemoved() {
        testObj.enqueue(1);
        testObj.enqueue(2);
        testObj.dequeue();
        testObj.dequeue();
        Assert.assertTrue(testObj.isEmpty());
    }

    @Test
    public void isEmptyTestWithResize() {
        testObj.enqueue(1);
        testObj.enqueue(2);
        testObj.enqueue(3);
        testObj.enqueue(4);
        testObj.enqueue(5);
        testObj.dequeue();
        testObj.dequeue();
        testObj.dequeue();
        testObj.dequeue();
        testObj.dequeue();
        Assert.assertTrue(testObj.isEmpty());
    }

    @Test
    public void isEmptyTestWithManyItemsRemovedSomeRemaining() {
        testObj.enqueue(1);
        testObj.enqueue(2);
        testObj.enqueue(3);
        testObj.dequeue();
        
        Assert.assertFalse(testObj.isEmpty());
    }

    @Test
    public void popTestWhenEmpty() {
        testObj.dequeue();
        Assert.assertTrue(testObj.isEmpty());

    }

    @Test
    public void popTestWithManyItemsInStack() {
        testObj.enqueue(1);
        testObj.enqueue(2);
        testObj.enqueue(3);
        testObj.dequeue();

        Assert.assertFalse(testObj.isEmpty());
    }

    @Test
    public void popTestFirstPopValueValidation() {
        testObj.enqueue(1);
        testObj.enqueue(2);
        testObj.enqueue(3);
        Assert.assertTrue(testObj.dequeue().equals(1));

    }

    @Test
    public void popTestLastPopValueValidation() {
        testObj.enqueue(1);        
        testObj.enqueue(2);
        testObj.enqueue(3);
        testObj.dequeue();
        testObj.dequeue();
        Assert.assertTrue(testObj.dequeue().equals(3));
    }

    @Test
    public void incSizeResizeTest() {
        testObj.enqueue(1);
        testObj.enqueue(2);
        testObj.enqueue(3);
        testObj.enqueue(4);
        testObj.enqueue(5);
        testObj.enqueue(6);
        testObj.enqueue(7);
        testObj.enqueue(8);

        Assert.assertTrue(testObj.size() == 8);

    }

    @Test
    public void incSizeResizeTestWithEmptyParts() {
        testObj.enqueue(1);
        testObj.enqueue(2);
        testObj.enqueue(3);
        testObj.enqueue(4);
        testObj.enqueue(5);
        testObj.enqueue(6);

        Assert.assertTrue(testObj.size() == 6);

    }

    @Test
    public void incSizeThenDecSizeTest() {
        testObj.enqueue(1);
        testObj.enqueue(2);
        testObj.enqueue(3);
        testObj.enqueue(4);
        testObj.enqueue(5);
        testObj.enqueue(6);
        testObj.dequeue();
        testObj.dequeue();

        Assert.assertTrue(testObj.size() == 4);

    }

}
