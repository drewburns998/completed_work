/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.stack;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class ArrayStackTest {

    ArrayStack testObj = new ArrayStack();

    public ArrayStackTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void isEmptyTestWithEmptyStack() {

        Assert.assertTrue(testObj.isEmpty());

    }

    @Test
    public void isEmptyTestWithManyItemsInStack() {
        testObj.push(1);
        testObj.push(2);
        testObj.push(3);
        testObj.push(4);
        Assert.assertFalse(testObj.isEmpty());

    }

    @Test
    public void isEmptyTestWithManyItemsRemoved() {
        testObj.push(1);
        testObj.push(2);
        testObj.pop();
        testObj.pop();
        Assert.assertTrue(testObj.isEmpty());
    }

    @Test
    public void isEmptyTestWithResize() {
        testObj.push(1);
        testObj.push(2);
        testObj.push(3);
        testObj.push(4);
        testObj.push(5);
        testObj.pop();
        testObj.pop();
        testObj.pop();
        testObj.pop();
        testObj.pop();
        Assert.assertTrue(testObj.isEmpty());
    }

    @Test
    public void isEmptyTestWithManyItemsRemovedSomeRemaining() {
        testObj.push(1);
        testObj.push(2);
        testObj.pop();

        Assert.assertFalse(testObj.isEmpty());
    }

    @Test
    public void popTestWhenEmpty() {
        testObj.pop();
        Assert.assertTrue(testObj.isEmpty());

    }

    @Test
    public void popTestWithManyItemsInStack() {
        testObj.push(1);
        testObj.push(2);
        testObj.push(3);
        testObj.pop();

        Assert.assertFalse(testObj.isEmpty());
    }

    @Test
    public void popTestFirstPopValueValidation() {
        testObj.push(1);
        testObj.push(2);
        testObj.push(3);
        Assert.assertTrue(testObj.pop().equals(3));

    }

    @Test
    public void popTestLastPopValueValidation() {
        testObj.push(1);
        testObj.push(2);
        testObj.push(3);
        testObj.pop();
        testObj.pop();
        Assert.assertTrue(testObj.pop().equals(1));
    }

    @Test
    public void incSizeResizeTest() {
        testObj.push(1);
        testObj.push(2);
        testObj.push(3);
        testObj.push(4);
        testObj.push(5);
        testObj.push(6);
        testObj.push(7);
        testObj.push(8);

        Assert.assertTrue(testObj.size() == 8);

    }

    @Test
    public void incSizeResizeTestWithEmptyParts() {
        testObj.push(1);
        testObj.push(2);
        testObj.push(3);
        testObj.push(4);
        testObj.push(5);
        testObj.push(6);

        Assert.assertTrue(testObj.size() == 6);

    }

    @Test
    public void incSizeThenDecSizeTest() {
        testObj.push(1);
        testObj.push(2);
        testObj.push(3);
        testObj.push(4);
        testObj.push(5);
        testObj.push(6);
        testObj.pop();
        testObj.pop();

        Assert.assertTrue(testObj.size() == 4);

    }

}
