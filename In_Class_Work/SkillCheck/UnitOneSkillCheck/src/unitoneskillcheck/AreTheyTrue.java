/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unitoneskillcheck;

/**
 *
 * @author apprentice
 */
public class AreTheyTrue {
    public static void main(String[] args) {
        boolean test1 = true;
        boolean test2 = false;
        
        AreTheyTrue testFunction = new AreTheyTrue();
        
        System.out.println("true and true: " + testFunction.howTrue(test1, test1));
        System.out.println("false and false: " + testFunction.howTrue(test2, test2));
        System.out.println("true and false: " + testFunction.howTrue(test1, test2));
        System.out.println("false and true: " + testFunction.howTrue(test2, test1));
        
        
    }
    
    public String howTrue(boolean a, boolean b){
        String result = "";
        if(a == false && b == false){
            result = "NEITHER";
        } else if (a == true && b == true){
            result = "BOTH";
        } else{
            result = "ONLY ONE";
        }
        return result;
    }
}
