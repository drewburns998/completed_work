/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unitoneskillcheck;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WhatIsYourNumber {
    public static void main(String[] args) {
        int userInput = 0;
        Scanner sc = new Scanner(System.in);
        Counter testCase = new Counter();
        
        System.out.println("Please enter an integer: ");
        userInput = sc.nextInt();
        
        System.out.println("You input: " + userInput);
        
        //call previously created method
        testCase.toN(userInput);
        
        System.out.println("Thank you for playing!");
                
    }
}
