/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unitoneskillcheck;

/**
 *
 * @author apprentice
 */
public class Adder {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int test1, test2, test3, test4;

        Adder sumTwo = new Adder();

        test1 = sumTwo.add(1, 1);
        test2 = sumTwo.add(2, 3);
        test3 = sumTwo.add(5, 8);
        test4 = sumTwo.add(95, 42);

        System.out.println("1 + 1 = " + test1);
        System.out.println("2 + 3 = " + test2);
        System.out.println("5 + 8 = " + test3);
        System.out.println("95 + 42 = " + test4);

    }

    public int add(int x, int y) {
        return x + y;
    }

}
