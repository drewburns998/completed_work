/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unitoneskillcheck;

/**
 *
 * @author apprentice
 */
public class Counter {

    public static void main(String[] args) {
        Counter testCase = new Counter();

        //test case to 10
        testCase.to10();
        
        //test case to n
        testCase.toN(3);
        testCase.toN(8);
        testCase.toN(200);
    }

    public void to10() {
        for (int i = 1; i <= 10; i++) {
            System.out.println(i);
        }
    }
    
    public void toN(int x){
        for (int i = 0; i <= x ; i++) {
            System.out.println(i);
        }
    }

}
