/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

import objects.Llama.LlamaSpit;

/**
 *
 * @author apprentice
 */
public class LlamaFarm {
    public static void main(String[] args) {
        
        Llama noWhite = new Llama("Andrew");
        noWhite.setAge(10);
        System.out.println(noWhite.getName() + " is " + noWhite.getAge() + " yrs old" );
        noWhite.spit();
        
        Llama bubbleLlama = new Llama("Bubbles");
        
        LlamaSpit bubbleSpit = bubbleLlama.new LlamaSpit();

        LlamaThing thing = new Llama.LlamaThing();
     
        Llama ninjaLlama = new Llama(){
            public void spit(){
                System.out.println("Ptooey!!!!");
            }
        };
    }
}
