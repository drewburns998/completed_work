<%-- 
    Document   : index
    Created on : Mar 28, 2016, 10:17:55 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" />
        
        <title>Flooring Calculation MegaStore</title>
    </head>
    <body>
        <h1>Yo Dawg, I got that flooring .. here's the price</h1>
        <form method="POST" action="FlooringCalcServlet">
            Width:<input type=number min=100 max=1999 name="width">ft.<br/>
            Length:<input type=number min=100 max=1999 name="length">ft.<br/>
            Cost / SqFt:<input type=number min=0 max=199 name="costSqFt">$/ft^2<br/>
            <button type="submit" value="Submit">Submit</button>
        </form>
    </body>
</html>
