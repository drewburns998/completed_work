/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.flooringcalc;

/**
 *
 * @author apprentice
 */
public class FlooringCalculator {

    private double width, length, costSqFt, flooringCost, laborCost, laborTime, areaSqFt;

    private final double laborCostPerQuarterHour = 86 / 4;

    public FlooringCalculator(double width, double length, double costSqFt) {
        
        this.width = width;
        this.length = length;
        this.costSqFt = costSqFt;

        this.areaSqFt = width * length;
        this.flooringCost = areaSqFt * costSqFt;

        double leftOverFt = areaSqFt % 5;
        laborTime = (areaSqFt - leftOverFt) / 5;

        if (leftOverFt > 0) {
            laborTime = laborTime + 1;
        }

        laborCost = laborTime * laborCostPerQuarterHour;

    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public double getCostSqFt() {
        return costSqFt;
    }

    public double getFlooringCost() {
        return flooringCost;
    }

    public double getLaborCost() {
        return laborCost;
    }

    public double getLaborTime() {
        return laborTime;
    }

    public double getAreaSqFt() {
        return areaSqFt;
    }

}
