/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dvdlibrarymvc.controller;

import com.swcguild.dvdlibrarymvc.dao.DvdLibraryDao;
import com.swcguild.dvdlibrarymvc.model.Dvd;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@Controller
public class HomeController {
    
    private DvdLibraryDao dao;
    
    @Inject
    public HomeController(DvdLibraryDao dao){
        this.dao = dao;
    }
    
    @RequestMapping(value={"/","/home"}, method=RequestMethod.GET)
    public String displayHomePage(){
        return "home";
    }
    
    @RequestMapping(value="/dvd/{dvdId}", method=RequestMethod.GET)
    @ResponseBody
    public Dvd getTheContact(@PathVariable("dvdId")int id){
        return dao.getById(id);
    }
    
    @RequestMapping(value="/dvd", method=RequestMethod.POST)
    @ResponseBody @ResponseStatus(HttpStatus.CREATED)
    public Dvd createTheDvd(@Valid @RequestBody Dvd newDvd){        
        dao.add(newDvd);
        return newDvd;
    }
    
    @RequestMapping(value="/dvd/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTheDvd(@PathVariable("id")int dvdId){
        dao.remove(dvdId);
    }
    
    @RequestMapping(value="/dvd/{cId}", method=RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateTheDvd(@PathVariable("cId")int id, @RequestBody Dvd edittedDvd){        
        edittedDvd.setId(id);
        dao.updateDvd(edittedDvd);        
    }
    
    @RequestMapping(value="/dvds", method=RequestMethod.GET)
    @ResponseBody
    public List<Dvd> getAllTheContacts(){
        return dao.listAll();
    }   
    
}
