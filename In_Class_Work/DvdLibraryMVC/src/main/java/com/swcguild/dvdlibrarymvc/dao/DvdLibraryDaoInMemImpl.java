/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dvdlibrarymvc.dao;

import com.swcguild.dvdlibrarymvc.model.Dvd;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class DvdLibraryDaoInMemImpl implements DvdLibraryDao {

    private Map<Integer, Dvd> dvdMap = new HashMap<>();
    private static int dvdIdCounter = 0;

    public DvdLibraryDaoInMemImpl() {
        Dvd dvd = new Dvd();
        dvd.setTitle("Bravehart");
        dvd.setReleaseDate("07/30/1985");
        dvd.setMpaaRating("G");
        dvd.setStudio("Fox");
        dvd.setDirector("Spielberg");
        dvd.setUserRating(98);
        this.add(dvd);

        dvd = new Dvd();
        dvd.setTitle("Good Will Hunting");
        dvd.setReleaseDate("07/30/1985");
        dvd.setMpaaRating("R");
        dvd.setStudio("Indy");
        dvd.setDirector("Matt Damon");
        dvd.setUserRating(98);
        this.add(dvd);

    }

    @Override
    public Dvd add(Dvd dvd) {
        dvd.setId(dvdIdCounter);
        dvdIdCounter++;
        dvdMap.put(dvd.getId(), dvd);
        return dvd;
    }

    @Override
    public void remove(int id) {
        dvdMap.remove(id);
    }

    @Override
    public List<Dvd> listAll() {
        return new ArrayList(dvdMap.values());
    }

    @Override
    public Dvd getById(int id) {
        return dvdMap.get(id);
    }

    @Override
    public List<Dvd> searchDvdPred(Predicate<Dvd> dvdSearch) {
        return dvdMap.values().stream()
                .filter(dvdSearch)
                .collect(Collectors.toList());
    }

    @Override
    public List<Dvd> searchDvd(Map<SearchTerm, String> criteria) {

        //get all criteria
        String titleCriteria = criteria.get(SearchTerm.TITLE);
        String mpaaCriteria = criteria.get(SearchTerm.MPAA_RATING);
        String directorCriteria = criteria.get(SearchTerm.DIRECTOR);
        String studioCriteria = criteria.get(SearchTerm.STUDIO);
        String userRatingCriteria = criteria.get(SearchTerm.USER_RATING);
        String releaseDateCriteria = criteria.get(SearchTerm.RELEASE_DATE);

        // add in validation?
        double userRating = Double.parseDouble(userRatingCriteria);

        // Declare the predicates
        Predicate<Dvd> titlePred;
        Predicate<Dvd> mpaaPred;
        Predicate<Dvd> directorPred;
        Predicate<Dvd> studioPred;
        Predicate<Dvd> userRatingPred;
        Predicate<Dvd> releaseDatePred;

        // Declare & initialize an "all pass" predicate filter
        Predicate<Dvd> allPass = (Dvd) -> {
            return true;
        };

        titlePred = (titleCriteria == null || titleCriteria.isEmpty())
                ? allPass : (Dvd) -> Dvd.getTitle().toLowerCase().contains(titleCriteria.toLowerCase());
        mpaaPred = (mpaaCriteria == null || mpaaCriteria.isEmpty())
                ? allPass : (Dvd) -> Dvd.getMpaaRating().contains(mpaaCriteria);
        directorPred = (directorCriteria == null || directorCriteria.isEmpty())
                ? allPass : (Dvd) -> Dvd.getDirector().toLowerCase().contains(directorCriteria.toLowerCase());
        studioPred = (studioCriteria == null || studioCriteria.isEmpty())
                ? allPass : (Dvd) -> Dvd.getStudio().toLowerCase().contains(studioCriteria.toLowerCase());
        userRatingPred = (userRatingCriteria == null || userRatingCriteria.isEmpty())
                ? allPass : (Dvd) -> Dvd.getUserRating() == userRating;
        releaseDatePred = (releaseDateCriteria == null || releaseDateCriteria.isEmpty())
                ? allPass : (Dvd) -> Dvd.getReleaseDate().equals(releaseDateCriteria);

        return dvdMap.values().stream()
                .filter(titlePred)
                .filter(mpaaPred)
                .filter(directorPred)
                .filter(studioPred)
                .filter(userRatingPred)
                .filter(releaseDatePred)
                .collect(Collectors.toList());
    }

    @Override
    public void updateDvd(Dvd dvd) {
        dvdMap.put(dvd.getId(), dvd);
    }

}
