/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dvdlibrarymvc.dao;

import com.swcguild.dvdlibrarymvc.model.Dvd;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

/**
 *
 * @author apprentice
 */
public interface DvdLibraryDao {
    public Dvd add(Dvd dvd);
    public void remove(int id);
    public List<Dvd> listAll();
    public Dvd getById(int id);
    public void updateDvd(Dvd dvd);
    public List<Dvd> searchDvdPred(Predicate<Dvd> dvdSearch);
    public List<Dvd> searchDvd(Map<SearchTerm, String> criteria);    
    
}
