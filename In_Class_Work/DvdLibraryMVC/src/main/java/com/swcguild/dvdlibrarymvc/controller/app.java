/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dvdlibrarymvc.controller;

import com.swcguild.dvdlibrarymvc.dao.DvdLibraryDaoInMemImpl;
import com.swcguild.dvdlibrarymvc.dao.SearchTerm;
import com.swcguild.dvdlibrarymvc.model.Dvd;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public class app {
    
    public static void main(String[] args) {
        DvdLibraryDaoInMemImpl dao = new DvdLibraryDaoInMemImpl();
        
        Map<SearchTerm, String> criteriaMap = new HashMap<>();
        String currentTerm = "brave";
        
        criteriaMap.put(SearchTerm.TITLE, currentTerm);
        
        
        List<Dvd> test = dao.searchDvd(criteriaMap);
        
        for(Dvd a : test){
            System.out.println(a.getId());
            System.out.println(a.getTitle());
        }
        
    }
}
