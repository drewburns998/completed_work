<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>DVD Library Search</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/">Home</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/search">Search</a></li>                    
                </ul>    
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h2>Search Results</h2>
                    <%@include file="dvdSummaryTableFragment.jsp" %>
                </div>
                <div class="col-md-offset-1 col-md-5">
                    <h2>Search DVD Library</h2>
                    <form class="form-vertical" role="form">
                        <div class="form-group">
                            <label for=search-title" class="col-md-4 control-label">Title:</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="search-title" placeholder="Title" /></div>
                        </div>
                        <div class="form-group">
                            <label for=search-release-date" class="col-md-4 control-label">Release Date:</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="search-release-date" placeholder="Release Date" /></div>
                        </div>
                        <div class="form-group">
                            <label for=search-mpaa-rating" class="col-md-4 control-label">MPAA Rating:</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="search-mpaa-rating" placeholder="MPAA Rating" /></div>
                        </div>
                        <div class="form-group">
                            <label for=search-director" class="col-md-4 control-label">Director:</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="search-director" placeholder="Director" /></div>
                        </div>
                        <div class="form-group">
                            <label for=search-studio" class="col-md-4 control-label">Studio:</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="search-studio" placeholder="Studio" /></div>
                        </div>
                        <div class="form-group">
                            <label for=search-user-rating" class="col-md-4 control-label">User Rating:</label>
                            <div class="col-md-8"><input type="number" class="form-control" id="search-user-rating" placeholder="User Rating" /></div>
                        </div>
                        <br/>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <button id="search-button"type="submit" class="btn btn-default btn-primary">Search DVDs</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <%@include file="detailsAndEditModalFragment.jsp"%>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <%@include file="scriptsFragment.jsp"%>

    </body>
</html>

