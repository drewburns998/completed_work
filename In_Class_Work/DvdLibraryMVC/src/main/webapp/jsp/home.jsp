<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>DVD Library Application</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                    
                </ul>    
            </div>
            
                <div class="col-md-6">
                    <h2>My DVD's</h2>
                    <%@include file="dvdSummaryTableFragment.jsp" %>
                </div>

                <div class="col-md-offset-1 col-md-5">
                    <h2>Add New DVD</h2>
                    <form class="form-vertical" role="form">
                        <div class="form-group">
                            <label for=add-title" class="col-md-4 control-label">Title:</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="add-title" placeholder="Title" /></div>
                        </div>
                        <div class="form-group">
                            <label for=add-release-date" class="col-md-4 control-label">Release Date:</label>
                            <div class="col-md-8"><input type="date" class="form-control" id="add-release-date" placeholder="Last Name" /></div>
                        </div>
                        
                        <div class="form-group">
                            <label for=add-mpaa-rating" class="col-md-4 control-label">MPAA Rating:</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="add-mpaa-rating" placeholder="MPAA Rating" /></div>
                        </div>
                        <div class="form-group">
                            <label for=add-director" class="col-md-4 control-label">Director:</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="add-director" placeholder="Director" /></div>
                        </div>
                        <div class="form-group">
                            <label for=add-studio" class="col-md-4 control-label">Studio:</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="add-email" placeholder="Studio" /></div>
                        </div>
                        <div class="form-group">
                            <label for=add-user-rating" class="col-md-4 control-label">User Rating:</label>
                            <div class="col-md-8"><input type="number" step='0.1' class="form-control" id="add-user-rating" placeholder="User Rating" /></div>
                        </div>
                        <br/>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <button id="add-button"type="submit" class="btn btn-default btn-primary">Create Dvd</button>
                            </div>
                        </div>
                    </form>
                </div>
            
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <!-- Include File -->            
        <%@include file="detailsAndEditModalFragment.jsp"%>        
        <%@include file="scriptsFragment.jsp"%>

    </body>
</html>

