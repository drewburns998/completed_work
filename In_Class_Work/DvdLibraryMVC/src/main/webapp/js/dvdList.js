/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    // put other code to call when the document has finished loading
    loadDvds();

    $('#add-button').click(function (event) {

        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'dvd',
            data: JSON.stringify({
                id: $('#edit-dvdId').val(),
                title: $('#add-title').val(),
                releaseDate: $('#add-release-date').val(),
                mpaaRating: $('#add-mpaa-rating').val(),
                director: $('#add-director').val(),
                studio: $('#add-studio').val(),                
                userRating: $('#add-user-rating').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            $('#add-title').val('');
            $('#add-release-date').val('');
            $('#add-mpaa-rating').val('');
            $('#add-director').val('');
            $('#add-studio').val('');
            $('#add-user-rating').val('');

            loadDvds();
        });

    });

    $('#edit-button').click(function (event) {

        event.preventDefault();

        $.ajax({
            type: 'PUT',
            url: 'dvd/' + $('#edit-dvd-id').val(),
            data: JSON.stringify({
                id: $('#edit-dvd-id').val(),
                title: $('#edit-title').val(),
                releaseDate: $('#edit-release-date').val(),
                mpaaRating: $('#edit-mpaa-rating').val(),
                director: $('#edit-director').val(),
                studio: $('#edit-studio').val(),
                userRating: $('#edit-user-rating').val()                
                
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            loadDvds();
        });

    });

    $('#search-button').click(function(event) {

        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'search/dvds',
            data: JSON.stringify({
                titleSearch: $('#search-title').val(),
                releaseDateSearch: $('#search-release-date').val(),
                mpaaRatingSearch: $('#search-mpaa-rating').val(),
                directorSearch: $('#search-director').val(),
                studioSearch: $('#search-studio').val(),
                userRatingSearch: $('#search-user-rating').val()                
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            fillDvdTable(data, status);
        });
    });

});

function loadDvds() {

    var cTableBody = $('#contentRows');
    cTableBody.empty();

    $.ajax({
        url: "dvds",
        type: "GET",
        headers: {
            'Accept': 'application/json'
        }
    }).success(function(data, status) {
        fillDvdTable(data, status);
    });
}

function fillDvdTable(dvdList, status) {

    var cTableBody = $('#contentRows');
    cTableBody.empty();
    $.each(dvdList, function (index, dvd) {
        cTableBody.append($('<tr>')
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-dvd-id': dvd.id,
                                    'data-toggle': 'modal',
                                    'data-target': '#detailsModal'
                                })
                                .text(dvd.title)
                                )
                        )
                .append($('<td>').text(dvd.releaseDate))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-dvd-id': dvd.id,
                                    'data-toggle': 'modal',
                                    'data-target': '#editModal'
                                })
                                .text('Edit')
                                )
                        )
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'onClick': 'deleteDvd(' + dvd.id + ')'
                                })
                                .text('Delete')
                                )
                        )
                );
    });

}

function deleteDvd(id) {

    var answer = confirm("Do you REALLY want to delete this DVD?");

    if (answer === true) {
        $.ajax({
            url: 'dvd/' + id,
            type: 'DELETE'

        }).success(function () {
            loadDvds();
        });
    }
}

// This code is going to run in response to the show.bs.modal event
$('#detailsModal').on('show.bs.modal', function (event) {

    var element = $(event.relatedTarget);
    var dvdId = element.data('dvd-id');

    var modal = $(this);

    $.ajax({
        type: 'GET',
        url: 'dvd/' + dvdId,
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (dvd) {
        modal.find('#dvd-id').text(dvd.id);
        modal.find('#dvd-title').text(dvd.title);
        modal.find('#dvd-release-date').text(dvd.releaseDate);
        modal.find('#dvd-mpaa-rating').text(dvd.mpaaRating);
        modal.find('#dvd-director').text(dvd.director);
        modal.find('#dvd-studio').text(dvd.studio);
        modal.find('#dvd-user-rating').text(dvd.userRating);
    });

});

// This code is going to run in response to the show.bs.modal event
$('#editModal').on('show.bs.modal', function (event) {

    var element = $(event.relatedTarget);
    var dvdId = element.data('dvd-id');

    var modal = $(this);

    $.ajax({
        type: 'GET',
        url: 'dvd/' + dvdId,
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (dvd) {

        modal.find('#edit-dvd-id').text(dvd.id);
        modal.find('#edit-title').val(dvd.title);
        modal.find('#edit-release-date').val(dvd.releaseDate);
        modal.find('#edit-mpaa-rating').val(dvd.mpaaRating);
        modal.find('#edit-director').val(dvd.director);
        modal.find('#edit-studio').val(dvd.studio);
        modal.find('#edit-user-rating').val(dvd.userRating);        

    });

});



  