<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Blacksmith Blog">
        <meta name="author" content="Andrew Carrie Solomon">
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/blog-post.css" rel="stylesheet">
    </head>
    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="${pageContext.request.contextPath}">Blog</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <%@include file="navBarFragment.jsp"%>
                </div>
                <!-- /.navbar-collapse -->

            </div>

            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class="container">
            <div class="wide">
                <div class="col-xs-5 line"><hr></div>
                <div class="col-xs-2 logo"></div>
                <div class="col-xs-5 line"><hr></div>
            </div>

            <div class="row">

                <!-- Blog Post Content Column -->
                <div class="col-lg-8 col-lg-offset-2 text-center">

                    <!-- Blog Post -->

                    <!-- Title -->
                    <h1>Disclaimer</h1>

                    <!-- Author -->
                    <hr>
                    <p>
                        These are my own personal thoughts, beliefs, and perspectives of the world.
                        This is not representative of any employers, planning committees, clients, or any
                        other associations tied with me or my name.  Every person has an opinion on something,
                        and these are mine.  We may end up having to agree to disagree, and I can guarantee 
                        you that not everyone will feel the way that I do.                    
                    </p>
                    <br>
                    <p>
                        I may post happy things or sad things, run-of-the-mill things or controversial things.
                        Whatever I post is most likely posted straight out of m ymind, as I write stream-of-conscious
                        style.
                    </p>
                    <br>
                    <p>
                        If you have any questions for me personally, please feel free to email me <a href="${pageContext.request.contextPath}/contact">here</a>.
                    </p>



                </div>



            </div>
            <!-- /.row -->

            <hr>

            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; Your Website 2014</p>
                    </div>
                </div>
                <!-- /.row -->
            </footer>

        </div>
        <!-- /.container -->       

    </body>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/blogEntries.js"></script>

</html>

