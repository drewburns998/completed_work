/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.blacksmithblogcapstone.dto;

import java.sql.Timestamp;

/**
 *
 * @author apprentice
 */
public class Comment {
    int id;
    String body;
    String name;
    Timestamp timestamp;
    int blogEntryId;

    public Comment() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public int getBlogEntryId() {
        return blogEntryId;
    }

    public void setBlogEntryId(int blogEntryId) {
        this.blogEntryId = blogEntryId;
    }
}
