/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.blacksmithblogcapstone.dao;

import com.swcguild.blacksmithblogcapstone.dto.BlogEntry;
import com.swcguild.blacksmithblogcapstone.dto.BlogSummary;
import com.swcguild.blacksmithblogcapstone.dto.Category;
import com.swcguild.blacksmithblogcapstone.dto.Comment;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface BlackSmithDao {

    public BlogEntry addBlogEntry(BlogEntry blogEntry);

    public Comment addComment(Comment comment);

//    public BlogAuthor addBlogAuthor(String name);
    public List<BlogEntry> getAllBlogEntries();

    public BlogEntry getBlogEntryById(int id);

    public List<Comment> getCommentsByBlogId(int blogId);

    public List<BlogSummary> getBlogSummaries();

    public BlogEntry getBlogEntryByTitle(String title);
    
    public List<Category> getAllCategories();

    public void editBlogEntry(BlogEntry editedBlogEntry);
    
    public void approveBlogEntry (int blogEntryId);

    public void removeBlogEntry(int blogId);

    public void removeComment(int blogId);

    public List<BlogEntry> searchBody(String term);

    public List<BlogEntry> searchTitle(String term);

    public List<Category> searchCategory(String searchCategory);

    public List<BlogEntry> searchBlogEntries(String searchTerm);

}
