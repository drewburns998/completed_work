package com.swcguild.blacksmithblogcapstone.dao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.swcguild.blacksmithblogcapstone.dto.BlogEntry;
import com.swcguild.blacksmithblogcapstone.dto.Comment;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author Andrew Carrie Solomon
 */
public class NewEmptyJUnitTest {

    private BlackSmithDao dao;

    public NewEmptyJUnitTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("testDao", BlackSmithDao.class);

        JdbcTemplate cleaner = ctx.getBean("jdbcTemplate", JdbcTemplate.class);

        cleaner.execute("DELETE FROM BlogEntriesCategories");
        cleaner.execute("DELETE FROM BlogEntriesKeywords");
        cleaner.execute("DELETE FROM Categories");
        cleaner.execute("DELETE FROM Comments");
        cleaner.execute("DELETE FROM Keywords");
        cleaner.execute("DELETE FROM BlogEntries");

    }

    @After
    public void tearDown() {

    }

    public void addTestBlogEntry() {
        BlogEntry testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("test author");
        testBlogEntry.setBody("test body");
        testBlogEntry.setCategory("test category");
        testBlogEntry.setTitle("test title");

        dao.addBlogEntry(testBlogEntry);
    }

    @Test
    public void testAddBlogEntry() {
        BlogEntry testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("test author");
        testBlogEntry.setBody("test body");
        testBlogEntry.setCategory("test category");
        testBlogEntry.setTitle("test title");

        dao.addBlogEntry(testBlogEntry);

        Assert.assertNotNull(dao.getBlogEntryById(testBlogEntry.getId()));
        Assert.assertEquals("test author", dao.getBlogEntryById(testBlogEntry.getId()).getAuthorName());
        Assert.assertTrue(dao.getAllBlogEntries().size() == 1);
    }

    @Test
    public void testAddEmptyBlogEntry() {
        BlogEntry testBlogEntry = new BlogEntry();

        dao.addBlogEntry(testBlogEntry);
        Assert.assertTrue(dao.getAllBlogEntries().size() == 1);
        Assert.assertNull(dao.getBlogEntryById(testBlogEntry.getId()).getAuthorName());

    }

    @Test
    public void addCommentTest() {

        BlogEntry testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("test author");
        testBlogEntry.setBody("test body");
        testBlogEntry.setCategory("test category");
        testBlogEntry.setTitle("test title");

        dao.addBlogEntry(testBlogEntry);

        Comment newComment = new Comment();

        newComment.setBody("Comment body");
        newComment.setName("Comment name");
        newComment.setBlogEntryId(testBlogEntry.getId());
        newComment.setId(-1);

        dao.addComment(newComment);

        Assert.assertTrue(dao.getCommentsByBlogId(testBlogEntry.getId()).size() > 0);
        Assert.assertEquals("Comment name", newComment.getName());
    }

    @Test
    public void getAllBlogEntriesTest() {

        Assert.assertEquals(0, dao.getAllBlogEntries().size());

        BlogEntry testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("test author");
        testBlogEntry.setBody("test body");
        testBlogEntry.setCategory("test category");
        testBlogEntry.setTitle("test title");

        dao.addBlogEntry(testBlogEntry);

        Assert.assertEquals(1, dao.getAllBlogEntries().size());

    }

    @Test
    public void getBlogEntryByTitleTest() {
        BlogEntry testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("test author");
        testBlogEntry.setBody("test body");
        testBlogEntry.setCategory("test category");
        testBlogEntry.setTitle("test title");

        dao.addBlogEntry(testBlogEntry);

        Assert.assertEquals("test author", dao.getBlogEntryByTitle("test title").getAuthorName());
    }

    @Test
    public void editBlogEntryTest() {
        BlogEntry testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("test author");
        testBlogEntry.setBody("test body");
        testBlogEntry.setCategory("test category");
        testBlogEntry.setTitle("test title");

        dao.addBlogEntry(testBlogEntry);
        int testBlogEntryId = testBlogEntry.getId();

        Assert.assertEquals(1, dao.getAllBlogEntries().size());

        testBlogEntry.setTitle("edited title");
        dao.editBlogEntry(testBlogEntry);
        Assert.assertEquals(testBlogEntryId, testBlogEntry.getId());
        Assert.assertEquals("edited title", testBlogEntry.getTitle());
    }

    @Test
    public void removeBlogEntryTest() {

        BlogEntry testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("test author");
        testBlogEntry.setBody("test body");
        testBlogEntry.setCategory("test category");
        testBlogEntry.setTitle("test title");

        dao.addBlogEntry(testBlogEntry);
        int testBlogEntryId = testBlogEntry.getId();

        Assert.assertEquals(1, dao.getAllBlogEntries().size());

        dao.removeBlogEntry(testBlogEntryId);
        Assert.assertEquals(0, dao.getAllBlogEntries().size());
    }

    @Test
    public void removeCommentTest() {

        BlogEntry testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("test author");
        testBlogEntry.setBody("test body");
        testBlogEntry.setCategory("test category");
        testBlogEntry.setTitle("test title");

        dao.addBlogEntry(testBlogEntry);

        //remove a comment that doesn't exist
        Assert.assertTrue(dao.getCommentsByBlogId(testBlogEntry.getId()).isEmpty());

        Comment newComment = new Comment();

        newComment.setBody("Comment body2");
        newComment.setName("Comment name2");
        newComment.setBlogEntryId(testBlogEntry.getId());
        newComment.setId(-1);

        dao.addComment(newComment);

        Assert.assertTrue(dao.getCommentsByBlogId(testBlogEntry.getId()).size() == 1);
        Assert.assertEquals("Comment name2", newComment.getName());

        newComment = new Comment();

        newComment.setBody("Comment body");
        newComment.setName("Comment name");
        newComment.setBlogEntryId(testBlogEntry.getId());

        dao.addComment(newComment);

        Assert.assertTrue(dao.getCommentsByBlogId(testBlogEntry.getId()).size() == 2);
        Assert.assertEquals("Comment name", newComment.getName());

        dao.removeComment(newComment.getId());
        Assert.assertEquals(1, dao.getCommentsByBlogId(testBlogEntry.getId()).size());

    }

    @Test
    public void searchBlogEntries() {

        BlogEntry testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("test author");
        testBlogEntry.setBody("test body");
        testBlogEntry.setCategory("test category");
        testBlogEntry.setTitle("test title");

        dao.addBlogEntry(testBlogEntry);

        Assert.assertEquals(1, dao.searchBlogEntries("body").size());
        Assert.assertEquals(0, dao.searchBlogEntries("PENIS").size());

        testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("another test author");
        testBlogEntry.setBody("another test body");
        testBlogEntry.setCategory("another test category");
        testBlogEntry.setTitle("another test title");

        dao.addBlogEntry(testBlogEntry);

        Assert.assertEquals(1, dao.searchBlogEntries("another").size());
        Assert.assertEquals(2, dao.searchBlogEntries("body").size());
        Assert.assertEquals(0, dao.searchBlogEntries("PENIS").size());
        Assert.assertEquals(1, dao.searchBlogEntries("er te").size());

    }

//    @Test
//    public void searchTitleTest() {
//    }
    @Test
    public void getBlogSummariesTest() {

        Assert.assertEquals(0, dao.getBlogSummaries().size());

        BlogEntry testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("test author");
        testBlogEntry.setBody("test body");
        testBlogEntry.setCategory("test category");
        testBlogEntry.setTitle("test title");

        dao.addBlogEntry(testBlogEntry);

        Assert.assertEquals(1, dao.getBlogSummaries().size());

        testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("another test author");
        testBlogEntry.setBody("another test body");
        testBlogEntry.setCategory("another test category");
        testBlogEntry.setTitle("another test title");

        dao.addBlogEntry(testBlogEntry);

        Assert.assertEquals(2, dao.getBlogSummaries().size());
    }

    @Test
    public void getAllCategoriesTest() {

        Assert.assertEquals(0, dao.getBlogSummaries().size());

        BlogEntry testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("test author");
        testBlogEntry.setBody("test body");
        testBlogEntry.setCategory("test category");
        testBlogEntry.setTitle("test title");

        dao.addBlogEntry(testBlogEntry);

        Assert.assertEquals(1, dao.getAllCategories().size());

        testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("another test author");
        testBlogEntry.setBody("another test body");
        testBlogEntry.setCategory("another test category");
        testBlogEntry.setTitle("another test title");

        dao.addBlogEntry(testBlogEntry);

        Assert.assertEquals(2, dao.getAllCategories().size());
    }

    @Test
    public void searchCategoryTest() {
        Assert.assertEquals(0, dao.getBlogSummaries().size());

        BlogEntry testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("test author");
        testBlogEntry.setBody("test body");
        testBlogEntry.setCategory("test category");
        testBlogEntry.setTitle("test title");

        dao.addBlogEntry(testBlogEntry);

        Assert.assertEquals(1, dao.searchCategory("test category").size());

        testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("another test author");
        testBlogEntry.setBody("another test body");
        testBlogEntry.setCategory("test category");
        testBlogEntry.setTitle("another test title");

        dao.addBlogEntry(testBlogEntry);

        Assert.assertEquals(2, dao.searchCategory("test category").size());
    }

    @Test
    public void approveBlogEntryTest() {
        BlogEntry testBlogEntry = new BlogEntry();

        testBlogEntry.setAuthorName("test author");
        testBlogEntry.setBody("test body");
        testBlogEntry.setCategory("test category");
        testBlogEntry.setTitle("test title");

        dao.addBlogEntry(testBlogEntry);
        System.out.println(testBlogEntry.getId());
        Assert.assertEquals("NOT_APPROVED", testBlogEntry.getApprovalStatus());

        dao.approveBlogEntry(testBlogEntry.getId());

        testBlogEntry.setApprovalStatus("APPROVED");
        System.out.println(testBlogEntry.getApprovalStatus());

        Assert.assertEquals("APPROVED", testBlogEntry.getApprovalStatus());
    }

}
