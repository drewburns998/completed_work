/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.blacksmithblogcapstone.dto;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author apprentice
 */
public class BlogSummary {

    int blogId;
    String title;
    String author;
    int numComments;
    Date date;
    String category;
    String approvalStatus;

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }
    

    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setNumComments(int numComments) {
        this.numComments = numComments;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setCategory(String category) {
        this.category = category;
    }
   

    public BlogSummary(int blogId, String title, String author, int numComments, Timestamp date, String category, String approvalStatus) {
        this.blogId = blogId;
        this.title = title;
        this.author = author;
        this.numComments = numComments;
        this.date = date;
        this.category = category;
        this.approvalStatus = approvalStatus;
    }

    public BlogSummary() {
    }

    public int getBlogId() {
        return blogId;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getNumComments() {
        return numComments;
    }

    public Date getDate() {
        return date;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + this.blogId;
        hash = 23 * hash + Objects.hashCode(this.title);
        hash = 23 * hash + Objects.hashCode(this.author);
        hash = 23 * hash + this.numComments;
        hash = 23 * hash + Objects.hashCode(this.date);
        hash = 23 * hash + Objects.hashCode(this.category);
        hash = 23 * hash + Objects.hashCode(this.approvalStatus);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BlogSummary other = (BlogSummary) obj;
        if (this.blogId != other.blogId) {
            return false;
        }
        if (this.numComments != other.numComments) {
            return false;
        }
        if (!Objects.equals(this.title, other.title)) {
            return false;
        }
        if (!Objects.equals(this.author, other.author)) {
            return false;
        }
        if (!Objects.equals(this.category, other.category)) {
            return false;
        }
        if (!Objects.equals(this.approvalStatus, other.approvalStatus)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }
    
}
