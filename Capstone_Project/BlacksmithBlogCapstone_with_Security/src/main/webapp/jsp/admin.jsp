<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Blacksmith Blog">
        <meta name="author" content="Andrew Carrie Solomon">
        <title>Admin</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/blog-post.css" rel="stylesheet">

        <script src="https://cdn.tinymce.com/4/tinymce.min.js"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: 'textarea', // change this value according to your HTML
                plugins: 'code',
                a_plugin_option: true,
                a_configuration_option: 400
            });
        </script>        
    </head>
    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="${pageContext.request.contextPath}">Blog</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <%@include file="navBarFragment.jsp"%>
                </div>
                <!-- /.navbar-collapse -->

            </div>

            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class="container">

            <div class="row">

                <!-- Blog Post Content Column -->
                <div class="col-lg-12">

                    <!-- Blog Post -->

                    <!-- Title -->
                    <div class="row">
                        <h1>Admin View</h1>    
                    </div>
                    <h3></h3>
                    <!-- Author -->
                    <div class="row">

                        <div class="col-sm-3">
                            <button class="btn btn-warning btn-block" type = "submit" data-toggle="collapse" data-target="#summaryRows">View Blog Summaries</button>
                        </div>
                        <div class="col-sm-3">
                            <button class="btn btn-warning btn-block" type = "submit" data-toggle="collapse" data-target="#addBlogForm">Add New Blog</button>
                        </div>                        
                    </div>
                    <br>
                    <div id="errorDiv"></div>
                    <div id="addBlogForm" class="collapse">

                        <form method="post">                            
                            <fieldset class="form-group">
                                <label for="blogTitle">Title</label>
                                <input type="text" class="form-control" id="blogTitle" placeholder="Title">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="blogAuthor">Author</label>
                                <input type="text" class="form-control" id="blogAuthor" placeholder="Author">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="blogCategory">Category</label>
                                <input type="text" class="form-control" id="blogCategory" placeholder="Category">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="blogContent">Content</label>                            
                                <textarea class="form-control" id="blogContent" rows="10"></textarea>
                            </fieldset>
                            <button id="add-button" type="submit" class="btn btn-primary">Submit</button>

                        </form>
                    </div>

                    <div id="validationErrors" style="color: red"></div>

                    <!-- Blogs generated here via javascript file -->

                    <%@include file="blogSummaryFragment.jsp" %>


                </div>
            </div>
            <!-- /.row -->
            <hr>
            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; Your Website 2014</p>
                    </div>
                </div>
                <!-- /.row -->
            </footer>

        </div>
        <!-- /.container -->       


        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/blogEntries.js"></script>
    </body>
</html>