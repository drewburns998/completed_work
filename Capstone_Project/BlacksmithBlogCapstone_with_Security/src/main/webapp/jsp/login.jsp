<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Blacksmith Blog">
        <meta name="author" content="Andrew Carrie Solomon">
        <title>Login Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/blog-post.css" rel="stylesheet">
    </head>
    <body>
        <div>

            <!-- #1 - If login_error == 1 then there was a failed login attempt --> 
            <!-- so display an error message --> 
            <c:if test="${param.login_error == 1}">
                <h3>Wrong id or password!</h3> </c:if>
                <!-- #2 - Post to Spring security to check our authentication -->

                <div class="container">
                    <!-- Navigation -->
                    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                        <div class="container">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="${pageContext.request.contextPath}">Blog</a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <%@include file="navBarFragment.jsp"%>
                        </div>
                        <!-- /.navbar-collapse -->

                    </div>

                    <!-- /.container -->
                </nav>
                <div class="col-lg-8">
                    <form class="form-signin" method="post" action="j_spring_security_check">
                        <h2 class="form-signin-heading">Please sign in</h2>
                        <label for="username" class="sr-only">Username</label>                    
                        <input type="text" id="username_or_email" name="j_username" class="form-control" placeholder="Username" required autofocus>


                        <label for="password" class="sr-only">Password</label>
                        <input type="password" id="password" name="j_password" class="form-control" placeholder="Password" required>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="remember-me"> Remember me
                            </label>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                    </form>
                </div>

            </div> <!-- /container -->




            <!--            <form method="post" class="signin" action="j_spring_security_check">  post req needs to go to spring_sec check to be authorized!! 
                            <fieldset>
                                <table cellspacing="0"> <tr>
                                        <th>
                                            <label for="username">Username </label>
                                        </th>
                                        <td><input id="username_or_email" 
                                                   name="j_username" 
                                                   type="text" /> these need to be named so that spring sec magic can find them! 
                                        </td> </tr>
                                    <tr>
                                        <th><label for="password">Password</label></th> 
                                         #2b - must be j_password for Spring  
                                        <td><input id="password"
                                                   name="j_password" 
                                                   type="password" />  these need to be named so that spring sec magic can find them! 
                                        </td> 
                                    </tr>
                                    <tr> 
                                        <th></th>
                                        <td><input name="commit" type="submit" value="Sign In" /></td> </tr>
                                </table> </fieldset>
                        </form> -->
        </div>
    </body> 
</html>
