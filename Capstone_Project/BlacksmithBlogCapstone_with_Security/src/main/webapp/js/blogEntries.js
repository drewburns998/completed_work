
$(document).ready(function () {
    loadBlogs();
    loadSummaries();
    loadPopularPosts();
    loadBlogComments();
    loadCategories();

    $('#global-search-button').click(function (event) {

        $.ajax({
            type: 'GET',
            url: '/BlacksmithBlogCapstone/search/' + $('#global-search-term').val(),
            data: JSON.stringify({
                searchTerm: $('#global-search-term').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }

        }).success(function (data, status) {
            //$('#global-search-term').val('');                        
            fillBlogEntries(data, status);

        });

    });

    $('#add-button').click(function (event) {

        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: '/BlacksmithBlogCapstone/blogEntry',
            data: JSON.stringify({
                authorName: $('#blogAuthor').val(),
                body: tinyMCE.activeEditor.getContent(),
                title: $('#blogTitle').val(),
                category: $('#blogCategory').val()

            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }

        }).success(function (data, status) {
            $('#blogAuthor').val('');
            $('#blogContent').val('');
            $('#blogTitle').val('');
            $('#blogCategory').val('');
            $('#validationErrors').empty();
            loadSummaries(data, status);
        })

                .error(function (data, status) {
                    $.each(data.responseJSON.fieldErrors, function (index, validationError) {
                        var errorDiv = $('#validationErrors');
                        errorDiv.empty();
                        errorDiv.append(validationError.message).append($('<br />'));
                    });
                });

    });

    $('#add-comment-button').click(function (event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/BlacksmithBlogCapstone/comment/',
            data: JSON.stringify({
                name: $('#commentAuthor').val(),
                body: $('#commentContent').val(),
                blogEntryId: $('#blogId').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }

        }).success(function (data, status) {
            $('#commentAuthor').val('');
            $('#commentContent').val('');
            loadBlogComments();
        });

    });

    $('#edit-blog-button').click(function (event) {
        event.preventDefault();
        $.ajax({
            type: 'PUT',
            url: '/BlacksmithBlogCapstone/edit/blogEntry/' + $('#blogId').val(),
            data: JSON.stringify({
                authorName: $('#edit-author').val(),
                body: tinyMCE.activeEditor.getContent(),
                title: $('#edit-title').val(),
                category: $('#edit-category').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }

        }).success(function (data, status) {
            window.location = '/../BlacksmithBlogCapstone/admin';
        });

    });
});

function loadCategories() {
    var cBody = $('#blogCategories');
    cBody.empty();

    $.ajax({
        url: '/BlacksmithBlogCapstone/categories/',
        type: 'GET',
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (data, status) {
        fillCategories(data, status);
    });
}

function fillCategories(categories, status) {

    var catBody = $('#blogCategories');
    catBody.empty();

    $.each(categories, function (index, category) {
        //catBody.append(category.category + '</br>');
        catBody.append($('<a>')
                .attr({
                    'onClick': 'filterByCategory(' + category.category + ')'
                })
                .text(category.category)
                );
        catBody.append($('<br>'));
    });
}



function loadBlogComments() {
    var cBody = $('#blogComments');
    var blogId = $('#blogId');
    cBody.empty();


    $.ajax({
        url: '/BlacksmithBlogCapstone/comment/' + blogId.val(),
        type: "GET",
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (data, status) {
        fillBlogComments(data, status);
    });
}

function loadPopularPosts() {
    var pBody = $('#popularRows');
    pBody.empty();

    $.ajax({
        url: '/BlacksmithBlogCapstone/blogSummary',
        type: "GET",
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (data, status) {
        fillPopularBlogs(data, status);
    });
}

function loadBlogs() {
    var cBody = $('#contentRows');
    cBody.empty();

    $.ajax({
        url: "/BlacksmithBlogCapstone/blogEntry",
        type: "GET",
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (data, status) {
        fillBlogEntries(data, status);
    });
}
function loadSummaries() {
    var cBody = $('#summaryRows');
    cBody.empty();

    $.ajax({
        url: "/BlacksmithBlogCapstone/blogSummary",
        type: "GET",
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (data, status) {
        fillBlogSummaries(data, status);
    });
}
function fillBlogComments(comments, status) {

    var cBody = $('#blogComments');
    cBody.empty();


    $.each(comments, function (index, val) {
        var date = new Date(val.timestamp);
        cBody.append($('<h6><span class="glyphicon glyphicon-comment"></span> Comment by ' + val.name + '</h6>'));
        cBody.append($('<h6><span class="glyphicon glyphicon-time"></span>' + ' ' + date + '</h6>'));
        cBody.append($('<p>' + val.body + '</p>'));
        cBody.append($('<a>')
                .attr({
                    'onClick': 'deleteComment(' + val.id + ')'
                })
                .text('Remove Comment')
                );
        cBody.append($('<hr>'));
    });

}

function fillBlogSummaries(blogList, status) {

    var cBody = $('#summaryRows');
    cBody.empty();

    $.each(blogList, function (index, blogSummary) {
        var title = blogSummary.title;
        var new_title = encodeURIComponent(title.trim());
        cBody.append($('<tr>')
                .append($('<td align="left">').text(blogSummary.date))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'href': '../BlacksmithBlogCapstone/blogEntry/' + new_title
                                })
                                .text(blogSummary.title)
                                )
                        )
                .append($('<td>').text(blogSummary.author))
                .append($('<td>').text(blogSummary.approvalStatus))
                .append($('<td align="center">').text(blogSummary.numComments))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-blogId': blogSummary.blogId,
                                    'onClick': 'approveBlogEntry(' + blogSummary.blogId + ')'
                                })
                                .text('Approve')
                                ) // Link for Contact Name
                        ) // ContactName                                
                .append($('<td>') // Delete Link
                        .append($('<a>') // Delete Anchor
                                .attr({
                                    'onClick': 'deleteBlogEntry(' + blogSummary.blogId + ')'
                                })
                                .text('Delete')
                                ) // Delete Anchor
                        ) // Delete link
                );
    });
}

function fillPopularBlogs(blogList, status) {

    var dBody = $('#popularRows');
    dBody.empty();
    var count = 0;


    blogList.sort(function (a, b) {
        return b.numComments - a.numComments;
    });

    $.each(blogList, function (index, blogSummary) {
        var title = blogSummary.title;
        var new_title = encodeURIComponent(title.trim());
        dBody.append($('<tr>')
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'href': '../BlacksmithBlogCapstone/blogEntry/' + new_title
                                })
                                .text(blogSummary.title)
                                )
                        )
                .append($('<td>').text(blogSummary.numComments))

                );
    });
}

function deleteComment(id) {

    var answer = confirm("Do you really want to delete this Comment?");

    if (answer === true) {
        $.ajax({
            url: "/BlacksmithBlogCapstone/comment/delete/" + id,
            type: "DELETE",
            headers: {
                'Accept': 'application/json'
            }
        }).success(function (data, status) {
            loadBlogComments();
        });
    }
}

function deleteBlogEntry(id) {
    var cBody = $('#summaryRows');
    cBody.empty();
    var answer = confirm("Do you really want to delete this Blog Entry?");

    if (answer === true) {
        $.ajax({
            url: "/BlacksmithBlogCapstone/blogEntry/delete/" + id,
            type: "DELETE",
            headers: {
                'Accept': 'application/json'
            }
        }).success(function (data, status) {
            fillBlogSummaries(data, status);
            loadSummaries();
        });
    }
}

function filterByCategory(category) {
    var cat = category;
    var answer = confirm("Do you really want to d");

    $.ajax({
        url: "/BlacksmithBlogCapstone/blogEntry/category/" + cat,
        type: "GET",
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (data, status) {
        fillBlogEntries(data, status);
    });
}

function approveBlogEntry(id) {
    $.ajax({
        url: "/BlacksmithBlogCapstone/approve/" + id,
        type: "POST",
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (data, status) {
        fillBlogSummaries(data, status);
        loadSummaries();
    });

}


function fillBlogEntries(blogList, status) {

    var cBody = $('#contentRows');
    cBody.empty();

    var blogSize = blogList.length;
    if (blogSize === 0)
        cBody.append($('<h3> No Blogs Found</h3>'));

    blogList.sort(function (a, b) {
        return b.timestamp - a.timestamp;
    });

    $.each(blogList, function (index, blogEntry) {
        var date = new Date(blogEntry.timestamp);
        cBody.append($('<h6><span class="glyphicon glyphicon-time"></span> Posted on '
                + date.toString() + '&nbsp;&nbsp;' + '<span class="glyphicon glyphicon-comment"></span> <i> '
                + blogEntry.authorName + '&nbsp;&nbsp;' + 'Category: ' + blogEntry.category + '</i></h6>'));
        //cBody.append($('<p> Category: ' + blogEntry.category +'</p>'));
        var title = blogEntry.title;
        var new_title = encodeURIComponent(title.trim());
        //new_title.replace('_',/%20/g);

        cBody.append($('<a>')
                .attr({
                    'href': '../BlacksmithBlogCapstone/blogEntry/' + new_title
                })
                .html('<h3>' + blogEntry.title + '</h3>')
                );
        cBody.append($('<div class="row">' + '<div class="col-md-4">' +
                '<img class="img-responsive" src="img/rsz_blacksmithing.jpg" alt="">' + '</div>' + '<div class="col-md-8">' + blogEntry.body
                + '</div>'
                ));
        cBody.append($('<hr>'));
    });
}

