/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forLoops;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class CountingMachine {

    public static void main(String[] args) {

        int userNumber = 0; //initialize to 1 since while loop
        Scanner sc = new Scanner(System.in);

        System.out.println("Input a positive number: ");
        userNumber = sc.nextInt();

        for (int i = 1; i <= userNumber; i++) {
            System.out.print(i + " ");
        }
    }
}
