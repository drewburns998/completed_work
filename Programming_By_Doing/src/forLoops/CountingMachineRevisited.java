/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forLoops;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class CountingMachineRevisited {

    public static void main(String[] args) {

        int userNumber, userCountTo, userCountBy = 0; //initialize to 1 since while loop
        Scanner sc = new Scanner(System.in);

        System.out.print("Count from : ");
        userNumber = sc.nextInt();
        
        System.out.print("Count to : ");
        userCountTo = sc.nextInt();
        
        System.out.print("Count by : ");
        userCountBy = sc.nextInt();

        for (int i = userNumber; i <= userCountTo; i=i+userCountBy) {
            System.out.print(i + " ");
        }
    }

}
