/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forLoops;

/**
 *
 * @author apprentice
 */
public class XandYs {

    public static void main(String[] args) {
        double y = 0;
        
        System.out.println("X         Y");
        System.out.println("-----------------");
        for (double i = -10; i <= 10; i=i+0.5) {
            System.out.print(i + "     ");
            System.out.println(i*i);
        }
    }
}
