/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forLoops;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AddingValues {
    public static void main(String[] args) {
        int userInput, userSum = 0;
        Scanner kb = new Scanner(System.in);

        System.out.print("What is your number? ");
        userInput = kb.nextInt();
        
        for (int i = 1; i <= userInput; i++) {
            System.out.print(i + " ");
            userSum = userSum + i;
        }
        
        System.out.println("\rCounting each adds up to: " + userSum);
    }
    
}
