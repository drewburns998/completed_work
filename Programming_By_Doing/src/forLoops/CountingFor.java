/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package forLoops;

import java.util.Scanner;

/**
 *  1) n = n+1 increments N to approach the end condition of the loop
 *  2) int n =1 initializes the start point of the loop
 *  3) 
 * 
 */
public class CountingFor {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Type in a message, and I'll display it five times.");
        System.out.print("Message: ");
        String message = keyboard.nextLine();

        for (int n = 1; n <= 10; n = n + 1) {
            System.out.println(2*n + ". " + message);
        }

    }
}
