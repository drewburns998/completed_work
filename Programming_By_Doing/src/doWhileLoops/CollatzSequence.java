/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doWhileLoops;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class CollatzSequence {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int userInput = 0;
        int newSum = 0;
        
        System.out.print("Enter an integer: ");
        userInput = sc.nextInt();
        
        while(userInput != 1){
            if(userInput % 2 == 0){
                userInput = userInput / 2;
                System.out.print(userInput + " ");
            }else{
                userInput = (3 * userInput) +1;
                System.out.print(userInput + " ");
            }        
            
        }
        
    }
    
}
