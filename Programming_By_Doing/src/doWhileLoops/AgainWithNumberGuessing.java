/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doWhileLoops;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AgainWithNumberGuessing {

    public static void main(String[] args) {
        int userGuess = 0;
        Random r = new Random();
        int secretNumber = 1 + r.nextInt(10);
        int guessCounter = 0; //initialize to 1 since while loop

        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("Guess a number from 1 to 10");
            userGuess = sc.nextInt();
            guessCounter++;
        } while (userGuess != secretNumber);
        System.out.println("Correct Guess.  Secret number was " + secretNumber);
        System.out.println("Total Guess: " + guessCounter);
    }
}
