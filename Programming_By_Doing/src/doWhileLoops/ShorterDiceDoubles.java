/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doWhileLoops;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class ShorterDiceDoubles {

    public static void main(String[] args) {

        Random r = new Random();
        int diceOne;
        int diceTwo;
        int diceTotal;

        do {
            diceOne = 1 + r.nextInt(6);
            diceTwo = 1 + r.nextInt(6);
            diceTotal = diceOne + diceTwo;
            System.out.println("Roll 1: " + diceOne);
            System.out.println("Roll 2: " + diceTwo);
            System.out.println("The total is: " + diceTotal);            
            
        }while (diceOne != diceTwo);       
        
    }
}
