/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doWhileLoops;

import java.util.Scanner;
import java.lang.*;
/**
 *
 * @author apprentice
 */
public class SafeSquareRoots {
    public static void main(String[] args) {
        double userNumber = 0; //initialize to 1 since while loop
        double userNumberRoot = 0;
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Input a positive number to find the sq root! ");
        userNumber = sc.nextDouble();
        
        while(userNumber < 0){
            System.out.println("Please input a positive number: ");
            userNumber = sc.nextDouble();
        }
        
        userNumberRoot = Math.sqrt(userNumber);
        System.out.println("user number is " + userNumberRoot);
    }
}
