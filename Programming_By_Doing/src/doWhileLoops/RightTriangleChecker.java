/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doWhileLoops;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class RightTriangleChecker {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int side1 = 0;
        int side2 = 0;
        int side3 = 0;
        boolean isRightTri;
                
        while(side1 <= 0){
            System.out.println("Enter the first side: ");
            side1 = sc.nextInt();
        }
        
        while(side2 <= side1){
            System.out.println("Enter the second side: ");
            side2 = sc.nextInt();
        }
        
        while(side3 <= side2){
            System.out.println("Enter the third side: ");
            side3 = sc.nextInt();
        }
        
        if((side1*side1 + side2*side2) == (side3*side3)){
            System.out.println("Right triangle!");
        }else{
            System.out.println("Not a right triangle!");
        }
        
    }
    
}
