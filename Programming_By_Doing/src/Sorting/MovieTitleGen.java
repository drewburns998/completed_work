package Sorting;


import java.net.URL;
import java.util.Scanner;

public class MovieTitleGen {

    public static void main(String[] args) throws Exception {

        String[] adjectives = arrayFromUrl("http://cs.leanderisd.org/txt/adjectives.txt");
        String[] nouns = arrayFromUrl("http://cs.leanderisd.org/txt/nouns.txt");

        System.out.println("Mitchell's Random Movie Title Generator\n");

        System.out.print("Choosing randomly from " + adjectives.length + " adjectives ");
        System.out.println("and " + nouns.length + " nouns (" + (adjectives.length * nouns.length) + " combinations).");

        String adjective = "Cool";
        String noun = "Mitchell";

        System.out.println("Your movie title is: " + adjective + " " + noun);
    }

    /**
     * @param url - the URL to read words from
     * @return An array of words, initialized from the given URL
     * @throws java.lang.Exception
     */
    public static String[] arrayFromUrl(String url) throws Exception {
        String[] words;
        try (Scanner fin = new Scanner((new URL(url)).openStream())) {
            int count = fin.nextInt();
            words = new String[count];
            for (int i = 0; i < words.length; i++) {
                words[i] = fin.next();
            }
        }

        return words;
    }

}
