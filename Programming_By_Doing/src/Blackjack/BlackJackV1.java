/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Blackjack;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class BlackJackV1 {

    public static void main(String[] args) {
        int playerCard1, playerCard2, compCard1, compCard2, hitCard;
        int playerTotal, compTotal;
        String playValue = ""; //hit or stay
        boolean playerAlive = true;
        boolean compAlive = true;

        Scanner sc = new Scanner(System.in);

        playerCard1 = drawCard();
        playerCard2 = drawCard();
        compCard1 = drawCard();
        compCard2 = drawCard();
        playerTotal = playerCard1 + playerCard2;
        compTotal = compCard1 + compCard2;

        System.out.println("Welcome to simple Blackjack!");
        System.out.println("You've drawn a " + playerCard1 + " and "
                + playerCard2 + " for a total of " + playerTotal);
        System.out.println("Dealer is showing: " + compCard1);

        if (playerTotal > 21) {
            System.out.println("Sorry, you lose with value of " + playerTotal);
            playerAlive = false;
        }

        if (compTotal > 21) {
            System.out.println("You win, dealer drew 22 from start!");
            compAlive = false;
        }

        while (playerAlive = true && playerTotal <= 21) {
            System.out.println("Would you like to hit or stay?");
            playValue = sc.nextLine();

            if (playValue.equals("hit")) {
                playerTotal = playerTotal + drawCard();
                System.out.println("Your new total is " + playerTotal);
            } else {
                System.out.println("You chose to stay at " + playerTotal);
                break;
            }
            if (playerTotal > 21) {
                playerAlive = false;
                System.out.println("You've busted!");
            }
        }

        if (playerAlive == true) {
            System.out.println("Okay, dealers turn:");
            System.out.println("His hidden card was " + compCard1);
            System.out.println("His total: " + compTotal);
            while (compAlive && compTotal < 17 && playerAlive == true) {
                compTotal = compTotal + drawCard();
                System.out.println("Dealer Draws:");
                if (compTotal > 21) {
                    System.out.println("Dealer Busts! Value of " + compTotal);
                    compAlive = false;
                    playerAlive = true;
                } else {
                    System.out.println("New total: " + compTotal);
                }
            }
            System.out.println("Dealer Stays at: " + compTotal);
        } else {
            System.out.println("Dealer doesn't even need to draw!");
        }

        if (playerAlive == true && compAlive == true) {
            if (playerTotal > compTotal) {
                System.out.println("Congrats, you win with " + playerTotal + "vs " + compTotal);
            } else {
                System.out.println("Dealer Wins with " + compTotal + " vs your " + playerTotal);
            }
        } else if (playerAlive == true && compAlive == false) {
            System.out.println("Dealer busts, you win");
        } else {
            System.out.println("Dealer wins!");
        }

    }

    public static int drawCard() {
        Random randomDraw = new Random();
        return (randomDraw.nextInt(10) + 2);
    }
}
