/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayLists;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author apprentice
 */
public class LocatingLargestValue {

    public static void main(String[] args) {
        Random r = new Random();
        ArrayList<Integer> demo = new ArrayList<>();

        Integer userInput;
        Integer largest = 0;

        for (int i = 0; i < 10; i++) {
            demo.add(r.nextInt(100) + 1);
            if (demo.get(i) > largest) {
                largest = demo.get(i);
            }
        }
        System.out.println("ArrayList is: " + demo);
        System.out.print("Largest value is: " + largest + " and its position is: "
                + demo.lastIndexOf(largest) + "\n");
    }
}
