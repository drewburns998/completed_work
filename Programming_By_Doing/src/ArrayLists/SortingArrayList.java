/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayLists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 *
 * @author apprentice
 */
public class SortingArrayList {

    public static void main(String[] args) {
        Random r = new Random();
        ArrayList<Integer> demo = new ArrayList<>();       

        for (int i = 0; i < 10; i++) {
            demo.add(r.nextInt(100) + 1);
        }        
        System.out.println("ArrayList is: " + demo);
        System.out.println("Sorted List:  " + sort(demo));
    }
    
    public static ArrayList<Integer> sort(ArrayList<Integer> unsorted){
        
        Collections.sort(unsorted);        
        return unsorted;
    }
}
