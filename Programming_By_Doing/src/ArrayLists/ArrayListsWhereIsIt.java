/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayLists;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ArrayListsWhereIsIt {

    public static void main(String[] args) {
        Random r = new Random();
        ArrayList<Integer> demo = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        Integer userInput;
        

        for (int i = 0; i < 10; i++) {
            demo.add(r.nextInt(50) + 1);
        }

        System.out.println("ArrayList is: " + demo);
        System.out.print("Value to find: ");
        userInput = sc.nextInt();

        if (demo.contains(userInput)) {
            System.out.println("Value in position "+ demo.lastIndexOf(userInput));
        } else {
            System.out.println(userInput + " is not in the ArrayList");
        }
    }
}
