/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayLists;

import static ArrayLists.SortingArrayList.sort;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author apprentice
 */
public class SortingArrayListOfStrings {
    public static void main(String[] args) {
        
        ArrayList<String> demo = new ArrayList<>();       
        demo.add("ask");
        demo.add("what");
        demo.add("his");
        demo.add("name");
        demo.add("is");
        demo.add("over");
        demo.add("there");
        
        System.out.println("ArrayList is: " + demo);
        System.out.println("Sorted List:  " + sort(demo));        
        
    }
    public static ArrayList<String> sort(ArrayList<String> unsorted){
        
        Collections.sort(unsorted);        
        return unsorted;
    }    
}
