/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayLists;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author apprentice
 */
public class CopyingArrayLists {

    public static void main(String[] args) {
        Random r = new Random();
        ArrayList<Integer> demo = new ArrayList<>();
        ArrayList<Integer> demo2 = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            demo.add(r.nextInt(100) + 1);
            demo2.add(demo.get(i));
        }

        System.out.println("ArrayList 1 is: " + demo);
        System.out.println("ArrayList 2 is: " + demo2);
    }

}
