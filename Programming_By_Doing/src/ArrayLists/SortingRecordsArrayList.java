/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayLists;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;


/**
 *
 * @author apprentice
 */
class Car {
    String make, model, license;
    int year;
    public String toString() {
        return year + " " + make + " " + model + " " + "(" + license + ")";
    }
}

public class SortingRecordsArrayList {

    public static void main(String[] args) throws FileNotFoundException {

        ArrayList<Car> carList = new ArrayList<>();
        Scanner sc = new Scanner(System.in);

        System.out.print("What is the name of the file? ");
        File file = new File(sc.next());
        Scanner sc2 = new Scanner(file);

        int i = 0;
        Car[] cars_array = new Car[5];

        while (sc2.hasNext()) {

            cars_array[i] = new Car();
            cars_array[i].make = sc2.next();
            cars_array[i].model = sc2.next();
            cars_array[i].year = sc2.nextInt();
            cars_array[i].license = sc2.next();
            i++;
        }
        System.out.println("Data loaded.");

        sort(cars_array);
        ArrayList<Car> cars_list = new ArrayList<>(Arrays.asList(cars_array));
        System.out.println("ArrayList: " + cars_list.toString());

        sc2.close();
    }

    public static void sort(Car[] carlist) {
        Car temp;
        for (int i = 0; i < carlist.length; i++) {
            for (int j = 0; j < carlist.length; j++) {
                if (carlist[i].year < carlist[j].year) {
                    temp = carlist[i];
                    carlist[i] = carlist[j];
                    carlist[j] = temp;
                }
            }
        }
    }

}
