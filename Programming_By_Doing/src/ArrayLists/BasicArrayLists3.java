/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayLists;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author apprentice
 */
public class BasicArrayLists3 {
    public static void main(String[] args) {
        Random r = new Random();
        ArrayList<Integer> demo = new ArrayList<>();
        
        for (int i = 0; i < 1000; i++) {
            demo.add(r.nextInt(90)+10);
        }
        
        System.out.println("Array list is: " + demo);
        System.out.println("size check: " + demo.size());
    }
    
}
