/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayLists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class SortingSentences {

    public static void main(String[] args) {
        ArrayList<String> demo = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        String sentence = "";
        String[] parts;

        System.out.println("Sentence:");
        sentence = sc.nextLine();

        parts = sentence.split(" ");

        for (int i = 0; i < parts.length; i++) {
            demo.add(parts[i]);
        }
        System.out.println("Sorted: " + sort(demo));
    }

    public static ArrayList<String> sort(ArrayList<String> unsorted) {

        Collections.sort(unsorted);
        return unsorted;
    }
}
