/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package whileLoops;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class KeepGuessing {

    public static void main(String[] args) {
        int userGuess = 0;
        Random r = new Random();
        int secretNumber = 1 + r.nextInt(10);

        Scanner sc = new Scanner(System.in);
        System.out.println("Guess a number from 1 to 10");
        userGuess = sc.nextInt();

        while (userGuess != secretNumber) {
            System.out.println("Keep Guessing!");
            userGuess = sc.nextInt();
        }
        System.out.println("Correct Guess.  Secret number was " + secretNumber);

    }

}
