/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package whileLoops;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class HiLowLimitedTries {

    public static void main(String[] args) {
        int userGuess = 0;
        Random r = new Random();
        int secretNumber = 1 + r.nextInt(100);
        int attemptCount = 1;
        Scanner sc = new Scanner(System.in);

        System.out.println("Thinking of a number between 1-100, you have 7 tries: ");
        userGuess = sc.nextInt();

        while (attemptCount <= 7) {            
            if (userGuess == secretNumber) {
                System.out.println("Congrats, you're correct!");
                break;
            } else if (userGuess < secretNumber) {
                System.out.println("You guessed too low on attempt " + attemptCount);
            } else {
                System.out.println("You guessed too high on attempt " + attemptCount);
            }
            userGuess = sc.nextInt();
            attemptCount++;
        }
    }
}
