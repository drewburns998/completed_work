/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package whileLoops;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AddingInLoop {

    public static void main(String[] args) {
        int userNum;
        int userSum = 0;

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a num, if 0 it will quit");
        userNum = sc.nextInt();

        while (userNum != 0) {
            userSum = userSum + userNum;
            System.out.println("Total so far is " + userSum);
            System.out.println("Number: ");
            userNum = sc.nextInt();            
        }
        System.out.println("Total is " + userSum);
    }

}
