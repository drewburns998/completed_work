/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package whileLoops;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class NumberGuessingWithCounter {

    public static void main(String[] args) {
        int userGuess = 0;
        Random r = new Random();
        int secretNumber = 1 + r.nextInt(10);
        int guessCounter = 1; //initialize to 1 since while loop

        Scanner sc = new Scanner(System.in);
        System.out.println("Guess a number from 1 to 10");
        userGuess = sc.nextInt();

        while (userGuess != secretNumber) {
            System.out.println("Keep Guessing!");
            userGuess = sc.nextInt();
            guessCounter++;
        }
        System.out.println("Correct Guess.  Secret number was " + secretNumber);
        System.out.println("Total Guess: " + guessCounter);
    }

}
