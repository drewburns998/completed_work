/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileIO;

import java.io.FileWriter;
import java.io.PrintWriter;

/**
 *
 * @author apprentice
 */
public class ALetterRevisited {

    public static void main(String[] args) throws java.io.IOException {
        PrintWriter pw = new PrintWriter(new FileWriter("letter.txt"));

        pw.println("+-----------------------------------+");
        pw.println("|                               ####|");
        pw.println("|                               ####|");
        pw.println("|                               ####|");
        pw.println("|                                   |");
        pw.println("|                                   |");
        pw.println("|             Andrew Burns          |");
        pw.println("|             123 Fake Street       |");
        pw.println("|             London, OH 43130      |");
        pw.println("|                                   |");
        pw.println("+-----------------------------------+");

        pw.close();

    }
}
