/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileIO;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class HighScore {

    public static void main(String[] args) throws IOException {
        PrintWriter pw = new PrintWriter(new FileWriter("score.txt"));
        Scanner sc = new Scanner(System.in);
        int highScore = 0;
        String userName = "";

        System.out.println("You got a high score");
        System.out.println("Please enter your score: ");
        highScore = sc.nextInt();
        System.out.println("Please enter your name: ");
        userName = sc.next();
        
        pw.println(highScore);
        pw.println(userName);
        pw.close();

        System.out.println("Data store in score.txt");

    }

}
