/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileIO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
class Dogs {
    String breed;
    int age;
    double weight;
}

public class DisplayingAFile {

    public static void main(String[] args) throws FileNotFoundException {

        Scanner sc = new Scanner(System.in);
        Dogs dog1 = new Dogs();
        Dogs dog2 = new Dogs();

        //dogs.txt        
        System.out.println("Enter a file Name:");
        File file = new File(sc.next());
        Scanner scTwo = new Scanner(file);

        dog1.breed = scTwo.next();
        dog1.age = scTwo.nextInt();
        dog1.weight = scTwo.nextDouble();

        dog2.breed = scTwo.next();
        dog2.age = scTwo.nextInt();
        dog2.weight = scTwo.nextDouble();

        System.out.println("First dog: " + dog1.breed + ", " + dog1.age + ", " + dog1.weight);
        System.out.println("Second dog: " + dog2.breed + ", " + dog2.age + ", " + dog2.weight);
    }
}
