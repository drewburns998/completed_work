/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileIO;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ThreeNumsAnyFile {

    public static void main(String[] args) throws FileNotFoundException {
        int numOne, numTwo, numThree;
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter a file Name:");
        System.out.println("3nums1.txt");
        System.out.println("3nums2.txt");
        System.out.println("3nums3.txt");
        
               
        File file = new File(sc1.next());

        Scanner sc = new Scanner(file);

        numOne = sc.nextInt();
        numTwo = sc.nextInt();
        numThree = sc.nextInt();

        int numSum = numOne + numTwo + numThree;

        System.out.println("Completed reading file: " + file);
        System.out.println(numOne + " + " + numTwo + " + " + numThree
                + " = " + numSum);

    }
}
