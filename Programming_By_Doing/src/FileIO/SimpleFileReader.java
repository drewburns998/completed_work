/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileIO;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author apprentice
 */
public class SimpleFileReader {

    public static void main(String[] args) throws FileNotFoundException, IOException {

        BufferedReader bufferedReader = new BufferedReader(new FileReader("name.txt"));
        
        String test1= bufferedReader.readLine();
        
        bufferedReader.close();
        System.out.println("I found the following name in this file: " + test1);
    }
}
