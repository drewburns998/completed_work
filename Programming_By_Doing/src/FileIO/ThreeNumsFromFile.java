/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileIO;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ThreeNumsFromFile {

    public static void main(String[] args) throws FileNotFoundException {
        int numOne, numTwo, numThree;
        File file = new File("3nums.txt");

        Scanner sc = new Scanner(file);

        numOne = sc.nextInt();
        numTwo = sc.nextInt();
        numThree = sc.nextInt();

        int numSum = numOne + numTwo + numThree;
        
        System.out.println("Completed reading 3nums.txt:");
        System.out.println(numOne + " + " + numTwo + " + " + numThree
                + " = " + numSum);

    }

}
