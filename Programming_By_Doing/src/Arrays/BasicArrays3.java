/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arrays;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class BasicArrays3 {

    public static void main(String[] args) {
        int[] dummyArray = new int[1000];
        Random findNum = new Random();

        for (int i = 0; i < dummyArray.length; i++) {
            dummyArray[i] = findNum.nextInt(90) + 10;
            System.out.println(dummyArray[i] + "  ");
        }
    }
}
