/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arrays;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class CopyingArrays {

    public static void main(String[] args) {
        int[] dummyArray = new int[10];
        int[] copiedArray = new int[10];
        Random findNum = new Random();

        for (int i = 0; i < dummyArray.length; i++) {
            dummyArray[i] = findNum.nextInt(100) + 1;

        }
        System.out.println(" ");

        for (int i = 0; i < dummyArray.length; i++) {
            copiedArray[i] = dummyArray[i];

        }
        dummyArray[9] = -7;

        for (int i = 0; i < dummyArray.length; i++) {
            System.out.print(dummyArray[i] + " ");
        }

        System.out.println(" ");
        for (int i = 0; i < copiedArray.length; i++) {
            System.out.print(copiedArray[i] + " ");
        }

    }

}
