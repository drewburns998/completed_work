/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arrays;

/**
 *
 * @author apprentice
 */
public class BasicArrays0 {

    public static void main(String[] args) {
        
        int[] dummyArray = {-113,-113,-113,-113,-113,-113,-113,-113,-113,-113};

        System.out.println("slot 0 contains: " + dummyArray[0]);
        System.out.println("slot 1 contains: " + dummyArray[1]);
        System.out.println("slot 2 contains: " + dummyArray[2]);
        System.out.println("slot 3 contains: " + dummyArray[3]);
        System.out.println("slot 4 contains: " + dummyArray[4]);
        System.out.println("slot 5 contains: " + dummyArray[5]);
        System.out.println("slot 6 contains: " + dummyArray[6]);
        System.out.println("slot 7 contains: " + dummyArray[7]);
        System.out.println("slot 8 contains: " + dummyArray[8]);
        System.out.println("slot 9 contains: " + dummyArray[9]);
        
    }
}
