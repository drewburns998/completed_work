/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arrays;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ParallelArrays {
    public static void main(String[] args) {
        String[] lastNames = {"Brown", "Chesney", "Davis", "Smith", "Williams"};
        double[] gradeAverages = {99.5, 84.3, 72.5, 98.4, 53.2};
        int[] studentId = {441,444,449,455,494};
        int findId = 0;
        Scanner sc = new Scanner(System.in);
        
        for (int i = 0; i < lastNames.length; i++) {
            System.out.println(lastNames[i] + " " + gradeAverages[i] + " " + studentId[i]);            
        }
        
        System.out.print("Enter an ID to find: ");
        findId = sc.nextInt();
        System.out.println(" ");
        
        for (int i = 0; i < studentId.length; i++) {
            if (findId == studentId[i]){
                System.out.println("Name: " + lastNames[i]);
                System.out.println("Average: " + gradeAverages[i]);
                System.out.println("ID: " + studentId[i]);
            }
        }
        
    }
    
}
