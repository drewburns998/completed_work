/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arrays;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class FindingValueInArray {
    public static void main(String[] args) {
        int[] dummyArray = new int[10];
        int findValue = 0;
        Random findNum = new Random();
        Scanner sc = new Scanner(System.in);
        
        for (int i = 0; i < dummyArray.length; i++) {
            dummyArray[i] = findNum.nextInt(50) + 1;
            System.out.print(dummyArray[i] + " ");
        }
        System.out.println(" ");
        System.out.print("Value to find: ");
        findValue = sc.nextInt();
        
        for (int i = 0; i < dummyArray.length; i++) {
            if(dummyArray[i] == findValue){
                System.out.println(findValue + " is in the array");   
            }
        }
    }
    
}
