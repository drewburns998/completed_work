/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arrays;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class GradesInAnArray {
    public static void main(String[] args) throws IOException {
        String userName = "";
        String fileName = "";
        int grade1, grade2, grade3, grade4, grade5;
        Random Grade = new Random();
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Name (last first)");
        userName = sc.nextLine();
        
        System.out.println("Filename: ");
        fileName = sc.nextLine();
        
        PrintWriter pw = new PrintWriter(new FileWriter(fileName));
                       
        grade1 = Grade.nextInt(100) + 1;
        grade2 = Grade.nextInt(100) + 1;
        grade3 = Grade.nextInt(100) + 1;
        grade4 = Grade.nextInt(100) + 1;
        grade5 = Grade.nextInt(100) + 1;
        
        pw.println(userName);
        pw.println(grade1);
        pw.println(grade2);
        pw.println(grade3);
        pw.println(grade4);
        pw.println(grade5);
        pw.close();

        System.out.println("Data store in: " + fileName);

        
        
    }
}
