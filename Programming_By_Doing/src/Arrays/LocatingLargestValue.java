/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Arrays;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class LocatingLargestValue {

    public static void main(String[] args) {
        int[] dummyArray = new int[10];
        int maxValue = 0;
        int maxValuePos = -1;

        Random findNum = new Random();
        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < dummyArray.length; i++) {
            dummyArray[i] = findNum.nextInt(100) + 1;
            System.out.print(dummyArray[i] + " ");
            if (dummyArray[i] >= maxValue) {
                maxValue = dummyArray[i];
                maxValuePos = i;
            }
        }
        System.out.println(" ");
        System.out.println("The max value is: " + maxValue);
        System.out.println("It is in slot: " + maxValuePos);

    }
}
