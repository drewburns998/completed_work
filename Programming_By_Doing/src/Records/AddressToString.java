/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Records;

import java.net.URL;
import java.util.Scanner;

class Address2 {

    String street;
    String city;
    String state;
    int zip;
    
    //Renaming to toString2() caused odd output - looked like memory addresses
    public String toString() {
        return (this.street + ", " + this.city + ", " + this.state + " " + this.zip);
    }
}

public class AddressToString {

    public static void main(String[] args) throws Exception {
        URL addys = new URL("https://cs.leanderisd.org/txt/fake-addresses.txt");

        Address2[] addybook = new Address2[6];

        Scanner fin = new Scanner(addys.openStream());

        for (int i = 0; i < addybook.length; i++) {
            addybook[i] = new Address2();
            addybook[i].street = fin.nextLine();
            addybook[i].city = fin.nextLine();
            addybook[i].state = fin.next();
            addybook[i].zip = fin.nextInt();
            fin.skip("\n");
        }
        fin.close();

        for (int i = 0; i < addybook.length; i++) {
            System.out.println((i + 1) + ". " + addybook[i]);
        }
    }
}
