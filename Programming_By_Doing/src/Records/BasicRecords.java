/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Records;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */

class Student{
    String name;
    int gradeClass;
    double avg;
}

public class BasicRecords {
    public static void main(String[] args) {
       Student stud1 = new Student();
       Student stud2 = new Student();
       Student stud3 = new Student();
       
       Scanner sc = new Scanner(System.in);
       
        System.out.print("\nEnter the first students name:");
        stud1.name = sc.next();
        System.out.print("\nEnter the first students grade:");
        stud1.gradeClass = sc.nextInt();
        System.out.print("\nEnter the first students average:");
        stud1.avg = sc.nextDouble();
        
        System.out.print("\nEnter the second students name: ");
        stud2.name = sc.next();
        System.out.print("\nEnter the second students grade: ");
        stud2.gradeClass = sc.nextInt();
        System.out.print("\nEnter the second students average: ");
        stud2.avg = sc.nextDouble();
        
        
        System.out.print("\nEnter the third students name: ");
        stud3.name = sc.next();
        System.out.print("\nEnter the third students grade: ");
        stud3.gradeClass = sc.nextInt();
        System.out.print("\nEnter the third students average: ");
        stud3.avg = sc.nextDouble();
       
        System.out.println("The names are " + stud1.name + " " + stud2.name + " " + stud3.name);
        System.out.println("The grades are " + stud1.gradeClass + " " + stud2.gradeClass + " " + stud3.gradeClass);
        System.out.println("The avgs are " + stud1.avg + " " + stud2.avg + " " + stud3.avg);
        
        double average = (stud1.avg + stud2.avg + stud3.avg) / 3;
        System.out.println("The average for all three students is " + average);
        
    }
}
