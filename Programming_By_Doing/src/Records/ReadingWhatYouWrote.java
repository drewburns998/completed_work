/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Records;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ReadingWhatYouWrote {

    public static void main(String[] args) throws FileNotFoundException {
        Car[] carDB = new Car[5];
        Scanner sc = new Scanner(System.in);

        System.out.print("What is the name of the file? ");
        File file = new File(sc.next());

        Scanner sc2 = new Scanner(file);

        for (int i = 0; i < carDB.length; i++) {
            carDB[i] = new Car();
            carDB[i].make = sc2.next();
            carDB[i].model = sc2.next();
            carDB[i].year = sc2.nextInt();
            carDB[i].license = sc2.next();
        }
        for (int i = 0; i < carDB.length; i++) {
            System.out.println("Car " + (i+1));
            System.out.println("\tMake: " + carDB[i].make);
            System.out.println("\tModel: " + carDB[i].model);
            System.out.println("\tYear: " + carDB[i].year);
            System.out.println("\tLicence: " + carDB[i].license);
            System.out.println("");
        }
    }
}
