/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Records;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
class studentGrades {

    int studentID;
    int gradeNumber;
    float score;
    String letterGrade;
}

public class SortingOnTwoFields {

    public static void main(String[] args) throws FileNotFoundException {
        studentGrades[] allStudents = new studentGrades[30];
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a file Name:");
        File file = new File(sc.next());
        Scanner scTwo = new Scanner(file);

        for (int i = 0; i < allStudents.length; i++) {
            allStudents[i] = new studentGrades();
            allStudents[i].studentID = scTwo.nextInt();
            allStudents[i].gradeNumber = scTwo.nextInt();
            allStudents[i].score = scTwo.nextFloat();
            allStudents[i].letterGrade = scTwo.next();
        }

        for (int i = 0; i < allStudents.length; i++) {
            for (int j = 0; j < allStudents.length; j++) {
                if (allStudents[i].studentID < allStudents[j].studentID) {
                    studentGrades temp = allStudents[j];
                    allStudents[j] = allStudents[i];
                    allStudents[i] = temp;
                }
                if (allStudents[i].studentID == allStudents[j].studentID) {
                    if (allStudents[i].gradeNumber < allStudents[j].gradeNumber) {
                        studentGrades temp2 = allStudents[j];
                        allStudents[j] = allStudents[i];
                        allStudents[i] = temp2;
                    }
                }
            }
        }
        System.out.println("Here are the sorted grades:");
        for (int i = 0; i < allStudents.length; i++) {
            System.out.println(allStudents[i].studentID + " " + allStudents[i].gradeNumber
            + " " + allStudents[i].score + " " + allStudents[i].letterGrade);

        }

    }

}
