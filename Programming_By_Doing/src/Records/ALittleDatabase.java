/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Records;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ALittleDatabase {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Student[] stud1 = new Student[3];
        stud1[0] = new Student();
        stud1[1] = new Student();
        stud1[2] = new Student();

        System.out.print("\nEnter the first students name:");
        stud1[0].name = sc.next();
        System.out.print("\nEnter the first students grade:");
        stud1[0].gradeClass = sc.nextInt();
        System.out.print("\nEnter the first students average:");
        stud1[0].avg = sc.nextDouble();

        System.out.print("\nEnter the second students name: ");
        stud1[1].name = sc.next();
        System.out.print("\nEnter the second students grade: ");
        stud1[1].gradeClass = sc.nextInt();
        System.out.print("\nEnter the second students average: ");
        stud1[1].avg = sc.nextDouble();

        System.out.print("\nEnter the third students name: ");
        stud1[2].name = sc.next();
        System.out.print("\nEnter the third students grade: ");
        stud1[2].gradeClass = sc.nextInt();
        System.out.print("\nEnter the third students average: ");
        stud1[2].avg = sc.nextDouble();

        System.out.println("The names are " + stud1[0].name + " " + stud1[1].name + " " + stud1[2].name);
        System.out.println("The grades are " + stud1[0].gradeClass + " " + stud1[1].gradeClass + " " + stud1[2].gradeClass);
        System.out.println("The avgs are " + stud1[0].avg + " " + stud1[1].avg + " " + stud1[2].avg);

        double average = (stud1[0].avg + stud1[1].avg + stud1[2].avg) / stud1.length;
        System.out.println("The average for all three students is " + average);
    }

}
