/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Records;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
class Person {

    String name;
    int age;
}

public class GettingMoreFromFile {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        Person[] people = new Person[5];
        
        //nameage.txt
        System.out.println("Enter a file Name:");
        File file = new File(sc.next());
        Scanner scTwo = new Scanner(file);
        
        for (int i = 0; i < people.length; i++) {
            people[i] = new Person();
            people[i].name = scTwo.next();
            people[i].age = scTwo.nextInt();
            
            System.out.println(people[i].name + " is " + people[i].age);
        }
        
    }
}
