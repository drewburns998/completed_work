/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Records;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ALittleDatabaseWithLoop {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Student[] stud1 = new Student[3];

        for (int i = 0; i < stud1.length; i++) {

            stud1[i] = new Student();            

            System.out.print("\nEnter student " + (i+1) + "'s name: ");
            stud1[i].name = sc.next();
            System.out.print("\nEnter student " + (i+1) + "'s grade: ");
            stud1[i].gradeClass = sc.nextInt();
            System.out.print("\nEnter student " + (i+1) + "'s average: ");
            stud1[i].avg = sc.nextDouble();
        }
        
        System.out.println("The names are " + stud1[0].name + " " + stud1[1].name + " " + stud1[2].name);
        System.out.println("The grades are " + stud1[0].gradeClass + " " + stud1[1].gradeClass + " " + stud1[2].gradeClass);
        System.out.println("The avgs are " + stud1[0].avg + " " + stud1[1].avg + " " + stud1[2].avg);

        double average = (stud1[0].avg + stud1[1].avg + stud1[2].avg) / stud1.length;
        System.out.println("The average for all three students is " + average);
    }

}
