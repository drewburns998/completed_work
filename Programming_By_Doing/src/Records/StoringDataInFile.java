/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Records;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
class Car{
    String make;
    String model;
    int year;
    String license;    
}
public class StoringDataInFile {
    public static void main(String[] args) throws IOException {
        Car[] carDB = new Car[5];
        Scanner sc = new Scanner(System.in);
        
        for (int i = 0; i < carDB.length; i++) {
            carDB[i] = new Car();
            System.out.println("Car " + (i+1));
            System.out.print("\tMake: ");
            carDB[i].make = sc.next();
            
            System.out.print("\tModel: ");
            carDB[i].model = sc.next();
            
            System.out.print("\tYear: ");
            carDB[i].year = sc.nextInt();
            
            System.out.print("\tLicense: ");
            carDB[i].license = sc.next();            
        }
        
        System.out.print("Name of file to save info? ");
        File file = new File(sc.next());        
        PrintWriter pw = new PrintWriter(new FileWriter(file));
        
        for (Car carDB1 : carDB) {
            pw.println(carDB1.make + " " + carDB1.model + " " + carDB1.year + " " + carDB1.license);
        }
        pw.close();
        
    }
    
}
