/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RandomNumbers;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Dice {

    public static void main(String[] args) {

        Random r = new Random();
        int diceOne = 1 + r.nextInt(6);
        int diceTwo = 1 + r.nextInt(6);
        int diceTotal = diceOne + diceTwo;
        
        System.out.println("Here comes the dice!");
        System.out.println("Roll 1: " + diceOne);
        System.out.println("Roll 2: " + diceTwo);
        System.out.println("The total is: " + diceTotal);

    }
}
