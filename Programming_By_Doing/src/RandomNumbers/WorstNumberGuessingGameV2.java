/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RandomNumbers;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WorstNumberGuessingGameV2 {

    public static void main(String[] args) {
        int userGuess = 0;
        Random r = new Random();
        int secretNumber = 1 + r.nextInt(10);

        Scanner sc = new Scanner(System.in);
        System.out.println("Guess a number from 1 to 10");
        userGuess = sc.nextInt();

        if (userGuess == secretNumber) {
            System.out.println("Congrats, you're right");
        } else {
            System.out.println("Wrong, my secret number was " + secretNumber);
        }

    }

}
