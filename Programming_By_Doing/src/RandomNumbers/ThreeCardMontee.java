/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RandomNumbers;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ThreeCardMontee {

    public static void main(String[] args) {
        int userGuess = 0;
        Random r = new Random();
        int secretNumber = 1 + r.nextInt(3);

        Scanner sc = new Scanner(System.in);

        System.out.println("Which one is the Ace?: ");
        System.out.println("\t ## ## ##");
        System.out.println("\t ## ## ##");
        System.out.println("\t 1  2  3");
        userGuess = sc.nextInt();

        if (userGuess == secretNumber) {
            System.out.println("Congrats, you're correct!");
        } else {
            System.out.println("You guessed too wrong, I was thinking of " + secretNumber);
        }

        switch(secretNumber){
            case 1:
                System.out.println("\t AA ## ##");
                System.out.println("\t AA ## ##");
                System.out.println("\t 1  2  3");
                break;
            case 2:
                System.out.println("\t ## AA ##");
                System.out.println("\t ## AA ##");
                System.out.println("\t 1  2  3");
                break;
            case 3:
                System.out.println("\t ## ## AA");
                System.out.println("\t ## ## AA");
                System.out.println("\t 1  2  3");
                break;
        }
    }
}
