/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RandomNumbers;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class OneShotHighLow {

    public static void main(String[] args) {
        int userGuess = 0;
        Random r = new Random();
        int secretNumber = 1 + r.nextInt(100);

        Scanner sc = new Scanner(System.in);

        System.out.println("Thinking of a number between 1-100, guess it: ");
        userGuess = sc.nextInt();

        if (userGuess == secretNumber) {
            System.out.println("Congrats, you're correct!");
        } else if (userGuess < secretNumber) {
            System.out.println("You guessed too low, I was thinking of " + secretNumber);
        } else {
            System.out.println("You guessed too high, I was thinking of " + secretNumber);
        }

    }
}
