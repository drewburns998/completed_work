/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RandomNumbers;

import java.util.Random;
import java.util.Scanner;
/**
 *
 * @author apprentice
 */
public class FortuneCookie {
    public static void main(String[] args) {
        String response = "";
        Random r = new Random();
        int fortuneFound = 1 + r.nextInt(6);
        int lottoNumber1 = 1 + r.nextInt(54);
        int lottoNumber2 = 1 + r.nextInt(54);
        int lottoNumber3 = 1 + r.nextInt(54);
        int lottoNumber4 = 1 + r.nextInt(54);
        int lottoNumber5 = 1 + r.nextInt(54);
        
        if (fortuneFound == 1) {
            response = "Love is coming ";
        } else if (fortuneFound == 2) {
            response = "Divorce is near ";
        } else if (fortuneFound == 3) {
            response = "Expect some good fortune ";
        } else if (fortuneFound == 4) {
            response = "A big decision is ahead ";
        } else if (fortuneFound == 5) {
            response = "Get ready to meet an old friend ";
        } else if (fortuneFound == 6) {
            response = "Pay attention to those closest ";
        } else{
            System.out.println("error");
        }
        
        System.out.println("Fortune: " + response);
        System.out.println(lottoNumber1 + "-" + lottoNumber2 + "-" + lottoNumber3 + 
                "-" + lottoNumber4 + "-" + lottoNumber5);
    }
}
