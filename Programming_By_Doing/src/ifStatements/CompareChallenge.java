/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifStatements;

/**
 *
 * @author apprentice
 */
public class CompareChallenge {

    public static void main(String[] args) {
        System.out.print("Comparing \"axe\" with \"dog\" produces ");
        int i = "axe".compareTo("dog");
        System.out.println(i);

        System.out.print("Comparing \"applebee's\" with \"apple\" produces ");
        System.out.println("applebee's".compareTo("apple"));
        
        System.out.print("Comparing \"love\" with \"hate\" produces ");
        int j = "love".compareTo("hate");
        System.out.println(j);

        System.out.print("Comparing \"applebee's\" with \"applebee's\" produces ");
        System.out.println("applebee's".compareTo("applebee's"));
        
        System.out.print("Comparing \"sin\" with \"sin\" produces ");
        int k = "sin".compareTo("sin");
        System.out.println(k);

        System.out.print("Comparing \"red\" with \"green\" produces ");
        System.out.println("red".compareTo("green"));
        
        System.out.print("Comparing \"butter\" with \"salad\" produces ");
        int l = "butter".compareTo("salad");
        System.out.println(l);

        System.out.print("Comparing \"applebee's\" with \"apple\" produces ");
        System.out.println("applebee's".compareTo("apple"));
        
        System.out.print("Comparing \"lust\" with \"pepper\" produces ");
        int m = "lust".compareTo("pepper");
        System.out.println(m);

        System.out.print("Comparing \"sale\" with \"honest\" produces ");
        System.out.println("sale".compareTo("honest"));
        
        System.out.print("Comparing \"wreck\" with \"fall\" produces ");
        int n = "wreck".compareTo("fall");
        System.out.println(n);

        System.out.print("Comparing \"a\" with \"senior\" produces ");
        System.out.println("a".compareTo("senior"));
    }
}
