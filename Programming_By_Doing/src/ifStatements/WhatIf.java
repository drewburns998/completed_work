/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifStatements;

/**
 *  Curly braces allow multiple lines of code within them
 *  
 */
public class WhatIf {

    public static void main(String[] args) {
        int people = 20;
        int cats = 20; //change to 20 to make neither cat message work
        int dogs = 15;

        // will print
        if (people < cats) { 
            System.out.println("Too many cats!  The world is doomed!");
        }
        // won't print
        if (people > cats) {
            System.out.println("Not many cats!  The world is saved!");
        }
        
        // wont print
        if (people < dogs) {
            System.out.println("The world is drooled on!");
        }
        // will print
        if (people > dogs) {
            System.out.println("The world is dry!");
        }
        // dogs now is 20
        dogs += 5;

        // will print
        if (people >= dogs) {
            System.out.println("People are greater than or equal to dogs.");
        }
        // will print
        if (people <= dogs) {
            System.out.println("People are less than or equal to dogs.");
        }
        // will print
        if (people == dogs) {
            System.out.println("People are dogs.");
        }
    }
}
