/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifStatements;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class BMICalculator2 {
public static void main(String[] args) {
        double height, weight, bmi;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("What is your height in meters? ");
        height = keyboard.nextDouble();

        System.out.print("What is your weight in kg? ");
        weight = keyboard.nextDouble();

        bmi = weight / (height * height);

        System.out.println("Your BMI is: " + bmi);
        
        if(bmi< 18.5){
            System.out.println("BMI Category: Underweight");
        }
        if(bmi>=18.5 && bmi<25){
            System.out.println("BMI Category: Normal Weight");
        }
        if(bmi>=25 && bmi<30){
            System.out.println("BMI Category: Overweight");
        }
        if(bmi >= 30){
            System.out.println("BMI Category: Obese");
        }
    }    
}
