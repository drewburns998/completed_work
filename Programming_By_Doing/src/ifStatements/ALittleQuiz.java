/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifStatements;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ALittleQuiz {
    
    public static void main(String[] args) {
        int letsPlay = 0;
        int answer1, answer2, answer3 = 0;
        int totalCorrect = 0;
        int totalIncorrect = 0;
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Please type any whole number to begin");
        
        letsPlay = sc.nextInt();
        
        System.out.println("What is the capital of Ohio?");
        System.out.println("  1) Ottowa");
        System.out.println("  2) Kansas");
        System.out.println("  3) Columbus");
        
        answer1 = sc.nextInt();
        if (answer1 == 3){
            totalCorrect++;
            System.out.println("That's Correct");            
        } else{
            totalIncorrect--;
            System.out.println("That's Incorrect");
        }
        
        System.out.println("What direction is the sky?");
        System.out.println("  1) Sideways");
        System.out.println("  2) Down");
        System.out.println("  3) Up");
        
        answer2 = sc.nextInt();
        if (answer1 == 3){
            totalCorrect++;
            System.out.println("That's Correct");            
        } else{
            totalIncorrect--;
            System.out.println("That's Incorrect");
        }
        
        System.out.println("What color is the white house");
        System.out.println("  1) Black");
        System.out.println("  2) Yellow");
        System.out.println("  3) White");
        
        answer3 = sc.nextInt();
        if (answer3 == 3){
            totalCorrect++;
            System.out.println("That's Correct");            
        } else{
            totalIncorrect--;
            System.out.println("That's Incorrect");
        }
        
        System.out.println("total correct: " + totalCorrect);
        System.out.println("total incorrect: " + totalIncorrect);
        
        
        
    }
    
}
