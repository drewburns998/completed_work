/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifStatements;

import java.util.Scanner;
/**
 *
 * @author apprentice
 */
public class SpaceBoxing {
    public static void main(String[] args) {
        
        double userWeight = 0;
        double newUserWeight = 0;
        int visitPlanet = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("How much is your earth weight?");
        userWeight = sc.nextDouble();
        
        
        System.out.println("I have info on the follow planets: ");
        System.out.println("1. Venus   2. Mars   3. Jupiter");
        System.out.println("4. Saturn  5. Uranus 6. Neptune");
        
        System.out.print("Which Planet are you visiting? ");
        visitPlanet = sc.nextInt();
        
        newUserWeight = visitPlanet * userWeight;
        
        System.out.println("Your will weigh " + newUserWeight + " on that planet");
        
        
        
        
    }
    
}
