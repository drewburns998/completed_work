/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifStatements;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class GenderGame {

    public static void main(String[] args) {
        String gender, firstName, lastName, maritalStatus, newName = "";
        int age = 0;
        Scanner sc = new Scanner(System.in);

        System.out.println("Are you male (m) or female (f)");
        gender = sc.nextLine();

        System.out.println("First Name: ");
        firstName = sc.nextLine();

        System.out.println("Last Name: ");
        lastName = sc.nextLine();

        System.out.println("Age: ");
        age = sc.nextInt();

        if (gender.equals("f") && age >= 20) {
            System.out.println("Are you married y or n?");
            maritalStatus = sc.next();

            System.out.println("printing marital status" + maritalStatus);
            if (maritalStatus.equals("y")) {
                newName = "MRS" + " " + firstName + " " + lastName;
                System.out.println("I'll call you " + newName);
            } else {
                newName = "MS" + " " + firstName + " " + lastName;
                System.out.println("I'll call you " + newName);
            }
        } else if (gender.equals("f") && age < 20) {
            System.out.println("Then I shall call you " + firstName + " " + lastName);
        }
        
        if (gender.equals("m") && age >= 20){
            System.out.println("Then I shall call you Mr " + firstName + " " + lastName);
        } else{
            System.out.println("Then I shall call you " + firstName + " " + lastName);
        }

    }

}
