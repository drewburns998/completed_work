/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifStatements;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class TwoQuestions {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String q1Answer = "";
        String q2Answer = "";
        
        
        System.out.println("This of an object and I'll try to guess it.");
        
        System.out.println("Question 1) Is it an animal, vegetable, or mineral? ");
        q1Answer = sc.nextLine();
        
        System.out.println("Question 2) Is it bigger than a break box (yes or no)?");
        q2Answer = sc.nextLine();
        
        if(q1Answer.equals("animal")){
            if(q2Answer.equals("yes")){
                System.out.println("You're thinking of a Moose");
            } else {
                System.out.println("You're thinking of a Squirrel");
            }
        } else if(q1Answer.equals("vegetable")){
            if(q2Answer.equals("yes")){
                System.out.println("You're thinking of a Watermelon");
            } else {
                System.out.println("You're thinking of a Carrot");
            }
        } else if(q1Answer.equals("mineral")){
            if(q2Answer.equals("yes")){
                System.out.println("You're thinking of a Camaro");
            } else {
                System.out.println("You're thinking of a paper clip");
            }
        }
        
        System.out.println("I would ask if I'm right, but i don't care");
    }
    
}
