/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifStatements;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AgeMessages3 {

    public static void main(String[] args) {

        int age = 0;

        Scanner sc = new Scanner(System.in);
        System.out.println("How old are you? ");
        age = sc.nextInt();
        
        if(age < 16){
            System.out.println("You can't drive");            
        }
        if(age>=16 && age <=17){
            System.out.println("You can drive but not vote");
        }
        if(age>17 && age <=24){
            System.out.println("You can vote but not rent a car");
        }
        if(age>=25){
            System.out.println("You can do anything");
        }

    }
}
