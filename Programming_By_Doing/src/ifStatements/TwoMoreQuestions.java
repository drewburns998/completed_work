/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifStatements;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class TwoMoreQuestions {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String q1Answer = "";
        String q2Answer = "";

        System.out.println("This of an object and I'll try to guess it.");

        System.out.println("Question 1) Is it an animal, vegetable, or mineral? ");
        q1Answer = sc.nextLine();

        System.out.println("Question 2) Is it bigger than a break box (yes or no)?");
        q2Answer = sc.nextLine();

        if (q1Answer.equals("animal") && q2Answer.equals("yes") ) {
            System.out.println("You're thinking of a Moose");
        }
        if (q1Answer.equals("animal") && q2Answer.equals("no") ) {
            System.out.println("You're thinking of a Squirrel");
        }
        if (q1Answer.equals("vegetable") && q2Answer.equals("yes") ) {
            System.out.println("You're thinking of a Watermelon");
        }
        if (q1Answer.equals("vegetable") && q2Answer.equals("no") ) {
            System.out.println("You're thinking of a Carrot");
        }
        if (q1Answer.equals("mineral") && q2Answer.equals("yes") ) {
            System.out.println("You're thinking of a Camaro");
        }
        if (q1Answer.equals("mineral") && q2Answer.equals("no") ) {
            System.out.println("You're thinking of a paperclip");
        }
        
        System.out.println("I would ask if I'm right, but i don't care");
    }
}
