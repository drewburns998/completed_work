/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifStatements;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ChooseAdventure {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int levelOneAnswer, levelTwoAnswer, levelThreeAnswer, levelFourAnswer = 0;

        System.out.println("Starting point, Door 1 or 2 (enter 1 or 2)");
        levelOneAnswer = sc.nextInt();

        if (levelOneAnswer == 1) {
            System.out.println("You chose door 1");
            System.out.println("Type 1 for left, 2 for right");
            levelTwoAnswer = sc.nextInt();
            if (levelTwoAnswer == 1) {
                System.out.println("You chose left.");
                System.out.println("Type 1 for left, 2 for right");
                levelThreeAnswer = sc.nextInt();
                if (levelThreeAnswer == 1) {
                    System.out.println("Room A is it!");
                } else {
                    System.out.println("Room B is it!");
                }
            } else {
                System.out.println("You chose right.");
                System.out.println("Type 1 for left, 2 for right");
                levelThreeAnswer = sc.nextInt();
                if (levelThreeAnswer == 1) {
                    System.out.println("Room c is it!");
                } else {
                    System.out.println("Room d is it!");
                }
            }
        } else {
            System.out.println("You chose door 2");
            System.out.println("Type 1 for left, 2 for right");
            levelTwoAnswer = sc.nextInt();
            if (levelTwoAnswer == 1) {
                System.out.println("You chose left.");
                System.out.println("Type 1 for left, 2 for right");
                levelThreeAnswer = sc.nextInt();
                if (levelThreeAnswer == 1) {
                    System.out.println("Room e is it!");
                } else {
                    System.out.println("Room f is it!");
                }
            } else {
                System.out.println("You chose right.");
                System.out.println("Type 1 for left, 2 for right");
                levelThreeAnswer = sc.nextInt();
                if (levelThreeAnswer == 1) {
                    System.out.println("Room g is it!");
                } else {
                    System.out.println("Room h is it!");
                }
            }
        }

    }

}
