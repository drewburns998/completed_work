/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifStatements;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AlphabetOrder {

    public static void main(String[] args) {
        String lastName, maritalStatus, newName = "";

        int age = 0;
        Scanner sc = new Scanner(System.in);

        System.out.println("What is your last name");
        lastName = sc.nextLine();

        int i = lastName.compareTo("Carswell");
        int j = lastName.compareTo("Jones");
        int k = lastName.compareTo("Smith");
        int l = lastName.compareTo("Young");

        if (i <= 0) {
            System.out.println("You don't have to wait long");
        } else if (j <= 0) {
            System.out.println("that's not bad");
        } else if (k <= 0) {
            System.out.println("looks like a bit of a wait");
        } else if (l <= 0) {
            System.out.println("It's gonna be a while");
        } else {
            System.out.println("Not going anywhere for a while?");
        }

    }

}
