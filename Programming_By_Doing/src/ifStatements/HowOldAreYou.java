/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifStatements;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class HowOldAreYou {
    
    public static void main(String[] args) {
        int userAge = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("How old are you?");
        
        userAge = sc.nextInt();
        
        if (userAge >= 25 ){
            System.out.println("You can do anything that's legal");
        }
        if(userAge < 25){
            System.out.println("You can't rent a car");
        }
        
        if(userAge < 18){
            System.out.println("You can't vote");
        }
        
        if(userAge <16){
            System.out.println("You can't drive");
        }
        
    }
}
