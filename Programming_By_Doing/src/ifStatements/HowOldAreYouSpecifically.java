/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifStatements;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class HowOldAreYouSpecifically {

    public static void main(String[] args) {
        int userAge = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("How old are you?");

        userAge = sc.nextInt();

        if (userAge < 16) {
            System.out.println("You can't drive");
        }
        else if (userAge <= 17) {
            System.out.println("You can drive but not vote");
        }
        else if (userAge <= 24) {
            System.out.println("You can vote but not rent a car");
        }
        else {
            System.out.println("you can do pretty much anything");
        }

    }
}
