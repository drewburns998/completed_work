/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifStatements;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WorstNumberGuessingGame {

    public static void main(String[] args) {
        String userGuess = "";
        String secretNumber = "4";

        Scanner sc = new Scanner(System.in);
        System.out.println("Guess a number from 1 to 10");
        userGuess = sc.nextLine();

        if (userGuess.equals(secretNumber)) {
            System.out.println("Congrats, you're right");
        } else {
            System.out.println("haha you're wrong");
        }

    }

}
