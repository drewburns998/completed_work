/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Methods;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AreaCalculator {

    public static void main(String[] args) {
        int base, height, length, width, sideLength, radius;
        int userChoice = 0;
        double area;
        Scanner sc = new Scanner(System.in);

        while (userChoice != 5) {
            System.out.println("1.Triangle");
            System.out.println("2.Rectangle");
            System.out.println("3.Square");
            System.out.println("4.Circle");
            System.out.println("5.Quit");

            userChoice = sc.nextInt();

            switch (userChoice) {
                case 1:
                    System.out.println("Base:");
                    base = sc.nextInt();
                    System.out.println("Height");
                    height = sc.nextInt();
                    area = area_triangle(base, height);
                    System.out.println("Area is " + area);
                    break;
                case 2:
                    System.out.println("Length:");
                    length = sc.nextInt();
                    System.out.println("Width");
                    width = sc.nextInt();
                    area = area_rectangle(length, width);
                    System.out.println("Area is " + area);
                    break;
                case 3:
                    System.out.println("Side Length");
                    sideLength = sc.nextInt();
                    area = area_square(sideLength);
                    System.out.println("Area is " + area);
                    break;
                case 4:
                    System.out.println("Radius");
                    radius = sc.nextInt();
                    area = area_circle(radius);
                    System.out.println("Area is " + area);
                    break;
                case 5:
                    System.out.println("You Quit");
                    break;
            }

        }

    }

    public static double area_circle(int radius) {
        return (Math.PI * radius * radius);
    }

    public static int area_rectangle(int length, int width) {
        return length * width;
    }

    public static int area_square(int side) {
        return side * side;
    }

    public static double area_triangle(int base, int height) {
        return (0.5 * base * height);
    }

}
