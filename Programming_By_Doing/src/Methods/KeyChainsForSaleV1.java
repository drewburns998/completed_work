/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Methods;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class KeyChainsForSaleV1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int userChoice = 0;

        while (userChoice != 4) {
            showMenu();
            System.out.print("Please enter your choice:");
            userChoice = sc.nextInt();
            switch(userChoice){
                case 1: 
                    add_keychains();
                    break;
                case 2:
                    remove_keychains();
                    break;
                case 3:
                    view_order();
                    break;
                case 4:
                    checkout();
                    break;
                default:
                    userChoice = 4;
                    break;
            }
            System.out.println("");
            
        }

    }

    public static void showMenu() {
        System.out.println("1. Add Keychain");
        System.out.println("2. Remove Keychain");
        System.out.println("3. View Current Order");
        System.out.println("4. Checkout");
    }
    
    public static void add_keychains(){
        System.out.println("ADD KEYCHAINS");
    }
    public static void remove_keychains(){
        System.out.println("REMOVE KEYCHAINS");
    }
    public static void view_order(){
        System.out.println("VIEW ORDER");
    }
    public static void checkout(){
        System.out.println("CHECKOUT");
    }

}
