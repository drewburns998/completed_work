/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Methods;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class KeyChainsForSaleV2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int userChoice = 0;
        int totalKeychains = 0;
        int keychainPrice = 10;
        double salesTax = 8.25;
        double shippingCost = 5;
        double shippingPerKey = 1;

        while (userChoice != 4) {
            showMenu();
            System.out.print("Please enter your choice:");
            userChoice = sc.nextInt();
            switch (userChoice) {
                case 1:
                    System.out.println("You have " + totalKeychains + ". how many to add?");
                    totalKeychains = add_keychains(totalKeychains);
                    System.out.println("You now have " + totalKeychains + " keychains.");
                    break;
                case 2:
                    System.out.println("You have " + totalKeychains + ". how many to remove?");
                    totalKeychains = remove_keychains(totalKeychains);
                    System.out.println("You now have " + totalKeychains + " keychains.");
                    break;
                case 3:
                    view_order(totalKeychains, keychainPrice, salesTax, shippingCost, shippingPerKey);
                    break;
                case 4:
                    checkout(totalKeychains, keychainPrice, salesTax, shippingCost, shippingPerKey);
                    break;
                default:
                    userChoice = 0;
                    System.out.println("You entered an invalid choice, choose again!");
                    break;
            }
            System.out.println("");

        }

    }

    public static void showMenu() {
        System.out.println("1. Add Keychain");
        System.out.println("2. Remove Keychain");
        System.out.println("3. View Current Order");
        System.out.println("4. Checkout");
    }

    public static int add_keychains(int x) {
        Scanner sc = new Scanner(System.in);
        int add = sc.nextInt();
        int sumKeychains = x + add;
        return sumKeychains;
    }

    public static int remove_keychains(int y) {
        Scanner sc = new Scanner(System.in);
        int remove = sc.nextInt();
        int sumKeychains;

        if (remove <= y) {
            sumKeychains = y - remove;
        } else {
            System.out.println("You don't have that many! Trying again");
            return y;
        }
        return sumKeychains;
    }

    public static void view_order(int x, int y, double tax, double shippingTotal, double shippingPer) {
        double totalCost = x * y;
        double additionalShipping = shippingPer * x;
        totalCost = totalCost + additionalShipping;
        double totalTax = tax*totalCost/100;
        double finalCost = totalCost + totalTax;
        
        System.out.println("You have " + x + " keychains.");
        System.out.println("Cost is " + y + " each");
        System.out.println("Shipping base cost is " + shippingTotal);
        System.out.println("Additional shipping cost for the chains is " + additionalShipping);
        System.out.println("The subtotal is: " + totalCost);
        System.out.println("Total tax is: " + totalTax);
        System.out.println("The final cost is: " + finalCost);
        
    }

    public static void checkout(int x, int y, double tax, double shippingTotal, double shippingPer) {
        Scanner sc = new Scanner(System.in);
        System.out.println("What is your name? ");
        String userName = sc.nextLine();

        view_order(x, y, tax, shippingTotal, shippingPer);
        System.out.println("Thanks for your order " + userName);
    }

}
