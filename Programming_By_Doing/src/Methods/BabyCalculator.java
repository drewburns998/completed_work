/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Methods;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class BabyCalculator {

    public static void main(String[] args) {
        double input1, input2;        
        Scanner sc = new Scanner(System.in);
        String operation = "";

        
        do {
            System.out.print("> ");
            input1 = sc.nextDouble();
            operation = sc.next();
            input2 = sc.nextDouble();

            switch (operation) {
                case "+":
                    add(input1, input2);
                    break;
                case "-":
                    sub(input1, input2);
                    break;
                case "*":
                    multiply(input1, input2);
                    break;
                case "/":
                    divide(input1, input2);
                    break;
                default:
                    System.out.println("Unrecognized operation");
                    input1 = 0;
                    break;
            }

        } while (input1 != 0);
    }

    public static void add(double x, double y) {
        System.out.println(x + y);
    }

    public static void sub(double x, double y) {
        System.out.println(x - y);
    }

    public static void multiply(double x, double y) {
        double product = x*y;
        System.out.println(product);
    }

    public static void divide(double x, double y) {
        double quotient = x/y;
        System.out.println(quotient);
    }
}
