/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Methods;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class DisplayingMultiples {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int userInput;
        
        System.out.print("Choose a number: ");
        userInput = sc.nextInt();
        
        for (int i = 1; i <= 12; i++) {
            System.out.println(userInput + " x " + i + " = " + (userInput*i));
        }
        
    }
    
}
