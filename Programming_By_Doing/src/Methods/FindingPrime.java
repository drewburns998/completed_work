/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Methods;

/**
 *
 * @author apprentice
 */
public class FindingPrime {
    public static void main(String[] args) {
        for (int i = 2; i <= 20; i++) {
            if(isPrime(i)){
                System.out.println(i + " <");
            } else{
                System.out.println(i);
            }
        }                
        
    }
    public static boolean isPrime( int n ){
        int counter = 0;
        for (int i = 2; i < n; i++) {
            if(n%i == 0){
                counter++;
            }
        }
        return counter <= 0;
        
    }
}
