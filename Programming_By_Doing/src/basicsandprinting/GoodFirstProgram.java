/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicsandprinting;

/**
 *
 * @author apprentice
 */
public class GoodFirstProgram {
    public static void main(String[] args) {
        //System.out.println("Hello World!");
        //System.out.println("Hello again");
        //System.out.println("I like typing this.");
        //System.out.println("This is fun");
        //System.out.println("Yay! Printing");
        //System.out.println("I'd much rather you 'not'.");
        //System.out.println("I \"said\" do not touch this");
        System.out.println("I'm only printing this line since additional instructions"
                + "asked for the others not to be!");
    }
}
