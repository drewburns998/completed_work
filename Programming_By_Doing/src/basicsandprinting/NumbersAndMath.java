/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicsandprinting;

/**
 *
 * @author apprentice
 */
public class NumbersAndMath {
    public static void main(String[] args) {
        System.out.println("I will now count my chickens:");
        // 25 plus result of 30 div by 6
        System.out.println("Hens " + (25.0 + 30.0 / 6.0));
        // 100 - remainder of 75 div by 4 so 100-3
        System.out.println("Roosters " + (100.0 - 25.0 * 3.0 % 4.0));
        
        System.out.println("Now I will count the eggs:");
        //1 + remainder of 4 div by 2 minus 0 plus 6
        //1 + 0 - 0 + 6
        //changed to floating point soon after
        System.out.println(3.0 + 2.0 + 1.0 - 5.0 + 4.0 % 2.0 - 1.0 / 4.0 + 6.0);
        
        
        System.out.println("Is it true that 3 + 2 < 5 - 7");
        //boolean if left is less than right
        System.out.println(3 + 2 < 5 - 7);
        
        System.out.println("What is 3 + 2? " + (3.0+2.0));
        System.out.println("what is 5 - 7? " + (5.0-7.0));
        
        System.out.println("Oh, that's why it's false.");
        
        System.out.println("How about some more.");
        
        //boolean comparsion operators
        System.out.println("Is it greater? " + (5.0 > -2.0));
        System.out.println("Is it greater or equal? "+ (5.0 >= -2.0));
        System.out.println("Is is less or equal? "+ (5.0 <= -2.0));
        
        
    }
    
}
