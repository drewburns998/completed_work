/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicsandprinting;
//Andrew Burns 02/16/2016
//I already understood what // does

public class CommentsAndSlashes {
    public static void main(String[] args) {
        // A comment.
        // Anything after the // is ignored by Java
        System.out.println("I could have code like this"); //and comments after are ignored
        
        //comments can also be used to disable code
        //System.out.println("this wont run");
        
        System.out.println("this will run");
    }
    
}
