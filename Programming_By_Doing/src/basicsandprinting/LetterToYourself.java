/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicsandprinting;

/**
 *
 * @author apprentice
 */
public class LetterToYourself {
    public static void main(String[] args) {
        System.out.println("+-----------------------------------+");
        System.out.println("|                               ####|");
        System.out.println("|                               ####|");
        System.out.println("|                               ####|");
        System.out.println("|                                   |");
        System.out.println("|                                   |");
        System.out.println("|             Andrew Burns          |");
        System.out.println("|             123 Fake Street       |");
        System.out.println("|             London, OH 43130      |");
        System.out.println("|                                   |");
        System.out.println("+-----------------------------------+");
    }
    
}
