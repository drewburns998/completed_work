/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package variables;

/** Answers to questions
 *  1) 4.0 is not necessary since space_in_a_car is of type double
 *  2) Floating - the number of digits before/after decimal is not fixed
 */ 
public class VariablesAndNames {

    public static void main(String[] args) {
        
        int cars, drivers, passengers, cars_not_driven, cars_driven;
        double space_in_a_car, carpool_capacity, average_passengers_per_car;

        //int number of cars assigned
        cars = 100;
        //space is floating precision
        space_in_a_car = 4.0;
        //drivers are int numbers
        drivers = 30;
        //passengers are int numbers
        passengers = 90;
        //assign difference of cars and drivers
        cars_not_driven = cars - drivers;
        //assign drivers value to the cars driven variable
        cars_driven = drivers;
        //product of cars driven and space in a car
        carpool_capacity = cars_driven * space_in_a_car;
        //quotient of passengers and cars driven
        average_passengers_per_car = passengers / cars_driven;

        System.out.println("There are " + cars + " cars available.");
        System.out.println("There are only " + drivers + " drivers available.");
        System.out.println("There will be " + cars_not_driven + " empty cars today.");
        System.out.println("We can transport " + carpool_capacity + " people today.");
        System.out.println("We have " + passengers + " to carpool today.");
        System.out.println("We need to put about " + average_passengers_per_car + " in each car.");
    }

}
