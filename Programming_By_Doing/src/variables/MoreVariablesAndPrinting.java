/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package variables;

/**
 *
 * @author apprentice
 */
public class MoreVariablesAndPrinting {

    public static void main(String[] args) {

        String Name, Eyes, Teeth, Hair;
        int Age, Height, Weight;
        double HeightConverted, WeightConverted;

        Name = "Zed A. Shaw";
        Age = 35;     // not a lie
        Height = 74;  // inches
        Weight = 180; // lbs
        Eyes = "Blue";
        Teeth = "White";
        Hair = "Brown";
        HeightConverted = Height * 2.54;
        WeightConverted = Weight * 0.453592;
        
        
        System.out.println("Let's talk about " + Name + ".");
        System.out.println("He's " + Height + " inches (or "+ HeightConverted +"cm) tall.");
        System.out.println("He's " + Weight + " pounds (or "+ WeightConverted +"kg) heavy.");
        System.out.println("Actually, that's not too heavy.");
        System.out.println("He's got " + Eyes + " eyes and " + Hair + " hair.");
        System.out.println("His teeth are usually " + Teeth + " depending on the coffee.");

        // This line is tricky; try to get it exactly right.
        System.out.println("If I add " + Age + ", " + Height + ", and " + Weight
                + " I get " + (Age + Height + Weight) + ".");
    }
}
