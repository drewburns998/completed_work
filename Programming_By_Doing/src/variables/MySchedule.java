/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package variables;

/**
 *
 * @author apprentice
 */
public class MySchedule {

    public static void main(String[] args) {
        String p1Teacher = "Mrs Smith";
        String p2Teacher = "Mrs Jones";
        String p3Teacher = "Mr White";

        String p1Course = "Science";
        String p2Course = "Tech Shop";
        String p3Course = "Health";
        System.out.println("+---------------------------+");
        System.out.println("| 1 | " + p1Course + "   |" + " " + p1Teacher + " |");
        System.out.println("| 2 | " + p2Course + " |" + " " + p2Teacher + " |");
        System.out.println("| 3 | " + p3Course + "    |" + " " + p3Teacher + "  |");
        System.out.println("+---------------------------+");
    }

}
