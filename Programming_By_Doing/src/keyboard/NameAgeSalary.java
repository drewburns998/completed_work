/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package keyboard;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class NameAgeSalary {

    public static void main(String[] args) {
        String userName = "";
        int userAge;
        double userPay;

        Scanner keyboard = new Scanner(System.in);

        System.out.println("Hello what is your name?");
        userName = keyboard.next();

        System.out.println("Hi " + userName + " how old are you?");
        userAge = keyboard.nextInt();

        System.out.println("So your " + userAge + " eh? That's not old at all!");

        System.out.println("How much do you make, " + userName + "?");
        userPay = keyboard.nextDouble();

        System.out.println(userPay + ", good for you");
    }

}
