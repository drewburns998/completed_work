/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package keyboard;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class DumbCalculator {

    public static void main(String[] args) {

        double numberOne, numberTwo, numberThree;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("What is your first number? ");
        numberOne = keyboard.nextDouble();

        System.out.print("What is your second number? ");
        numberTwo = keyboard.nextDouble();

        System.out.print("What is your third number? ");
        numberThree = keyboard.nextDouble();

        double strangeSum = (numberOne + numberTwo + numberThree) / 2;
        System.out.println("(" + numberOne + " + " + numberTwo + " + "
                + numberThree + ") / 2 is .. " + strangeSum);

    }
}
