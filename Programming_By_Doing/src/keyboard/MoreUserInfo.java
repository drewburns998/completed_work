/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package keyboard;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class MoreUserInfo {

    public static void main(String[] args) {
        String firstName = "";
        String lastName = "";
        int studentGrade; //grade number
        int studentID;
        String studentLogin = "";
        double studentGPA;
        

        Scanner keyboard = new Scanner(System.in);

        System.out.print("First name: ");
        firstName = keyboard.next();
        
        System.out.print("Last name: ");
        lastName = keyboard.next();
        
        System.out.print("Grade (9-12): ");
        studentGrade = keyboard.nextInt();
        
        System.out.print("Student ID: ");
        studentID = keyboard.nextInt();
        
        System.out.print("Login: ");
        studentLogin = keyboard.next();
        
        System.out.print("GPA (0.0-4.0): ");
        studentGPA = keyboard.nextDouble();
        
        System.out.println("\n"+"Your Information:");
        System.out.printf("\t" + "login:" + "\t" + studentLogin + "\n");
        System.out.printf("\t" + "ID:" + "\t" + studentID + "\n");
        System.out.printf("\t" + "name:" + "\t" + lastName + ", " + firstName + "\n");
        System.out.printf("\t" + "gpa:" + "\t" + studentGPA + "\n");
        System.out.printf("\t" + "grade:" + "\t" + studentGrade + "\n");
        

        
    }
}
