/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package keyboard;

import java.util.Scanner;
/**
 *
 * @author apprentice
 */
public class FindAge {
    public static void main(String[] args) {
        String userName ="";
        int userAge;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Hello, what is your name?: ");
        userName = keyboard.next();
        
        System.out.print("Hi, " + userName + " How old are you? ");
        userAge = keyboard.nextInt();
        
        System.out.println("Did you know, in five years you will be " + (userAge+5));
        
        System.out.println("and five years ago you were " + (userAge-5) + "! Imagine that!");
        
    }
}
